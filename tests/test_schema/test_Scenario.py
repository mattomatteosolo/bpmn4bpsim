from bson import ObjectId
from datetime import datetime, timedelta
import unittest

from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import (
    TimeParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.Scenario import Scenario
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    DurationParameter,
    NumericParameter,
)
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.schemas.Scenario import ScenarioSC

from tests.test_bpsimclasses import test_init


class test_ScenarioSC(unittest.TestCase):
    def setUp(self) -> None:
        self.scenario_parameters_JSON = {
            "baseTimeUnit": "ms",
            "replication": 1,
            "traceOutput": True,
            "duration": {
                "parameterName": "Duration",
                "value": [
                    {
                        "type": "DurationParameter",
                        "parameterValue": {"value": "PT8H"},
                    }
                ],
            },
        }
        self.scenario_parameters = ScenarioParameters(
            **{
                "baseTimeUnit": TimeUnit.MS,
                "replication": 1,
                "traceOutput": True,
                "duration": Parameter(
                    "Duration",
                    value=[DurationParameter(value=timedelta(hours=8))],
                ),
            }
        )
        self.element_parameters = ElementParameters(
            test_init.taskElement,
            parametersId="dsadsd",
            parametersGroup=[
                TimeParameters(
                    bpmElement=test_init.taskElement,
                    parameters=[
                        Parameter(
                            parameterName="TransferTime",
                            resultRequest=[ResultType.MAX],
                            value=[NumericParameter(value=2, timeUnit="ms")],
                        )
                    ],
                )
            ],
        )
        self.element_parameters_JSON = {
            "parametersId": "dsadsd",
            "elementRef": test_init.sempliceTaskId,
            "parametersGroup": [
                {
                    "groupName": "TimeParameters",
                    "parameters": [
                        {
                            "parameterName": "TransferTime",
                            "resultRequest": ["max"],
                            "value": [
                                {
                                    "type": "NumericParameter",
                                    "parameterValue": {
                                        "value": 2,
                                        "timeUnit": "ms",
                                    },
                                }
                            ],
                        }
                    ],
                }
            ],
        }
        self.schema = ScenarioSC(
            document_root=test_init.sempliceBPMN.getroottree()
        )
        self._id = ObjectId()
        self.dt = datetime(2020, 1, 9, 3, 43, 0)
        self.scenario_JSON = {
            "_id": str(self._id),
            "name": "sc1",
            "description": "Primo tentativo di fare uno scenario con le classi da me costruite",
            "created": self.dt.isoformat(),
            "modified": self.dt.isoformat(),
            "scenarioParameters": self.scenario_parameters_JSON,
            "elementParameters": [self.element_parameters_JSON],
        }

        self.scenario = Scenario(
            **{
                "_id": str(self._id),
                "name": "sc1",
                "description": "Primo tentativo di fare uno scenario con le classi da me costruite",
                "created": self.dt,
                "modified": self.dt,
                "scenarioParameters": self.scenario_parameters,
                "elementParameters": [self.element_parameters],
            }
        )

    def test_validate(self):
        self.assertTrue(len(self.schema.validate(self.scenario_JSON)) == 0)

    def test_load(self):
        self.assertEqual(self.schema.load(self.scenario_JSON), self.scenario)

    def test_dump(self):
        self.assertDictEqual(
            self.schema.dump(self.scenario), self.scenario_JSON
        )

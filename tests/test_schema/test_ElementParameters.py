import unittest

from lxml import etree
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import (
    TimeParameters,
)
from bpmn4bpsim.schemas.ElementParameters import (
    ElementParameters,
    ElementParametersSC,
)
from tests.test_bpsimclasses import test_init


class test_ElementParameters(unittest.TestCase):
    def setUp(self) -> None:
        if not isinstance(test_init.sempliceBPMN, etree._Element):
            raise ValueError()
        self.schema = ElementParametersSC(
            document_root=test_init.sempliceBPMN.getroottree()
        )
        if not isinstance(test_init.taskElement, etree._Element):
            raise ValueError()

        self.element_parameters = ElementParameters(
            test_init.taskElement,
            parametersId="dsadsd",
            parametersGroup=[
                TimeParameters(
                    bpmElement=test_init.taskElement,
                    parameters=[
                        Parameter(
                            parameterName="TransferTime",
                            resultRequest=[ResultType.MAX],
                            value=[NumericParameter(value=2, timeUnit="ms")],
                        )
                    ],
                )
            ],
        )

        self.element_parameters_JSON = {
            "parametersId": "dsadsd",
            "elementRef": test_init.sempliceTaskId,
            "parametersGroup": [
                {
                    "groupName": "TimeParameters",
                    "parameters": [
                        {
                            "parameterName": "TransferTime",
                            "resultRequest": ["max"],
                            "value": [
                                {
                                    "type": "NumericParameter",
                                    "parameterValue": {
                                        "value": 2,
                                        "timeUnit": "ms",
                                    },
                                }
                            ],
                        }
                    ],
                }
            ],
        }

    def test_validate(self):
        self.assertTrue(
            len(self.schema.validate(self.element_parameters_JSON)) == 0
        )

    def test_load(self):
        self.assertEqual(
            self.schema.load(self.element_parameters_JSON),
            self.element_parameters,
        )

    def test_dump(self):
        self.assertEqual(
            self.schema.dump(self.element_parameters),
            self.element_parameters_JSON,
        )

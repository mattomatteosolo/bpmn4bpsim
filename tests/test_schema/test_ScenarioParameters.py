from datetime import timedelta
import unittest
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import DurationParameter
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.schemas.ScenarioParameters import ScenarioParametersSC


class test_ScenarioParametersSC(unittest.TestCase):
    schema = ScenarioParametersSC()
    scenarioParamsAttributes = {
        'baseTimeUnit': TimeUnit.MS,
        'replication': 1,
        'traceOutput': True,
        'duration': Parameter(
            'Duration',
            value=[DurationParameter(value=timedelta(hours=8))]
        )
    }
    scenario_parameters_JSON = {
        'baseTimeUnit':'ms',
        'replication':1,
        'traceOutput':True,
        'duration':{
            'parameterName':'Duration',
            'value':[
                {
                    'type':'DurationParameter',
                    'parameterValue':{
                        'value':'PT8H'
                    }
                }
            ]
        }
    }
    scenarioParameters = ScenarioParameters(**scenarioParamsAttributes)

    def test_validate(self):
        self.assertCountEqual(
            self.schema.validate(self.scenario_parameters_JSON),
            []
        )

    def test_load(self):
        self.assertEqual(
            self.schema.load(self.scenario_parameters_JSON),
            self.scenarioParameters
        )

    def test_dump(self):
        self.assertDictEqual(
            self.schema.dump(self.scenarioParameters),
            self.scenario_parameters_JSON
        )

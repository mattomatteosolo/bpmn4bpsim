import unittest
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import NumericParameter
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import TimeParameters
from bpmn4bpsim.schemas.ParameterGroups.TimeParameters import TimeParametersSC
from tests.test_bpsimclasses import test_init


class test_TimeParameters(unittest.TestCase):
    def setUp(self) -> None:
        if test_init.taskElement == None:
            raise ValueError()
        self.schema = TimeParametersSC(test_init.taskElement)

        self.time_parameters_JSON = {
            'parameters': [
                {
                    'parameterName': 'TransferTime',
                    'resultRequest': [
                        'max'
                    ],
                    'value':[
                        {
                            'type': 'NumericParameter',
                            'parameterValue': {
                                'value': 2,
                                'timeUnit': 'ms'
                            }
                        }
                    ]
                }
            ]
        }

        self.time_parameters = TimeParameters(
            bpmElement=test_init.taskElement,
            parameters=[
                Parameter(
                    'TransferTime',
                    value=[
                        NumericParameter(value=2, timeUnit='ms')
                    ],
                    resultRequest=[
                        ResultType.MAX
                    ]
                )
            ]
        )

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(
            self.time_parameters_JSON), [])

    def test_load(self):
        self.assertEqual(self.schema.load(
            self.time_parameters_JSON), self.time_parameters)

    def test_dump(self):
        self.assertDictEqual(self.schema.dump(
            self.time_parameters), self.time_parameters_JSON)

import unittest

from lxml import etree
from bpmn4bpsim.bpsimclasses.ParameterGroups.ControlParameters import (
    ControlParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    FloatingParameter,
)
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.schemas.ParameterGroups.ControlParameters import (
    ControlParametersSC,
)
from tests.test_bpsimclasses import test_init


class test_ControlParameters(unittest.TestCase):
    def setUp(self):
        if not isinstance(test_init.startEventElement, etree._Element):
            raise ValueError()
        self.schema = ControlParametersSC(test_init.startEventElement)
        self.control_JSON = {
            "parameters": [
                {
                    "parameterName": "TriggerCount",
                    "value": [
                        {
                            "type": "NumericParameter",
                            "parameterValue": {"value": 2},
                        }
                    ],
                    "resultRequest": ["count"],
                },
                {
                    "parameterName": "InterTriggerTimer",
                    "value": [
                        {
                            "type": "FloatingParameter",
                            "parameterValue": {"value": 2.5},
                        }
                    ],
                    "resultRequest": ["min"],
                },
            ]
        }

        self.control_JSON_error = {
            "parameters": [
                {
                    "parameterName": "InterTriggerTimer",
                    "value": [
                        {
                            "type": "NumericParameter",
                            "parameterValue": {"value": 2},
                        }
                    ],
                }
            ]
        }

        self.name_error = {
            "parameters": [
                {
                    "parameterName": "skdjaksj",
                    "value": [
                        {
                            "type": "NumericParameter",
                            "parameterValue": {"value": 2},
                        }
                    ],
                }
            ]
        }

        self.control_parameter = ControlParameters(
            test_init.startEventElement,
            parameters=[
                Parameter(
                    "TriggerCount",
                    value=[NumericParameter(2)],
                    resultRequest=[ResultType.COUNT],
                ),
                Parameter(
                    "InterTriggerTimer",
                    value=[FloatingParameter(2.5)],
                    resultRequest=[ResultType.MIN],
                ),
            ],
        )

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.control_JSON), [])
        self.assertNotEqual(
            len(self.schema.validate(self.control_JSON_error)), []
        )
        self.assertNotEqual(self.schema.validate(self.name_error), [])

    def test_load(self):
        self.assertEqual(
            self.schema.load(self.control_JSON), self.control_parameter
        )

    def test_dump(self):
        self.assertEqual(
            self.schema.dump(self.control_parameter), self.control_JSON
        )

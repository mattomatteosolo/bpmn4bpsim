import unittest
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import NumericParameter
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import TimeParameters
from bpmn4bpsim.schemas.ParameterGroups.ParametersGroupOneOf import ParametersGroupOneOf


class test_ParametersGroupOneOf(unittest.TestCase):
    def setUp(self) -> None:
        if test_init.taskElement == None:
            raise ValueError()
        self.schema_one_of = ParametersGroupOneOf(test_init.taskElement)

        self.pg_one_of_JSON = {
            'groupName': 'TimeParameters',
            'parameters': [
                {
                    'parameterName': 'TransferTime',
                    'resultRequest': [
                        'max'
                    ],
                    'value':[
                        {
                            'type': 'NumericParameter',
                            'parameterValue': {
                                'value': 2,
                                'timeUnit': 'ms'
                            }
                        }
                    ]
                }
            ]
        }

        self.time_parameters = TimeParameters(
            bpmElement=test_init.taskElement,
            parameters=[
                Parameter(
                    'TransferTime',
                    value=[
                        NumericParameter(value=2, timeUnit='ms')
                    ],
                    resultRequest=[
                        ResultType.MAX
                    ]
                )
            ]
        )

    def test_validate(self):
        self.assertTrue(
            len(self.schema_one_of.validate(self.pg_one_of_JSON)) <= 0
        )

    def test_load(self):
        self.assertEqual(
            self.schema_one_of.load(self.pg_one_of_JSON),
            self.time_parameters
        )

    def test_dump(self):
        self.assertEqual(
            self.schema_one_of.dump(self.time_parameters),
            self.pg_one_of_JSON
        )

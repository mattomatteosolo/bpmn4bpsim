import unittest
from bpmn4bpsim.bpsimclasses.ParameterGroups.PriorityParameters import PriorityParameters
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import BooleanParameter, NumericParameter
from bpmn4bpsim.schemas.ParameterGroups.PriorityParameters import PriorityParametersSC
from tests.test_bpsimclasses import test_init


class test_PriorityParameters(unittest.TestCase):
    def setUp(self) -> None:
        if test_init.taskElement == None:
            raise ValueError()
        self.schema = PriorityParametersSC(test_init.taskElement)

        self.priority_parameters = PriorityParameters(
            test_init.taskElement,
            [
                Parameter(
                    'Priority',
                    value=[
                        NumericParameter(2)
                    ]
                ),
                Parameter(
                    'Interruptible',
                    value=[
                        BooleanParameter(True)
                    ]
                )
            ]
        )

        self.priority_JSON = {
            'parameters': [
                {
                    'parameterName': 'Priority',
                    'value': [
                        {
                            'type': 'NumericParameter',
                            'parameterValue': {
                                'value': 2
                            }
                        }
                    ]
                },
                {
                    'parameterName': 'Interruptible',
                    'value': [
                        {
                            'type': 'BooleanParameter',
                            'parameterValue': {
                                'value': True
                            }
                        }
                    ]
                }
            ]
        }

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.priority_JSON), [])

    def test_load(self):
        self.assertEqual(self.schema.load(self.priority_JSON),
                         self.priority_parameters)

    def test_dump(self):
        self.assertDictEqual(self.schema.dump(
            self.priority_parameters), self.priority_JSON)

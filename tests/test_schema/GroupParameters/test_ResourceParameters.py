import unittest
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import StringParameter, BooleanParameter, NumericParameter
from bpmn4bpsim.bpsimclasses.ParameterGroups.ResourceParameters import ResourceParameters
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.schemas.ParameterGroups.ResourceParameters import ResourceParametersSC
from tests.test_bpsimclasses import test_init


class test_ResourceParametersSC(unittest.TestCase):
    def setUp(self) -> None:
        if test_init.resourceElement == None:
            raise ValueError()

        self.schema = ResourceParametersSC(test_init.resourceElement)
        self.resource_JSON = {
            'parameters': [
                {
                    'parameterName': 'Role',
                    'value': [
                        {
                            'type': 'StringParameter',
                            'parameterValue': {
                                'value': 'cuoco'
                            }
                        }
                    ]
                },
                {
                    'parameterName': 'Role',
                    'value': [
                        {
                            'type': 'StringParameter',
                            'parameterValue': {
                                'value': 'capo chef'
                            }
                        }
                    ]
                },
                {
                    'parameterName': 'Availability',
                    'value': [
                        {
                            'type': 'BooleanParameter',
                            'parameterValue': {
                                'value': True
                            }
                        }
                    ]
                },
                {
                    'parameterName': 'Quantity',
                    'value': [
                        {
                            'type': 'NumericParameter',
                            'parameterValue': {
                                'value': 2,
                                'timeUnit': 'ms'
                            }
                        }
                    ]
                }
            ]
        }

        self.resource_parameters = ResourceParameters(
            test_init.resourceElement,
            parameters=[
                Parameter(
                    'Quantity',
                    value=[
                        NumericParameter(2, timeUnit='ms')
                    ]
                ),
                Parameter(
                    'Availability',
                    value=[
                        BooleanParameter(True)
                    ]
                ),
                Parameter(
                    'Role',
                    value=[
                        StringParameter('cuoco')
                    ]
                ),
                Parameter(
                    'Role',
                    value=[
                        StringParameter('capo chef')
                    ]
                )
            ]
        )

    def test_validate(self):
        self.assertCountEqual(self.schema
                              .validate(self.resource_JSON), [])

    def test_load(self):
        self.assertEqual(self.schema.load(self.resource_JSON),
                         self.resource_parameters)

    def test_dump(self):
        self.assertEqual(
            sorted(
                self.schema.dump(self.resource_parameters)['parameters'],
                key=lambda x: x['parameterName']
            ),
            sorted(
                self.resource_JSON['parameters'],
                key=lambda x: x['parameterName']
            )
        )

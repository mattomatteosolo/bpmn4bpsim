import unittest
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.DistributionParameter import (
    TruncatedNormalDistribution,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterGroups.PropertyParameters import (
    PropertyParameters,
)
from bpmn4bpsim.schemas.ParameterGroups.PropertyParameters import (
    PropertyParametersSC,
    PropertyOrParameter,
)
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.Parameter import Property, Parameter, PropertyType
from bpmn4bpsim.bpsimclasses.ResultType import ResultType


class test_PropertyParametersSC(unittest.TestCase):
    def setUp(self):
        if test_init.taskElement == None:
            raise ValueError()
        self.schema = PropertyParametersSC(test_init.taskElement)
        self.schema_scenario_parameters = PropertyParametersSC(
            None, scenario_parameters=True
        )
        self.queueLength = Parameter(
            "QueueLength", resultRequest=[ResultType.MIN]
        )
        self.property1 = Property(
            propertyName="nOfIssues",
            value=[
                TruncatedNormalDistribution(
                    mean=2.0, minV=1.0, maxV=1000.0, standardDeviation=1.0
                )
            ],
            propertyType=PropertyType.LONG,
        )
        self.property2 = Property(
            "noOfIssues",
            value=[
                ExpressionParameter(value="bpsim:getProperty('nOfIssues') -1")
            ],
        )

        self.propertyParameters = PropertyParameters(
            test_init.taskElement,
            parameters=[self.queueLength, self.property2, self.property1],
        )
        self.property_parameters_scenario = PropertyParameters(
            None,
            parameters=[self.queueLength, self.property2, self.property1],
            scenario_parameters=True,
        )

        self.property_JSON = {
            "parameters": [
                {
                    "parameterName": "Property",
                    "value": [
                        {
                            "parameterValue": {
                                "value": "bpsim:getProperty('nOfIssues') -1"
                            },
                            "type": "ExpressionParameter",
                        }
                    ],
                    "propertyName": "noOfIssues",
                },
                {
                    "parameterName": "Property",
                    "value": [
                        {
                            "type": "TruncatedNormalDistribution",
                            "parameterValue": {
                                "mean": 2.0,
                                "standardDeviation": 1.0,
                                "min": 1.0,
                                "max": 1000.0,
                            },
                        }
                    ],
                    "propertyName": "nOfIssues",
                    "propertyType": "long",
                },
                {"parameterName": "QueueLength", "resultRequest": ["min"]},
            ]
        }

        self.prop_or_param = PropertyOrParameter()

    def test_ParameterOrProperty(self):
        # testint property2
        self.assertCountEqual(
            self.prop_or_param.validate(self.property_JSON["parameters"][0]), []
        )
        self.assertEqual(
            self.prop_or_param.load(self.property_JSON["parameters"][0]),
            self.property2,
        )
        self.assertDictEqual(
            self.prop_or_param.dump(self.property2),
            self.property_JSON["parameters"][0],
        )
        # testing queueLength
        self.assertCountEqual(
            self.prop_or_param.validate(self.property_JSON["parameters"][2]), []
        )
        self.assertEqual(
            self.prop_or_param.load(self.property_JSON["parameters"][2]),
            self.queueLength,
        )
        self.assertEqual(
            self.prop_or_param.dump(self.queueLength),
            self.property_JSON["parameters"][2],
        )
        # testing property1
        self.assertCountEqual(
            self.prop_or_param.validate(self.property_JSON["parameters"][1]), []
        )
        self.assertEqual(
            self.prop_or_param.load(self.property_JSON["parameters"][1]),
            self.property1,
        )
        self.assertEqual(
            self.prop_or_param.dump(self.property1),
            self.property_JSON["parameters"][1],
        )

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.property_JSON), [])

    def test_load(self):
        self.assertEqual(
            self.schema.load(self.property_JSON), self.propertyParameters
        )

    def test_dump(self):
        dumped = self.schema.dump(self.propertyParameters)
        self.assertDictEqual(
            dumped,
            self.property_JSON,
        )

    def test_scenario_properties(self):
        self.assertCountEqual(
            self.schema_scenario_parameters.validate(self.property_JSON), []
        )

import unittest

from lxml import etree
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    FloatingParameter,
    NumericParameter,
)
from bpmn4bpsim.schemas.ParameterGroups.CostParameters import (
    CostParmetersSC,
    CostParameters,
)
from tests.test_bpsimclasses import test_init


class test_CostParameters(unittest.TestCase):

    def setUp(self):
        if not isinstance(test_init.taskElement, etree._Element):
            raise ValueError()
        self.schema = CostParmetersSC(test_init.taskElement)
        self.cost_JSON = {
            "parameters": [
                {
                    "parameterName": "UnitCost",
                    "resultRequest": ["sum"],
                    "value": [
                        {
                            "type": "FloatingParameter",
                            "parameterValue": {"value": 2.5},
                        }
                    ],
                },
                {
                    "parameterName": "FixedCost",
                    "value": [
                        {
                            "type": "NumericParameter",
                            "parameterValue": {"value": 2, "timeUnit": "ms"},
                        }
                    ],
                },
            ]
        }

        self.paramValue_error = {
            "parameters": [
                {
                    "parameterName": "FixedCost",
                    "value": [
                        {
                            "type": "StringParameter",
                            "parameterValue": {
                                "value": "sdasd",
                            },
                        }
                    ],
                }
            ]
        }

        self.cost_parameters = CostParameters(
            test_init.taskElement,
            [
                Parameter(
                    "UnitCost",
                    value=[FloatingParameter(2.5)],
                    resultRequest=[ResultType.SUM],
                ),
                Parameter("FixedCost", value=[NumericParameter(2, timeUnit="ms")]),
            ],
        )

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.cost_JSON), [])
        self.assertCountEqual(
            self.schema.validate(self.paramValue_error), ["parameters"]
        )

    def test_load(self):
        self.assertEqual(self.schema.load(self.cost_JSON), self.cost_parameters)

    def test_dump(self):
        self.assertDictEqual(
            self.schema.dump(self.cost_parameters), self.cost_JSON
        )

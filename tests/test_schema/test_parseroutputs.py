from unittest import TestCase
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.parser.parseroutputs import (
    OutputParameters,
    PossibleInputs,
    PossibleOutputs,
    Possible,
)
from bpmn4bpsim.schemas.parseroutputs import (
    OutputParametersSC,
    PossibleInputsSC,
    PossibleOutputsSC,
    PossibleSC,
)


class ParserOutputSCBaseTestCase(TestCase):
    def setUp(self) -> None:
        self.op1 = OutputParameters(
            "TransferTime",
            [
                ResultType.MIN,
                ResultType.MAX,
                ResultType.MEAN,
                ResultType.COUNT,
                ResultType.SUM,
            ],
        )
        self.opDict = {
            "parameterName": "TransferTime",
            "possibleOutputs": ["min", "max", "mean", "count", "sum"],
        }

        self.possibleOutputs = PossibleOutputs(
            ParameterGroupsName.TimeParameters, [self.op1]
        )
        self.possibleOutputsDict = {
            "groupParameter": "TimeParameters",
            "parameters": [{**self.opDict}],
        }

        self.possibleInputs = PossibleInputs(
            ParameterGroupsName.TimeParameters,
            [
                "TransferTime",
                "QueueTime",
                "WaitTime",
                "SetupTime",
                "ProcessingTime",
                "ValidationTime",
                "ReworkTime",
            ],
        )
        self.possibleInputsDict = {
            "groupParameter": "TimeParameters",
            "parameters": [
                "TransferTime",
                "QueueTime",
                "WaitTime",
                "SetupTime",
                "ProcessingTime",
                "ValidationTime",
                "ReworkTime",
            ],
        }
        self.possible = Possible(
            xpathID="//*[@id='someId']",
            input=[
                PossibleInputs(
                    ParameterGroupsName.TimeParameters,
                    [
                        "TransferTime",
                        "QueueTime",
                        "WaitTime",
                        "SetupTime",
                        "ProcessingTime",
                        "ValidationTime",
                        "ReworkTime",
                    ],
                )
            ],
            output=[
                PossibleOutputs(ParameterGroupsName.TimeParameters, [self.op1])
            ],
        )
        self.possibleDict = {
            "xpathID": "//*[@id='someId']",
            "input": [{**self.possibleInputsDict}],
            "output": [{**self.possibleOutputsDict}],
        }
        return super().setUp()


class OutputParametersSCTestCase(ParserOutputSCBaseTestCase):
    def test_dumping(self):
        opS = OutputParametersSC()
        self.assertEqual(opS.dump(self.op1), self.opDict)

    def test_loading(self):
        opS = OutputParametersSC()
        self.assertEqual(opS.load(self.opDict), self.op1)

    def test_validate(self):
        opS = OutputParametersSC()
        self.assertEqual(opS.validate(self.opDict), {})


class PossibleOutputsSCTestCase(ParserOutputSCBaseTestCase):
    def test_dumping(self):
        poS = PossibleOutputsSC()
        self.assertEqual(
            poS.dump(self.possibleOutputs), self.possibleOutputsDict
        )

    def test_loading(self):
        poS = PossibleOutputsSC()
        self.assertEqual(
            poS.load(self.possibleOutputsDict), self.possibleOutputs
        )

    def test_validate(self):
        poS = PossibleOutputsSC()
        self.assertEqual(poS.validate(self.possibleOutputsDict), {})


class PossibleInputsSCTestCase(ParserOutputSCBaseTestCase):
    def test_dumping(self):
        piS = PossibleInputsSC()
        self.assertEqual(piS.dump(self.possibleInputs), self.possibleInputsDict)

    def test_loading(self):
        piS = PossibleInputsSC()
        self.assertEqual(piS.load(self.possibleInputsDict), self.possibleInputs)

    def test_validate(self):
        piS = PossibleInputsSC()
        self.assertEqual(piS.validate(self.possibleInputsDict), {})


class PossibleSCTestCase(ParserOutputSCBaseTestCase):
    def test_dumping(self):
        possibleSchema = PossibleSC()
        self.assertEqual(possibleSchema.dump(self.possible), self.possibleDict)

    def test_loading(self):
        possibleSchema = PossibleSC()
        self.assertEqual(possibleSchema.load(self.possibleDict), self.possible)

    def test_validate(self):
        possibleSchema = PossibleSC()
        self.assertEqual(possibleSchema.validate(self.possibleDict), {})

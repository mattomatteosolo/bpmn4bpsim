from os import path
from bson import ObjectId
from datetime import datetime, timedelta
import unittest
from lxml import etree
from bpmn4bpsim.bpsimclasses.Scenario import Scenario
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import DurationParameter, NumericParameter
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.Modello import Modello
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import TimeParameters
from bpmn4bpsim.schemas.Model import ModelloSC
from tests.test_bpsimclasses import test_init


class test_ModelloSC(unittest.TestCase):
    scenario_parameters_JSON = {
        'baseTimeUnit': 'ms',
        'replication': 1,
        'traceOutput': True,
        'duration': {
            'parameterName': 'Duration',
            'value': [
                {
                    'type': 'DurationParameter',
                    'parameterValue': {
                        'value': 'PT8H'
                    }
                }
            ]
        }
    }
    scenario_parameters = ScenarioParameters(**{
        'baseTimeUnit': TimeUnit.MS,
        'replication': 1,
        'traceOutput': True,
        'duration': Parameter(
            'Duration',
            value=[DurationParameter(value=timedelta(hours=8))]
        )
    })
    element_parameters = ElementParameters(
        test_init.taskElement,
        parametersId='dsadsd',
        parametersGroup=[
            TimeParameters(
                bpmElement=test_init.taskElement,
                parameters=[
                    Parameter(
                        parameterName='TransferTime',
                        resultRequest=[ResultType.MAX],
                        value=[NumericParameter(value=2, timeUnit='ms')]
                    )
                ]
            )
        ]
    )
    element_parameters_JSON = {
        'parametersId': 'dsadsd',
        'elementRef': test_init.sempliceTaskId,
        'parametersGroup': [
            {
                'groupName': 'TimeParameters',
                'parameters': [
                    {
                        'parameterName': 'TransferTime',
                        'resultRequest': [
                            'max'
                        ],
                        'value':[
                            {
                                'type': 'NumericParameter',
                                'parameterValue': {
                                    'value': 2,
                                    'timeUnit': 'ms'
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
    _id = ObjectId()
    dt = datetime(2020, 1, 9, 3, 43, 0)
    scenario_JSON = {
        '_id': str(_id),
        'name': 'sc1',
        'description': 'Primo tentativo di fare uno scenario con le classi da me costruite',
        'created': dt.isoformat(),
        'modified': dt.isoformat(),
        'scenarioParameters': scenario_parameters_JSON,
        'elementParameters': [
                element_parameters_JSON
        ]
    }

    scenario = Scenario(
        ** {
            '_id': str(_id),
            'name': 'sc1',
            'description': 'Primo tentativo di fare uno scenario con le classi da me costruite',
            'created': dt,
            'modified': dt,
            'scenarioParameters': scenario_parameters,
            'elementParameters': [
                element_parameters
            ]
        }
    )
    with open(path.join(test_init.modelsDir, "simple.bpmn")) as s:
        modello_XML = etree.parse(
            s,
            parser=etree.XMLParser(remove_blank_text=True)
        ).getroot()

    schema = ModelloSC(modello_XML)
    modello_JSON = {
        'nomeModello': 'sei un culo',
        '_id': 'mela',
        'scenari': [scenario_JSON]
    }
    modello = Modello(
        modello_XML,
        'sei un culo',
        'mela',
        [scenario]
    )

    def test_validate(self):
        self.assertTrue(
            len(self.schema.validate(self.modello_JSON)) == 0
        )

    def test_load(self):
        self.assertEqual(
            self.schema.load(self.modello_JSON),
            self.modello
        )

    def test_dump(self):
        self.assertEqual(
            self.schema.dump(self.modello),
            self.modello_JSON
        )

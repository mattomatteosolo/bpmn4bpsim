import unittest

from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.Parameter import Parameter, Property, PropertyType
from bpmn4bpsim.schemas.Parameter import ParameterSC, PropertySC
from bpmn4bpsim.schemas.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from bpmn4bpsim.schemas.ParameterValues import constant_schemas


class test_Parameter(unittest.TestCase):
    numeric_value = NumericParameter(1, timeUnit="ms")
    parameter = Parameter("Ciao", value=[numeric_value])
    parameter_json = {
        "parameterName": "Ciao",
        "value": [
            {
                "type": "NumericParameter",
                "parameterValue": {"value": 1, "timeUnit": "ms"},
            }
        ],
    }
    parameter_only_name = {"parameterName": "Ciao"}
    queue_length = {"parameterName": "QueueLength", "resultRequest": ["min"]}
    schema = ParameterSC(schemas=constant_schemas)

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.parameter_json), [])
        self.assertCountEqual(
            self.schema.validate(self.parameter_only_name), {"_schema": "2"}
        )
        self.assertCountEqual(self.schema.validate(self.queue_length), [])

    def test_load(self):
        self.assertEqual(self.parameter, self.schema.load(self.parameter_json))
        self.assertEqual(
            self.schema.load(self.queue_length),
            Parameter(
                parameterName="QueueLength", resultRequest=[ResultType.MIN]
            ),
        )

    def test_dump(self):
        self.assertEqual(self.schema.dump(self.parameter), self.parameter_json)
        self.assertEqual(
            self.schema.dump(
                Parameter(
                    parameterName="QueueLength", resultRequest=[ResultType.MIN]
                )
            ),
            self.queue_length,
        )


class test_Property(unittest.TestCase):

    schema = PropertySC()

    property_json = {
        "parameterName": "Property",
        "propertyName": "noOfIssue",
        "value": [
            {
                "type": "NumericParameter",
                "parameterValue": {"value": 1, "timeUnit": "ms"},
            }
        ],
    }

    prop_type_json = {
        "parameterName": "Property",
        "propertyName": "noOfIssue",
        "propertyType": "long",
        "value": [
            {
                "type": "NumericParameter",
                "parameterValue": {"value": 1, "timeUnit": "ms"},
            }
        ],
    }

    prop = Property(
        "noOfIssue", value=[NumericParameter(value=1, timeUnit="ms")]
    )
    prop_type = Property(
        "noOfIssue",
        value=[NumericParameter(value=1, timeUnit="ms")],
        propertyType=PropertyType.LONG,
    )

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.property_json), [])
        self.assertCountEqual(self.schema.validate(self.prop_type_json), [])

    def test_load(self):
        self.assertEqual(self.schema.load(self.property_json), self.prop)
        self.assertEqual(self.schema.load(self.prop_type_json), self.prop_type)

    def test_dump(self):
        self.assertEqual(self.schema.dump(self.prop), self.property_json)
        self.assertEqual(self.schema.dump(self.prop_type), self.prop_type_json)

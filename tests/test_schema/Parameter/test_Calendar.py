from datetime import datetime
import uuid
import unittest
from bpmn4bpsim.bpsimclasses.Calendar import Calendar
from bpmn4bpsim.schemas.Calendar import CalendarSC, iCalendar, Event


class test_CalendarSC(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls.schema = CalendarSC()
        eventUUID = uuid.uuid1()
        dt1 = (
            datetime.isoformat(
                datetime(
                    year=2012, month=5, day=25, hour=14, minute=27, second=4
                )
            )
            .replace(":", "")
            .replace("-", "")
        )
        dt2 = (
            datetime.isoformat(
                datetime(
                    year=2010, month=5, day=25, hour=14, minute=27, second=4
                )
            )
            .replace(":", "")
            .replace("-", "")
        )
        dt3 = (
            datetime.isoformat(
                datetime(
                    year=2010, month=5, day=25, hour=18, minute=27, second=4
                )
            )
            .replace(":", "")
            .replace("-", "")
        )
        cls.cal_JSON = {
            "name": "c1",
            "id": "id1",
            "calendar": {
                "prodid": "BPSimaaS",
                "version": 2.0,
                "event": [
                    {
                        "dtstamp": dt1,
                        "dtstart": dt2,
                        "dtend": dt3,
                        "uid": str(eventUUID),
                        "rrule": {
                            "freq": "weekly",
                            "byday": ["mo", "tu", "we", "th", "fr"],
                        },
                    }
                ],
            },
        }
        cls.calJSON1 = cls.cal_JSON["calendar"]
        cls.calendar1 = iCalendar()
        # calendar1.add()
        event1 = Event()
        event1.add(
            "dtstamp",
            datetime(year=2012, month=5, day=25, hour=14, minute=27, second=4),
        )
        event1.add(
            "dtstart",
            datetime(year=2010, month=5, day=25, hour=14, minute=27, second=4),
        )
        event1.add(
            "dtend",
            datetime(year=2010, month=5, day=25, hour=18, minute=27, second=4),
        )
        event1.add("rrule", cls.cal_JSON["calendar"]["event"][0]["rrule"])
        calendar = iCalendar()
        event = Event()
        event.add(
            "dtstamp",
            datetime(year=2012, month=5, day=25, hour=14, minute=27, second=4),
        )
        event.add(
            "dtstart",
            datetime(year=2010, month=5, day=25, hour=14, minute=27, second=4),
        )
        event.add(
            "dtend",
            datetime(year=2010, month=5, day=25, hour=18, minute=27, second=4),
        )
        # event.add('duration',timedelta(hours=8))
        event["uid"] = str(eventUUID)
        event.add(
            "rrule", {"freq": "weekly", "byday": ["mo", "tu", "we", "th", "fr"]}
        )
        calendar.add_component(event)
        calendar["prodid"] = "BPSimaaS"
        calendar["version"] = 2.0

        cls.bpsim_calendar = Calendar(name="c1", id="id1", calendar=calendar)

    def test_validate(self):
        self.assertTrue(len(self.schema.validate(self.cal_JSON)) == 0)

    def test_load(self):
        self.assertEqual(self.schema.load(self.cal_JSON), self.bpsim_calendar)

    def test_dump(self):
        self.maxDiff = None
        self.assertDictEqual(
            self.schema.dump(self.bpsim_calendar), self.cal_JSON
        )

import unittest
from bpmn4bpsim.schemas.ParameterValues.EnumParameter import (
    EnumParameter,
    EnumParameterSC,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    StringParameter,
    FloatingParameter,
)


class test_EnumParameterSC(unittest.TestCase):

    enum_parameter = EnumParameter(
        [NumericParameter(1), FloatingParameter(2.5), StringParameter("mela")],
        validFor="c1",
    )

    enum_JSON = {
        "validFor": "c1",
        "value": [
            {"type": "NumericParameter", "parameterValue": {"value": 1}},
            {"type": "FloatingParameter", "parameterValue": {"value": 2.5}},
            {"type": "StringParameter", "parameterValue": {"value": "mela"}},
        ],
    }
    schema = EnumParameterSC()

    def test_validation(self):
        self.assertCountEqual(self.schema.validate(self.enum_JSON), [])
        # print(self.schema.dump(self.enum_parameter))

    def test_load(self):
        self.assertEqual(self.schema.load(self.enum_JSON), self.enum_parameter)

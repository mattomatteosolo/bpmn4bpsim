from copy import deepcopy
import unittest
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from bpmn4bpsim.schemas.ParameterValues.ExtendedParameter import (
    ExtendedParameterValueSC,
)
from bpmn4bpsim.schemas.ParameterValues import constant_schemas


class test_ExtendedParameterValue(unittest.TestCase):
    ext_numeric_json = {
        "type": "NumericParameter",
        "parameterValue": {"value": 1},
    }
    numeric_value = NumericParameter(1)
    ext_numeric_json_timeUnit = {
        "type": "NumericParameter",
        "parameterValue": {"value": 1, "timeUnit": "ms"},
    }
    numeric_value_timeUnit = NumericParameter(1, timeUnit="ms")
    ext_numeric_json_timeUnit_validFor = {
        "type": "NumericParameter",
        "parameterValue": {"value": 1, "timeUnit": "ms", "validFor": "c1"},
    }
    numeric_value_timeUnit_validFor = NumericParameter(
        1, timeUnit="ms", validFor="c1"
    )
    schema = ExtendedParameterValueSC(type_schemas=constant_schemas)

    def test_validation(self):
        self.assertCountEqual(self.schema.validate(self.ext_numeric_json), [])
        self.assertCountEqual(
            self.schema.validate(self.ext_numeric_json_timeUnit), []
        )

    def test_load(self):
        self.assertEqual(
            self.schema.load(self.ext_numeric_json), self.numeric_value
        )
        self.assertEqual(
            self.schema.load(self.ext_numeric_json_timeUnit),
            self.numeric_value_timeUnit,
        )

    def test_dump(self):
        data = deepcopy(self.ext_numeric_json)
        data["parameterValue"]["timeUnit"] = "ms"
        numeic_value = NumericParameter(1, timeUnit="ms")
        self.assertEqual(self.schema.dump(numeic_value), data)
        self.assertEqual(
            self.schema.dump(self.numeric_value_timeUnit),
            self.ext_numeric_json_timeUnit,
        )
        self.assertEqual(
            self.schema.dump(self.numeric_value_timeUnit_validFor),
            self.ext_numeric_json_timeUnit_validFor,
        )

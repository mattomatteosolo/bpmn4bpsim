import unittest
from datetime import timedelta, datetime
from bpmn4bpsim.schemas.ParameterValues.ConstantParameter import (
    NumericParameterSC, FloatingParameterSC, StringParameterSC, BooleanParameterSC, DatetimeParameterSC, DurationParameterSC)
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter, FloatingParameter, StringParameter, BooleanParameter, DurationParameter, DateTimeParameter)


class test_ConstantParameter(unittest.TestCase):
    def test_Validation(self):
        # print('Niente da testare per ConstantParameter')
        pass

    def test_post_load(self):
        pass


class test_NumericParameter(unittest.TestCase):
    numericJsonAll = {
        'value': 2,
        'validFor': 'c1',
        'timeUnit': 'ms'
    }
    numericJsonKeyError = {
        'validFor': 'c1',
        'timeUnit': 'ms'
    }
    numericJsonVal = {
        'value': 2
    }
    numericJsonValValid = {
        'value': 2,
        'validFor': 'c1',
    }
    numericJsonValTime = {
        'value': 2,
        'timeUnit': 'ms'
    }
    # 'value' : '86' is still valid
    numericJsonValError = {
        'value': '86klk',
    }

    numeric = NumericParameter(2, 'c1', 'ms')
    schema = NumericParameterSC()

    def test_Validation(self):
        schema = NumericParameterSC()
        self.assertEqual(len(schema.validate(self.numericJsonAll)), 0)
        self.assertNotEqual(len(schema.validate(self.numericJsonKeyError)), 0)
        self.assertNotEqual(len(schema.validate(self.numericJsonValError)), 0)
        self.assertEqual(len(schema.validate(self.numericJsonVal)), 0)
        self.assertEqual(len(schema.validate(self.numericJsonValValid)), 0)
        self.assertEqual(len(schema.validate(self.numericJsonValTime)), 0)

    def test_load(self):
        schema = NumericParameterSC()
        numericFromJson = schema.load(self.numericJsonAll)
        self.assertEqual(self.numeric, numericFromJson)

    def test_dump(self):
        # import json
        # print(
        #     json.dumps(
        #         self.schema.dump(NumericParameter(2, 'c1', 'ms')),
        #         indent=2
        #     )
        # )
        # print(
        #     json.dumps(
        #         self.numericJsonAll,
        #         indent=2
        #     )
        # )
        self.assertEqual(
            self.numericJsonAll,
            self.schema.dump(NumericParameter(2, validFor='c1', timeUnit='ms'))
        )


class test_FloatingParameter(unittest.TestCase):
    floatingJsonAll = {
        'value': 9.5,
        'validFor': 'c1',
        'timeUnit': 'ms'
    }
    floatingJsonKeyError = {
        'validFor': 'c1',
        'timeUnit': 'ms'
    }
    floatingJsonVal = {
        'value': 9.5
    }
    floatingJsonValValid = {
        'value': 9.5,
        'validFor': 'c1',
    }
    floatingJsonValTime = {
        'value': 9.5,
        'timeUnit': 'ms'
    }
    # 'value' : '86.45' is still valid
    floatingJsonValError = {
        'value': '86klk',
    }

    def test_Validation(self):
        schema = FloatingParameterSC()
        self.assertEqual(len(schema.validate(self.floatingJsonAll)), 0)
        self.assertNotEqual(len(schema.validate(self.floatingJsonKeyError)), 0)
        self.assertNotEqual(len(schema.validate(self.floatingJsonValError)), 0)
        self.assertEqual(len(schema.validate(self.floatingJsonVal)), 0)
        self.assertEqual(len(schema.validate(self.floatingJsonValValid)), 0)
        self.assertEqual(len(schema.validate(self.floatingJsonValTime)), 0)

    def test_load(self):
        schema = FloatingParameterSC()
        floating_from_json = schema.load(self.floatingJsonAll)
        self.assertEqual(FloatingParameter(
            9.5, 'c1', 'ms'), floating_from_json)


class test_StringParameter(unittest.TestCase):
    string_json_all = {
        'value': 'mela',
        'validFor': 'c1',
    }
    string_json_key_error = {
        'validFor': 'c1',
    }
    string_json_val = {
        'value': 'mela'
    }
    string_json_val_error = {
        'value': True
    }

    schema = StringParameterSC()

    def test_Validation(self):
        self.assertEqual(len(self.schema.validate(self.string_json_all)), 0)
        self.assertEqual(len(self.schema.validate(self.string_json_val)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.string_json_key_error)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.string_json_val_error)), 0)

    def test_load(self):
        string_param = self.schema.load(self.string_json_all)
        self.assertEqual(StringParameter('mela', 'c1'), string_param)


class test_BooleanParameter(unittest.TestCase):

    boolean_json_all = {
        'value': True,
        'validFor': 'c1',
    }
    boolean_json_key_error = {
        'validFor': 'c1',
    }
    boolean_json_val = {
        'value': True
    }
    boolean_json_val_error = {
        'value': 95
    }

    schema = BooleanParameterSC()

    def test_validation(self):
        self.assertEqual(len(self.schema.validate(self.boolean_json_all)), 0)
        self.assertEqual(len(self.schema.validate(self.boolean_json_val)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.boolean_json_key_error)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.boolean_json_val_error)), 0)

    def test_load(self):
        boolean_parameter = self.schema.load(self.boolean_json_all)
        self.assertEqual(BooleanParameter(True, 'c1'), boolean_parameter)


class test_DurationParameter(unittest.TestCase):
    duration_json_all = {
        # 'value':timedelta(days=1),
        'value': 'P1D',
        'validFor': 'c1',
    }
    duration_json_key_error = {
        'validFor': 'c1',
    }
    duration_json_val = {
        'value': timedelta(days=1)
    }
    duration_json_value_error = {
        'value': 'P23231'
    }

    schema = DurationParameterSC()

    def test_validation(self):
        self.assertEqual(len(self.schema.validate(self.duration_json_all)), 0)
        self.assertEqual(len(self.schema.validate(self.duration_json_val)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.duration_json_key_error)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.duration_json_value_error)), 0)

    def test_load(self):
        duration_parameter = self.schema.load(self.duration_json_all)
        self.assertEqual(duration_parameter,
                         DurationParameter(timedelta(days=1), 'c1'))


class test_DateTimeParameter(unittest.TestCase):

    dt = datetime(year=2019, month=8, day=1, hour=2, minute=22, second=11)
    datetime_json_all = {
        'value': dt.isoformat(),
        'validFor': 'c1',
    }
    datetime_json_key_error = {
        'validFor': 'c1',
    }
    datetime_json_val = {
        'value': dt.isoformat()
    }
    datetime_json_value_error = {
        'value': '2019-sdadsadsada'
    }

    schema = DatetimeParameterSC()

    def test_validation(self):
        self.assertEqual(len(self.schema.validate(self.datetime_json_all)), 0)
        self.assertEqual(len(self.schema.validate(self.datetime_json_val)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.datetime_json_key_error)), 0)
        self.assertNotEqual(
            len(self.schema.validate(self.datetime_json_value_error)), 0)

    def test_load(self):
        datetime_parameter = DateTimeParameter(self.dt, 'c1')
        self.assertEqual(datetime_parameter,
                         self.schema.load(self.datetime_json_all))

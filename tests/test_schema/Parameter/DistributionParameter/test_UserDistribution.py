import unittest
from bpmn4bpsim.schemas.ParameterValues.DistributionParameter.UserDistribution import (
    UserDistributionDataPointSC,
    UserDistributionSC,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.UserDistribution import (
    UserDistributionDataPoint,
    UserDistribution,
)


class test_UserDistributionDataPointSC(unittest.TestCase):

    point = UserDistributionDataPoint(
        probability=0.3, value=NumericParameter(3)
    )
    point_JSON = {
        "probability": 0.3,
        "value": {
            "type": "NumericParameter",
            "parameterValue": {
                "value": 3,
            },
        },
    }
    point_JSON_prob_neg = {
        "probability": -0.3,
        "value": {"type": "NumericParameter", "parameterValue": {"value": 3}},
    }
    point_JSON_prob_err = {
        "probability": 40.3,
        "value": {"type": "NumericParameter", "parameterValue": {"value": 3}},
    }
    schema = UserDistributionDataPointSC()

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.point_JSON), [])
        self.assertCountEqual(
            self.schema.validate(self.point_JSON_prob_neg), ["probability"]
        )
        self.assertCountEqual(
            self.schema.validate(self.point_JSON_prob_err), ["probability"]
        )

    def test_load(self):
        self.assertEqual(self.schema.load(self.point_JSON), self.point)

    def test_dump(self):
        self.assertDictEqual(self.schema.dump(self.point), self.point_JSON)


class test_UserDistributionSC(unittest.TestCase):

    schema = UserDistributionSC()
    points = [
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.5),
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.3),
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.2),
    ]
    points_JSON = [
        {
            "probability": 0.5,
            "value": {
                "type": "NumericParameter",
                "parameterValue": {"value": 3},
            },
        },
        {
            "probability": 0.3,
            "value": {
                "type": "NumericParameter",
                "parameterValue": {"value": 3},
            },
        },
        {
            "probability": 0.2,
            "value": {
                "type": "NumericParameter",
                "parameterValue": {"value": 3},
            },
        },
    ]
    points_prob_gt = [
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.5),
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.3),
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.6),
    ]
    points_prob_lt = [
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.5),
        UserDistributionDataPoint(value=NumericParameter(3), probability=0.3),
    ]
    user_dist_JSON = {"points": points_JSON, "discrete": False}
    user_dist = UserDistribution(points=points, discrete=False)

    def test_validate(self):
        self.assertCountEqual(self.schema.validate(self.user_dist_JSON), [])

    def test_load(self):
        self.assertEqual(self.schema.load(self.user_dist_JSON), self.user_dist)

    def test_dump(self):
        self.assertDictEqual(
            self.schema.dump(self.user_dist), self.user_dist_JSON
        )

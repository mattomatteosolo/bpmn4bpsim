from lxml import etree
from tests import LoadingModelsBaseTestCase
from bpmn4bpsim.bpmnfacilities import bpmnelement, BPMN_NAMESPACES
from bpmn4bpsim.bpmnfacilities import bpmn_enums as enums


class bpmnelementTestCase(LoadingModelsBaseTestCase):
    def setUp(self):
        super().setUp()

        # TODO: only made some tests, not all, so, keep an eye open
        element = self.everything.getroot().find(
            ".//task[@id='Task_1hcentk' ]", BPMN_NAMESPACES
        )
        self.task = element if element != None else etree.Element("x")

        element = self.everything.getroot().find(
            ".//subProcess[@id='Task_0hd3qhk']", BPMN_NAMESPACES
        )
        self.subprocess1 = element if element != None else etree.Element("x")

        element = self.everything.getroot().find(
            ".//subProcess[@id='Task_1pldqvf']", BPMN_NAMESPACES
        )
        self.subprocess2 = element if element != None else etree.Element("x")

        element = self.everything.getroot().find(
            ".//subProcess[@id='SubProcess_19tbqz0']", BPMN_NAMESPACES
        )
        self.subprocess3 = element if element != None else etree.Element("x")

        element = self.everythingRes.getroot().find(
            ".//resource[@id='Resource_1']", BPMN_NAMESPACES
        )
        self.resource = element if element != None else etree.Element("x")

        element = self.inclusivegateway.getroot().find(
            ".//sequenceFlow[@id='SequenceFlow_1fd1656']",
            BPMN_NAMESPACES,
        )
        self.sf = element if element != None else etree.Element("x")
        element = self.everythingRes.getroot().find(
            ".//sequenceFlow[@id='SequenceFlow_1ybszr4']",
            BPMN_NAMESPACES,
        )
        self.invalidsf1 = element if element != None else etree.Element("x")
        element = self.everythingRes.getroot().find(
            ".//sequenceFlow[@id='SequenceFlow_0wnb4ke']",
            BPMN_NAMESPACES,
        )
        self.invalidsf2 = element if element != None else etree.Element("x")

    def test_getConditionTask(self):
        self.assertListEqual(
            bpmnelement.getConditions(self.task),
            [
                enums.BPMNActivities.task,
                enums.BPMNActivityConditions.nodecomposition,
            ],
        )

    def test_getConditionSubProcess(self):
        self.assertListEqual(
            bpmnelement.getConditions(self.subprocess1),
            [
                enums.BPMNActivities.subprocess,
                enums.BPMNActivityConditions.nodecomposition,
                enums.BPMNActivityConditions.noincoming,
            ],
        )
        self.assertNotEqual(
            bpmnelement.getConditions(self.subprocess2),
            [
                enums.BPMNActivities.subprocess,
                enums.BPMNActivityConditions.nodecomposition,
                enums.BPMNActivityConditions.noincoming,
            ],
        )
        self.assertListEqual(
            bpmnelement.getConditions(self.subprocess3),
            [
                enums.BPMNActivities.subprocess,
                enums.BPMNActivityConditions.noincoming,
            ],
        )

    def test_getConditionEventSubProcess(self):
        self.assertNotEqual(
            bpmnelement.getConditions(self.subprocess1),
            [
                enums.BPMNActivities.eventsubprocess,
                enums.BPMNActivityConditions.nodecomposition,
                enums.BPMNActivityConditions.noincoming,
            ],
        )
        self.assertListEqual(
            bpmnelement.getConditions(self.subprocess2),
            [
                enums.BPMNActivities.eventsubprocess,
                enums.BPMNActivityConditions.nodecomposition,
            ],
        )

    def test_getConditionResource(self):
        self.assertEqual(
            bpmnelement.getConditions(self.resource),
            [enums.BPMNAttributes.resource],
        )

    def test_getConditionSequenceFlow(self):
        self.assertEqual(
            bpmnelement.getConditions(self.sf),
            [enums.BPMNConnObjects.sequenceflow],
        )
        self.assertNotEqual(
            bpmnelement.getConditions(self.invalidsf1),
            [enums.BPMNConnObjects.sequenceflow],
        )
        self.assertNotEqual(
            bpmnelement.getConditions(self.invalidsf2),
            [enums.BPMNConnObjects.sequenceflow],
        )
        self.assertNotEqual(
            bpmnelement.getConditions(self.subprocess3),
            [enums.BPMNConnObjects.sequenceflow],
        )

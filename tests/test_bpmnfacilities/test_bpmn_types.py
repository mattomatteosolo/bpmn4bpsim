from lxml import etree
from tests import LoadingModelsBaseTestCase
from bpmn4bpsim.bpmnfacilities import bpmn_types


class bpmn_typesTestCase(LoadingModelsBaseTestCase):
    def setUp(self):
        super().setUp()
        activity = self.everything.getroot().xpath(
            "//*[local-name()='task' and @id='Task_1hcentk']",
        )
        assert isinstance(activity, list)
        assert isinstance(activity[0], etree._Element), "Element not found"
        self.activity = activity[0]
        eventsubprocess = self.everything.xpath(
            ".//*[local-name()='subProcess' and @id='Task_1pldqvf']"
        )
        assert isinstance(eventsubprocess, list)
        assert isinstance(
            eventsubprocess[0], etree._Element
        ), "Element not found"
        self.eventsubprocess = eventsubprocess[0]

        es_parent = self.eventsubprocess.getparent()
        self.es_parent = es_parent if es_parent != None else etree.Element("")

        sequenceFlow = self.everything.xpath(
            "//*[local-name()='sequenceFlow' and @id='SequenceFlow_0wnb4ke']"
        )
        assert isinstance(sequenceFlow, list)
        assert isinstance(sequenceFlow[0], etree._Element)
        self.sequenceFlowInvalid = sequenceFlow[0]

        sequenceFlow = self.carRepair.xpath(
            "//*[local-name()='sequenceFlow' and @id='_3B161ED4-4C10-4349-9A2D-B4E002D5EC0E']"
        )
        assert isinstance(sequenceFlow, list)
        assert isinstance(sequenceFlow[0], etree._Element)
        self.sequenceFlow = sequenceFlow[0]

        resource = self.everythingRes.xpath("//*[local-name()='resource' and @id='Resource_1']")
        assert isinstance(resource, list)
        assert isinstance(resource[0], etree._Element)
        self.resource = resource[0]

        # <resourceRole id="ResourceRole_1" name="Resource Role 1">
        resRole = self.everythingRes.xpath("//*[local-name()='resourceRole' and @id='ResourceRole_1']")
        assert isinstance(resRole, list)
        assert isinstance(resRole[0], etree._Element)
        self.resRole = resRole[0]

    def test_isEventSubprocess(self):
        self.assertFalse(bpmn_types.isEventSubProcess(self.activity))
        self.assertTrue(bpmn_types.isEventSubProcess(self.eventsubprocess))

    def test_isSequenceFlow(self):
        self.assertFalse(bpmn_types.isSequenceFlow(self.eventsubprocess))
        self.assertFalse(bpmn_types.isSequenceFlow(self.sequenceFlowInvalid))
        self.assertTrue(bpmn_types.isSequenceFlow(self.sequenceFlow))

    def test_isResource(self):
        self.assertFalse(bpmn_types.isResource(self.sequenceFlow))
        self.assertTrue(bpmn_types.isResource(self.resource))

    def test_isResourceRole(self):
        self.assertFalse(bpmn_types.isResourceRole(self.resource))
        self.assertTrue(bpmn_types.isResourceRole(self.resRole))

    def test_isMessageFlow(self):
        pass

    def test_isStartEvent(self):
        pass

    # Others are really similar

from lxml import etree
from tests import LoadingModelsBaseTestCase
from bpmn4bpsim.bpmnfacilities import bpmn_conditional_types, BPMN_NAMESPACES


class bpmn_conditional_typesTestCase(LoadingModelsBaseTestCase):
    def setUp(self):
        super().setUp()
        element = self.everything.getroot().find(
            ".//process[@id='Process_1']", namespaces=BPMN_NAMESPACES
        )
        self.process = element if element != None else etree.Element("")

        element = self.everything.getroot().find(
            ".//subProcess[@id='SubProcess_19tbqz0']",
            namespaces=BPMN_NAMESPACES,
        )
        self.decomposableSubProcess = (
            element if element != None else etree.Element("")
        )

        element = self.everything.getroot().find(
            ".//manualTask[@id='Task_1ne04od']", namespaces=BPMN_NAMESPACES
        )
        self.manualTask = element if element != None else etree.Element("x")

        element = self.eventSubprocess.getroot().find(
            ".//startEvent[@id='Event_0zonw3h']", namespaces=BPMN_NAMESPACES
        )
        self.startEventInESP = (
            element if element != None else etree.Element("x")
        )

        element = self.everything.getroot().find(
            ".//receiveTask[@id='ReceiveTask_0efo7fs']", BPMN_NAMESPACES
        )
        self.messageTask = element if element != None else etree.Element("x")
        element = self.everything.getroot().find(
            ".//task[@id='Task_1hcentk']", namespaces=BPMN_NAMESPACES
        )
        self.task = element if element != None else etree.Element("x")

    def test_isDecomposable(self):
        self.assertTrue(bpmn_conditional_types.isDecomposable(self.process))
        self.assertTrue(
            bpmn_conditional_types.isDecomposable(self.decomposableSubProcess)
        )
        self.assertFalse(bpmn_conditional_types.isDecomposable(self.manualTask))

    def test_startInEventSubprocess(self):
        self.assertTrue(
            bpmn_conditional_types.startInEventSubProcess(self.startEventInESP)
        )
        self.assertFalse(
            bpmn_conditional_types.startInEventSubProcess(self.process)
        )
        self.assertFalse(
            bpmn_conditional_types.startInEventSubProcess(
                self.decomposableSubProcess
            )
        )

    def test_hasIncoming(self):
        self.assertFalse(bpmn_conditional_types.hasIncoming(self.messageTask))
        self.assertTrue(bpmn_conditional_types.hasIncoming(self.task))

import unittest
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit

class test_TimeUnit(unittest.TestCase):
    def test_isTimeUnit(self):
        self.assertTrue(TimeUnit.isTimeUnit('ms'))

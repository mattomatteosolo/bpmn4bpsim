import unittest
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.ParameterGroups.CostParameters import (
    CostParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from tests.test_bpsimclasses.test_TimeParameters import tParams
from tests.test_bpsimclasses import test_init


class test_ElementParameters(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls.elParams = ElementParameters(
            test_init.taskElement,
            parametersId="dsadsd",
            parametersGroup=[tParams],
        )
        # print(etree.tostring(elParams.toXML(),encoding='unicode',pretty_print=True))
        cls.cost_parameter = CostParameters(
            bpmElement=test_init.taskElement,
            parameters=[
                Parameter(
                    parameterName="UnitCost", value=[NumericParameter(value=2)]
                )
            ],
        )
        cls.elParams1 = ElementParameters(
            test_init.taskElement,
            parametersId="dsadsd",
            parametersGroup=[tParams],
        )

    def test_init_checks(self):
        # self.assertRaises(TypeError, ElementParameters, elementRef="ciao")
        self.assertRaises(
            TypeError,
            ElementParameters,
            elementRef=test_init.taskElement,
            id=[1, 2],
        )
        self.assertRaises(
            TypeError,
            ElementParameters,
            elementRef=test_init.taskElement,
            parametersGroup=True,
        )
        # self.assertRaises(
        #     ValueError,
        #     ElementParameters,
        #     elementRef=test_init.taskElement,
        #     parametersGroup=[1, 2, 3],
        # )

    def test_addParameterGroup(self):
        previousLen = len(self.elParams1.groupParameters)
        self.elParams1.addGroupParameter(self.cost_parameter)
        self.assertGreater(len(self.elParams1.groupParameters), previousLen)

    def test_removeParameterGroup(self):
        self.elParams1.addGroupParameter(self.cost_parameter)
        prev_len = len(self.elParams1.groupParameters)
        self.elParams1.removeGroupParameter("CostParameters")
        self.assertLess(len(self.elParams1.groupParameters), prev_len)

    def test_updateParameterGroup(self):
        c_params = CostParameters(
            bpmElement=test_init.taskElement,
            parameters=[
                Parameter(
                    parameterName="FixedCost", value=[NumericParameter(value=2)]
                )
            ],
        )
        self.elParams1.addGroupParameter(self.cost_parameter)
        self.elParams1.updateGroupParameter(c_params)
        self.assertNotEqual(
            self.elParams1.parameterGroup("CostParameters"), self.cost_parameter
        )
        self.assertEqual(
            self.elParams1.parameterGroup("CostParameters"), c_params
        )

    def test_toXML(self):
        pass

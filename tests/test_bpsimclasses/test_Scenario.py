from bson import ObjectId
from datetime import datetime, timedelta
import unittest
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.Scenario import Scenario
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    DurationParameter,
    NumericParameter,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import (
    TimeParameters,
)
from tests.test_bpsimclasses import test_init

scenarioParameters = ScenarioParameters(
    **{
        "baseTimeUnit": TimeUnit.MS,
        "replication": 1,
        "traceOutput": True,
        "duration": Parameter(
            "Duration", value=[DurationParameter(value=timedelta(hours=8))]
        ),
    }
)
elParams = ElementParameters(
    test_init.taskElement,
    parametersId="dsadsd",
    parametersGroup=[
        TimeParameters(
            test_init.taskElement,
            parameters=[
                Parameter(
                    parameterName="TransferTime",
                    resultRequest=[ResultType.MAX],
                    value=[NumericParameter(value=2, timeUnit="ms")],
                )
            ],
        )
    ],
)
_id = ObjectId()
dt = datetime(2020, 1, 9, 3, 43, 0)
scenario = Scenario(
    **{
        "_id": str(_id),
        "name": "sc1",
        "description": "Primo tentativo di fare uno scenario con le classi da me costruite",
        "created": dt,
        "modified": dt,
        "scenarioParameters": scenarioParameters,
        "elementParameters": [elParams],
    }
)
# print(etree.tostring(scenario.toXML(),encoding='unicode',pretty_print=True))
scenarioXML = f"""<bpsim:Scenario xmlns:bpsim="http://www.bpsim.org/schemas/2.0" id="{str(_id)}" name="sc1" description="Primo tentativo di fare uno scenario con le classi da me costruite" created="{dt.isoformat()[:19]}" modified="{dt.isoformat()[:19]}">
  <bpsim:ScenarioParameters replication="1" baseTimeUnit="ms" traceOutput="true">
    <bpsim:Duration>
      <bpsim:DurationParameter value="PT8H"/>
    </bpsim:Duration>
  </bpsim:ScenarioParameters>
  <bpsim:ElementParameters id="dsadsd" elementRef="Task_0yez3qj">
    <bpsim:TimeParameters>
      <bpsim:TransferTime>
        <bpsim:ResultRequest>max</bpsim:ResultRequest>
        <bpsim:NumericParameter value="2" timeUnit="ms"/>
      </bpsim:TransferTime>
    </bpsim:TimeParameters>
  </bpsim:ElementParameters>
</bpsim:Scenario>
"""
elementParameter = ElementParameters(
    test_init.taskElement,
    parametersId="mela",
    parametersGroup=[
        TimeParameters(
            test_init.taskElement,
            parameters=[
                Parameter(
                    parameterName="TransferTime",
                    resultRequest=[ResultType.MAX],
                    value=[NumericParameter(value=2, timeUnit="ms")],
                )
            ],
        )
    ],
)


class test_Scenario(unittest.TestCase):

    # def test_toXML(self):
    #     self.assertAlmostEqual(
    #         scenarioXML,
    #         etree.tostring(
    #             scenario.toXML(),
    #             encoding='unicode',
    #             pretty_print=True
    #         )
    #     )

    def test_elementParameter(self):
        self.assertEqual(
            elParams, scenario.elementParameter(parameterId="dsadsd")
        )
        # self.assertRaises(TypeError, scenario.elementParameter, 24)
        self.assertIsNone(scenario.elementParameter("sdadsa"))

    def test_addElementParameter(self):
        self.assertRaises(TypeError, scenario.addElementParameter, "mela")
        self.assertRaises(ValueError, scenario.addElementParameter, elParams)
        ##########
        scenario.addElementParameter(elementParameter)
        self.assertEqual(len(scenario.elementParameters), 2)

    def test_removeElementParameter(self):
        self.assertRaises(
            ValueError, scenario.removeElementparameter, "ambaraba"
        )
        prev_len = len(scenario.elementParameters)
        scenario.removeElementparameter(elParams.parametersId)
        self.assertLess(len(scenario.elementParameters), prev_len)

    def test_updateElementParameter(self):
        elementParameter.parametersId = elParams.parametersId
        scenario_updated = Scenario(
            **{
                "_id": str(_id),
                "name": "sc1",
                "description": "Primo tentativo di fare uno scenario con le classi da me costruite",
                "created": dt,
                "modified": dt,
                "scenarioParameters": scenarioParameters,
                "elementParameters": [elementParameter],
            }
        )
        scenario.updateElementParameter(elementParameter)
        self.assertEqual(scenario, scenario_updated)

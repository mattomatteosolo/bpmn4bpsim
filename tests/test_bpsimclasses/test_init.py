from os import path
from lxml import etree
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    StringParameter,
    FloatingParameter,
    BooleanParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.EnumParameter import EnumParameter
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)
from bpmn4bpsim.bpmnfacilities import BPMN_NAMESPACES

proj_dir = path.abspath(".")
testDir = path.join(proj_dir, "tests")
modelsDir = path.join(testDir, "data", "models")
with open(path.join(modelsDir, "simple.bpmn"), "r") as f:
    sempliceBPMN = etree.parse(f).getroot()

with open(path.join(modelsDir, "everythingRes.bpmn"), "r") as f:
    tutteCoseResBPMN = etree.parse(f).getroot()

with open(path.join(modelsDir, "everything.bpmn"), "r") as f:
    tutteCoseBPMN = etree.parse(f).getroot()

sempliceTaskId = "Task_0yez3qj"
startEventInEventSubProcessId = "StartEvent_0fj6ok1"
startEventId = "StartEvent_1y45yut"
processId = "Process_10fvunj"
subprocessWithDecompositionId = "SubProcess_19tbqz0"
boundaryEventId = "IntermediateThrowEvent_1xhqpgw"
resourceId = "Resource_1"
resourceRoleId = "ResourceRole_1"
taskElement = sempliceBPMN.find(
    f'.//task[@id="{sempliceTaskId}"]', BPMN_NAMESPACES
)
startEventElement = tutteCoseResBPMN.find(
    f'.//startEvent[@id="{startEventId}"]', BPMN_NAMESPACES
)
processElement = sempliceBPMN.find(
    f'.//process[@id="{processId}"]', BPMN_NAMESPACES
)
subprocessWithDecompositionElement = tutteCoseBPMN.find(
    f'.//subProcess[@id="{subprocessWithDecompositionId}"]', BPMN_NAMESPACES
)
boundaryEventElement = tutteCoseResBPMN.find(
    f'.//boundaryEvent[@id="{boundaryEventId}"]', BPMN_NAMESPACES
)
resourceElement = tutteCoseResBPMN.find(
    f'.//resource[@id="{resourceId}"]', BPMN_NAMESPACES
)
resourceRoleElement = tutteCoseResBPMN.find(
    f'.//resourceRole[@id="{resourceRoleId}"]', BPMN_NAMESPACES
)
if taskElement == None:
    raise ValueError()
if startEventElement == None:
    raise ValueError()
if processElement == None:
    raise ValueError()
if subprocessWithDecompositionElement == None:
    raise ValueError()
if boundaryEventElement == None:
    raise ValueError()
if resourceElement == None:
    raise ValueError()
if resourceRoleElement == None:
    raise ValueError()


numericValue = NumericParameter(2, timeUnit="ms")
stringValue = StringParameter(value="mela", validFor="s1")
floatingValue = FloatingParameter(value=2.5)
booleanValue = BooleanParameter(value=True)
expressionValue = ExpressionParameter(value="bpsim:getResource('mela')")

numericEnum = EnumParameter([numericValue, numericValue, numericValue])
stringEnum = EnumParameter([stringValue, stringValue])
numericStringEnum = EnumParameter([stringValue, numericValue])
floatingEnum = EnumParameter([floatingValue, floatingValue])
booleanEnum = EnumParameter([booleanValue, booleanValue])

unknownParamName = Parameter(parameterName="sddsda", value=[numericValue])

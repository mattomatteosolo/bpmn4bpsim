"""
Test per TimeParameters

Modulo di test per TimeParameters: Viene prima realizzato un test senza unittest. Per il futuro vedremo
"""
import unittest
from lxml import etree
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import TimeParameters
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    StringParameter,
)
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses import ParamNonValido


transferTime = Parameter(
    parameterName="TransferTime",
    resultRequest=[ResultType.MAX],
    value=[NumericParameter(value=2, timeUnit="ms")],
)
durationError = Parameter(
    parameterName="Duration",
    resultRequest=[ResultType.MAX],
    value=[NumericParameter(value=2, timeUnit="ms")],
)
duration = Parameter(
    parameterName="Duration", resultRequest=[ResultType.MAX, ResultType.MIN]
)
transferTimeResOnly = Parameter(
    parameterName="TransferTime",
    resultRequest=[ResultType.MIN, ResultType.MAX, ResultType.SUM],
)
transferTimeInvalidValues = Parameter(
    parameterName="TransferTime",
    resultRequest=[ResultType.SUM],
    value=[StringParameter(value="sdsa")],
)
unknownParamName = Parameter(
    parameterName="sddsda", value=[test_init.numericValue]
)
tParams = TimeParameters(test_init.taskElement, parameters=[transferTime])
tParamsXMLStr = '<bpsim:TimeParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0"><bpsim:TransferTime><bpsim:ResultRequest>max</bpsim:ResultRequest><bpsim:NumericParameter value="2" timeUnit="ms"/></bpsim:TransferTime></bpsim:TimeParameters>'


class test_TimeParameters(unittest.TestCase):
    def test_checkParameter(self):
        self.assertTrue(TimeParameters.checkParameter(transferTime))
        self.assertFalse(TimeParameters.checkParameter(unknownParamName))
        self.assertFalse(TimeParameters.checkParameter(2))

    def test_isValidResult(self):
        self.assertRaises(ParamNonValido, TimeParameters.isValidResult, 2)
        self.assertRaises(
            ParamNonValido, TimeParameters.isValidResult, unknownParamName
        )
        self.assertTrue(TimeParameters.isValidResult(transferTime))
        self.assertTrue(TimeParameters.isValidResult(duration))

    def test_parameterHasValidValue(self):
        self.assertRaises(
            ParamNonValido, TimeParameters.parameterHasValidValues, 2
        )
        self.assertRaises(
            ParamNonValido,
            TimeParameters.parameterHasValidValues,
            unknownParamName,
        )
        self.assertTrue(TimeParameters.parameterHasValidValues(transferTime))
        self.assertTrue(TimeParameters.parameterHasValidValues(duration))
        self.assertFalse(
            TimeParameters.parameterHasValidValues(transferTimeInvalidValues)
        )
        self.assertFalse(TimeParameters.parameterHasValidValues(durationError))

    # def test_initTypeCheck(self):
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement="sdasd",
    #         parameters=[transferTimeResOnly],
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters="dsda",
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=True,
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=1,
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=2.5,
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=dict(),
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=tuple(),
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=set(),
    #     )
    #     self.assertRaises(
    #         ValueError,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=list(),
    #     )
    #     self.assertRaises(
    #         ParamNonValido,
    #         TimeParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=[unknownParamName],
    #     )

    def test_isValidEnumTypeCheck(self):
        self.assertRaises(
            TypeError,
            TimeParameters.isValidEnum,
            parameterName="sdad",
            enumParameter=1,
        )
        self.assertRaises(
            TypeError,
            TimeParameters.isValidEnum,
            parameterName="TransferTime",
            enumParameter="sdad",
        )
        self.assertRaises(
            ValueError,
            TimeParameters.isValidEnum,
            parameterName="sdasd",
            enumParameter=test_init.numericEnum,
        )
        self.assertRaises(
            ValueError,
            TimeParameters.isValidEnum,
            parameterName="Duration",
            enumParameter=test_init.numericEnum,
        )

    def test_isValidEnum(self):
        self.assertTrue(
            TimeParameters.isValidEnum("TransferTime", test_init.numericEnum)
        )
        self.assertFalse(
            TimeParameters.isValidEnum("TransferTime", test_init.stringEnum)
        )
        self.assertFalse(
            TimeParameters.isValidEnum(
                "TransferTime", test_init.numericStringEnum
            )
        )

    def test_isApplicable(self):
        self.assertRaises(
            TypeError,
            TimeParameters.isApplicable,
            bpmElement="sda",
            parameter=transferTime,
        )
        self.assertRaises(
            TypeError,
            TimeParameters.isApplicable,
            bpmElement=2,
            parameter=transferTime,
        )
        self.assertTrue(
            TimeParameters.isApplicable(test_init.taskElement, transferTime)
        )
        self.assertFalse(
            TimeParameters.isApplicable(test_init.taskElement, durationError)
        )
        self.assertFalse(
            TimeParameters.isApplicable(
                test_init.startEventElement, transferTime
            )
        )
        self.assertFalse(
            TimeParameters.isApplicable(
                test_init.subprocessWithDecompositionElement, transferTime
            )
        )
        self.assertTrue(
            TimeParameters.isApplicable(
                test_init.processElement, transferTimeResOnly
            )
        )

    def test_parameter(self):
        param = tParams.parameter("TransferTime")
        self.assertEqual(param, transferTime)

    def test_toXML(self):
        self.assertEqual(
            etree.tostring(tParams.toXML(), encoding="unicode"), tParamsXMLStr
        )

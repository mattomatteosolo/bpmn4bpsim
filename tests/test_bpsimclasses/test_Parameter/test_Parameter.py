"""
Modulo di test per Parameter
"""
import unittest
import xmlunittest
from bpmn4bpsim.bpsimclasses.Parameter import (
    Parameter,
    ResultType,
    Property,
    PropertyType,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    FloatingParameter,
)

invalidValuesType = [NumericParameter(value=1, validFor="sw"), "mskdma", 2.5]
invalidValues = [NumericParameter(value=1), FloatingParameter(2.5)]
values = [NumericParameter(value=2), NumericParameter(value=1, validFor="l1")]

invalidResultsFromString = ["mela", "pera", "arancia"]
validResultsFromString = ["min", "max", "count"]
validResultsDuplicates = [ResultType.MIN, ResultType.MAX, ResultType.MAX]
validResults = [ResultType.MIN, ResultType.SUM]

parameter = Parameter(
    parameterName="TriggerCount",
    value=[NumericParameter(value=2), NumericParameter(value=1, validFor="s1")],
    resultRequest=[ResultType.MIN, ResultType.MAX],
)
parameterXML = """<bpsim:TriggerCount xmlns:bpsim="http://www.bpsim.org/schemas/2.0">
  <bpsim:ResultRequest>min</bpsim:ResultRequest>
  <bpsim:ResultRequest>max</bpsim:ResultRequest>
  <bpsim:NumericParameter validFor="s1" value="1"/>
  <bpsim:NumericParameter value="2"/>
</bpsim:TriggerCount>
"""

propertyParam = Property(
    propertyName="mela",
    propertyType=PropertyType.LONG,
    value=[NumericParameter(value=2), NumericParameter(value=1, validFor="s1")],
)
propertyXML = """<bpsim:Property xmlns:bpsim="http://www.bpsim.org/schemas/2.0" name="mela" type="long">
  <bpsim:NumericParameter value="2"/>
  <bpsim:NumericParameter validFor="s1" value="1"/>
</bpsim:Property>
"""


class test_Parameter(unittest.TestCase, xmlunittest.XmlTestMixin):
    def test_isValidValue(self):
        self.assertFalse(Parameter.isValidValue(invalidValuesType))
        self.assertFalse(Parameter.isValidValue(invalidValues))
        self.assertTrue(Parameter.isValidValue(values))
        self.assertFalse(Parameter.isValidValue([1, 2, 3]))
        self.assertTrue(Parameter.isValidValue([]))

    def test_isValidResult(self):
        self.assertFalse(Parameter.isValidResult([ResultType.MIN, "min", 2]))
        self.assertFalse(Parameter.isValidValue([1, 2]))
        self.assertFalse(Parameter.isValidResult(invalidResultsFromString))
        self.assertTrue(Parameter.isValidResult(validResultsFromString))
        self.assertTrue(Parameter.isValidResult([]))
        self.assertTrue(Parameter.isValidResult(validResultsDuplicates))
        self.assertTrue(Parameter.isValidResult(validResults))

    # def test_initTypeCheck(self):
        # self.assertRaises(ParamNonValido, Parameter, 'dsad', [], [])
        # self.assertRaises(TypeError, Parameter, 1, values, validResults)
        # self.assertRaises(TypeError, Parameter,
        #                   'TriggerCount', set(), validResults)
        # self.assertRaises(TypeError, Parameter,
        #                   'TriggerCount', dict(), validResults)
        # self.assertRaises(TypeError, Parameter,
        #                   'TriggerCount', 1, validResults)
        # self.assertRaises(TypeError, Parameter, 'TriggerCount', values, set())
        # self.assertRaises(TypeError, Parameter, 'TriggerCount', values, dict())
        # self.assertRaises(TypeError, Parameter, 'TriggerCount', values, 2.5)
        # self.assertRaises(
        #     ValueError, Parameter, "TriggerCount", invalidValues, validResults
        # )
        # self.assertRaises(
        #     ValueError,
        #     Parameter,
        #     "TriggerCount",
        #     values,
        #     invalidResultsFromString,
        # )

    # def test_toXML(self):
    #    self.assertXmlEquivalentOutputs(
    #        etree.tostring(parameter.toXML(),
    #                       encoding='unicode', pretty_print=True),
    #        parameterXML
    #    )


class test_Property(unittest.TestCase, xmlunittest.XmlTestMixin):
    def test_initTypeCheck(self):
        # self.assertRaises(TypeError, Property,
        # propertyName='mela', value=values, propertyType=1)
        self.assertRaises(
            ValueError,
            Property,
            propertyName="mela",
            value=values,
            propertyType="sdadss",
        )

    # def test_toXML(self):
    #     self.assertXmlEquivalentOutputs(
    #         etree.tostring(
    #             propertyParam.toXML(),
    #             pretty_print=True,
    #             encoding='unicode'
    #         ),
    #         propertyXML
    #     )

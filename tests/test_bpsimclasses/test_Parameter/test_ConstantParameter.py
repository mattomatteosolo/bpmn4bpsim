from datetime import datetime, timedelta
from lxml import etree
import unittest
from bpmn4bpsim.bpsimclasses.ParameterValues import ConstantParameter


##############################
# NumericParameter
##############################
numericXML = '<bpsim:NumericParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1" value="2" timeUnit="ms"/>'
##############################
# StringParameter
##############################
stringXML = '<bpsim:StringParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1" value="mela"/>'
##############################
# FloatinParameter
##############################
floatingXML = '<bpsim:FloatingParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1" value="9.5" timeUnit="ms"/>'
##############################
# BooleanParameter
##############################
booleanXML = '<bpsim:BooleanParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1" value="true"/>'
##############################
# DurationParameter
##############################
durationXML = '<bpsim:DurationParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1" value="P1D"/>'
##############################
# DateTimeParameter
##############################
datetimeXML = '<bpsim:DateTimeParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1" value="2019-08-01T02:22:11Z"/>'
dt = datetime(year=2019, month=8, day=1, hour=2, minute=22, second=11)


class test_NumericParameter(unittest.TestCase):
    def test_SetUpTypeChecking(self):
        # TypeError
        self.assertRaises(
            TypeError, ConstantParameter.NumericParameter, value=4, validFor=2
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.NumericParameter,
            value=4,
            validFor=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.NumericParameter,
            value="dsad",
            validFor="s1",
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.NumericParameter,
            value=True,
            validFor="s1",
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.NumericParameter,
            value=9.5,
            validFor="s1",
        )
        self.assertRaises(
            TypeError, ConstantParameter.NumericParameter, value=9.5, timeUnit=3
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.NumericParameter,
            value=9.5,
            timeUnit=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.NumericParameter,
            value=9.5,
            timeUnit=9.5,
        )
        # ValueError
        self.assertRaises(
            ValueError,
            ConstantParameter.NumericParameter,
            value=2,
            timeUnit="asds",
        )

    def test_toXML(self):
        param = ConstantParameter.NumericParameter(
            value=2, validFor="c1", timeUnit="ms"
        ).toXML()
        self.assertEqual(etree.tostring(param, encoding="unicode"), numericXML)


class test_StringParameter(unittest.TestCase):
    def test_SetUpTypeChecking(self):
        self.assertRaises(TypeError, ConstantParameter.StringParameter, value=1)
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=1.5
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=True
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=[]
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=dict()
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=()
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=3,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=dict(),
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=[],
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=2.5,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=(),
        )

    def test_toXML(self):
        param = ConstantParameter.StringParameter(
            value="mela", validFor="c1"
        ).toXML()
        self.assertEqual(etree.tostring(param, encoding="unicode"), stringXML)


class test_FloatingParameter(unittest.TestCase):
    def test_SetUpTypeChecking(self):
        # TypeError
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=9.5,
            validFor=2,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=9.5,
            validFor=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value="dsad",
            validFor="s1",
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=True,
            validFor="s1",
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=2,
            validFor="s1",
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=9.5,
            timeUnit=3,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=9.5,
            timeUnit=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.FloatingParameter,
            value=9.5,
            timeUnit=9.5,
        )
        # ValueError
        self.assertRaises(
            ValueError,
            ConstantParameter.FloatingParameter,
            value=9.4,
            timeUnit="asds",
        )

    def test_toXML(self):
        param = ConstantParameter.FloatingParameter(
            value=9.5, validFor="c1", timeUnit="ms"
        ).toXML()
        self.assertEqual(etree.tostring(param, encoding="unicode"), floatingXML)


class test_BooleanParameter(unittest.TestCase):
    def test_SetUpTypeChecking(self):
        self.assertRaises(
            TypeError, ConstantParameter.BooleanParameter, value=1
        )
        self.assertRaises(
            TypeError, ConstantParameter.BooleanParameter, value=1.5
        )
        self.assertRaises(
            TypeError, ConstantParameter.BooleanParameter, value="sdad"
        )
        self.assertRaises(
            TypeError, ConstantParameter.BooleanParameter, value=[]
        )
        self.assertRaises(
            TypeError, ConstantParameter.BooleanParameter, value=dict()
        )
        self.assertRaises(
            TypeError, ConstantParameter.BooleanParameter, value=()
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.BooleanParameter,
            value=True,
            validFor=3,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.BooleanParameter,
            value=True,
            validFor=dict(),
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.BooleanParameter,
            value=True,
            validFor=[],
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.BooleanParameter,
            value=True,
            validFor=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.BooleanParameter,
            value=True,
            validFor=2.5,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.BooleanParameter,
            value=True,
            validFor=(),
        )

    def test_toXML(self):
        param = ConstantParameter.BooleanParameter(
            value=True, validFor="c1"
        ).toXML()
        self.assertEqual(etree.tostring(param, encoding="unicode"), booleanXML)


class test_DurationParameter(unittest.TestCase):
    def test_SetUpTypeChecking(self):
        self.assertRaises(
            TypeError, ConstantParameter.DurationParameter, value=1
        )
        self.assertRaises(
            TypeError, ConstantParameter.DurationParameter, value=1.5
        )
        self.assertRaises(
            TypeError, ConstantParameter.DurationParameter, value=True
        )
        self.assertRaises(
            TypeError, ConstantParameter.DurationParameter, value=[]
        )
        self.assertRaises(
            TypeError, ConstantParameter.DurationParameter, value=dict()
        )
        self.assertRaises(
            TypeError, ConstantParameter.DurationParameter, value=()
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.DurationParameter,
            value=timedelta(days=1),
            validFor=3,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.DurationParameter,
            value=timedelta(days=1),
            validFor=dict(),
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.DurationParameter,
            value=timedelta(days=1),
            validFor=[],
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.DurationParameter,
            value=timedelta(days=1),
            validFor=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.DurationParameter,
            value=timedelta(days=1),
            validFor=2.5,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.DurationParameter,
            value=timedelta(days=1),
            validFor=(),
        )

    def test_toXML(self):
        param = ConstantParameter.DurationParameter(
            value=timedelta(days=1), validFor="c1"
        ).toXML()
        self.assertEqual(etree.tostring(param, encoding="unicode"), durationXML)


class test_DateTimeParameter(unittest.TestCase):
    def test_SetUpTypeChecking(self):
        self.assertRaises(TypeError, ConstantParameter.StringParameter, value=1)
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=1.5
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=True
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=[]
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=dict()
        )
        self.assertRaises(
            TypeError, ConstantParameter.StringParameter, value=()
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=3,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=dict(),
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=[],
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=True,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=2.5,
        )
        self.assertRaises(
            TypeError,
            ConstantParameter.StringParameter,
            value="mela",
            validFor=(),
        )

    def test_toXML(self):
        param = ConstantParameter.DateTimeParameter(
            value=dt, validFor="c1"
        ).toXML()
        self.assertEqual(etree.tostring(param, encoding="unicode"), datetimeXML)

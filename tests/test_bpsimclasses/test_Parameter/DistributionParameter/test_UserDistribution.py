"""
Modulo per il test delle sottoclassi di DistributionParmeter
"""
import unittest
from lxml import etree
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter import (
    UserDistribution,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    NumericParameter,
    FloatingParameter,
    StringParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.EnumParameter import EnumParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)

point = UserDistribution.UserDistributionDataPoint(
    probability=0.3, value=NumericParameter(3)
).toXML()

numericXML = '<bpsim:NumericParameter value="1"/>'
floatingXML = '<bpsim:FloatingParameter value="2.5"/>'
stringXML = '<bpsim:StringParameter value="mela"/>'
enumXML = f"<bpsim:EnumParameter>{numericXML}{floatingXML}{stringXML}</bpsim:EnumParameter>"
expressionXML = (
    f"<bpsim:ExpressionParameter value=\"bpsim:getProperty('mela')\"/>"
)

pointXML = f'<bpsim:UserDistributionDataPoint xmlns:bpsim="http://www.bpsim.org/schemas/2.0" probability="0.3"><bpsim:NumericParameter value="3"/></bpsim:UserDistributionDataPoint>'
pointEnumXMLStr = f'<bpsim:UserDistributionDataPoint xmlns:bpsim="http://www.bpsim.org/schemas/2.0" probability="0.5">{enumXML}</bpsim:UserDistributionDataPoint>'
pointExpressionXMLStr = f'<bpsim:UserDistributionDataPoint xmlns:bpsim="http://www.bpsim.org/schemas/2.0" probability="0.2">{expressionXML}</bpsim:UserDistributionDataPoint>'

usrDDP_numeric = UserDistribution.UserDistributionDataPoint(
    probability=0.3, value=NumericParameter(value=3)
)
usrDDP_enum = UserDistribution.UserDistributionDataPoint(
    probability=0.5,
    value=EnumParameter(
        paramList=[
            NumericParameter(value=1),
            FloatingParameter(value=2.5),
            StringParameter(value="mela"),
        ]
    ),
)
usrDDP_expr = UserDistribution.UserDistributionDataPoint(
    probability=0.2,
    value=ExpressionParameter(value="bpsim:getProperty('mela')"),
)


class test_UserDistributionDataPoint(unittest.TestCase):
    def test_typeChecking(self):
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability="sda",
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=1,
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=True,
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=list(),
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=dict(),
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=tuple(),
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=set(),
            value=NumericParameter(1),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=0.5,
            value=dict(),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=0.5,
            value=list(),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=0.5,
            value=2,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            probability=0.5,
            value=True,
        )
        self.assertRaises(
            ValueError,
            UserDistribution.UserDistributionDataPoint,
            probability=-0.5,
            value=NumericParameter(1),
        )
        self.assertRaises(
            ValueError,
            UserDistribution.UserDistributionDataPoint,
            probability=1.2,
            value=NumericParameter(1),
        )

    def test_toXML(self):
        self.assertAlmostEqual(
            etree.tostring(point, encoding="unicode"), pointXML
        )


points = [usrDDP_numeric, usrDDP_enum, usrDDP_expr]
nonValidPoint = [3, usrDDP_enum, usrDDP_expr]
pointsTooMuch = [usrDDP_numeric, usrDDP_numeric, usrDDP_enum, usrDDP_expr]
pointsTooLittle = [usrDDP_enum, usrDDP_expr]
usrDistributionJSON = {
    "discrete": True,
    "points": [usrDDP_numeric, usrDDP_enum, usrDDP_expr],
}
usrDistributionJSONKey = {"discrete": True, "points": dict()}

pointXML2 = f'<bpsim:UserDistributionDataPoint probability="0.3"><bpsim:NumericParameter value="3"/></bpsim:UserDistributionDataPoint>'
pointEnumXMLStr2 = f'<bpsim:UserDistributionDataPoint probability="0.5">{enumXML}</bpsim:UserDistributionDataPoint>'
pointExpressionXMLStr2 = f'<bpsim:UserDistributionDataPoint probability="0.2">{expressionXML}</bpsim:UserDistributionDataPoint>'
usrDistrXML = f'<bpsim:UserDistribution xmlns:bpsim="http://www.bpsim.org/schemas/2.0" discrete="true">{pointXML2}{pointEnumXMLStr2}{pointExpressionXMLStr2}</bpsim:UserDistribution>'

usrDistribution = UserDistribution.UserDistribution(
    points=points, discrete=True
).toXML()


class test_UserDistribution(unittest.TestCase):
    def test_typeChecking(self):
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistribution,
            discrete="sda",
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=1,
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=True,
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=list(),
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=dict(),
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=tuple(),
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=set(),
            points=points,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=True,
            points=dict(),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=True,
            points=list(),
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=True,
            points=2,
        )
        self.assertRaises(
            TypeError,
            UserDistribution.UserDistributionDataPoint,
            discrete=True,
            points=True,
        )
        self.assertRaises(
            ValueError,
            UserDistribution.UserDistribution,
            discrete=True,
            points=nonValidPoint,
        )
        self.assertRaises(
            ValueError,
            UserDistribution.UserDistribution,
            discrete=True,
            points=pointsTooMuch,
        )
        self.assertRaises(
            ValueError,
            UserDistribution.UserDistribution,
            discrete=True,
            points=pointsTooLittle,
        )

    def test_toXML(self):
        self.assertAlmostEqual(
            etree.tostring(usrDistribution, encoding="unicode"), usrDistrXML
        )

from lxml import etree
import unittest
from bpmn4bpsim.bpsimclasses.ParameterValues.EnumParameter import EnumParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    FloatingParameter,
    StringParameter,
)

paramList = [
    NumericParameter(1),
    FloatingParameter(2.5),
    StringParameter("mela"),
]

numericXML = '<bpsim:NumericParameter value="1"/>'
floatingXML = '<bpsim:FloatingParameter value="2.5"/>'
stringXML = '<bpsim:StringParameter value="mela"/>'
enumXML = f'<bpsim:EnumParameter xmlns:bpsim="http://www.bpsim.org/schemas/2.0" validFor="c1">{numericXML}{floatingXML}{stringXML}</bpsim:EnumParameter>'


class test_EnumParameter(unittest.TestCase):
    def test_initTypeChecking(self):
        self.assertRaises(TypeError, EnumParameter, paramList=set())
        self.assertRaises(TypeError, EnumParameter, paramList=dict())
        self.assertRaises(TypeError, EnumParameter, paramList=tuple())
        self.assertRaises(ValueError, EnumParameter, paramList=list())
        self.assertRaises(TypeError, EnumParameter, paramList=1)
        self.assertRaises(TypeError, EnumParameter, paramList=True)
        self.assertRaises(TypeError, EnumParameter, paramList=9.5)
        self.assertRaises(TypeError, EnumParameter, paramList="sdasd")
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=set()
        )
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=dict()
        )
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=list()
        )
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=tuple()
        )
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=1
        )
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=True
        )
        self.assertRaises(
            TypeError, EnumParameter, paramList=paramList, validFor=2.5
        )

    def test_toXML(self):
        enum = EnumParameter(paramList=paramList, validFor="c1").toXML()
        self.assertAlmostEqual(
            etree.tostring(enum, encoding="unicode"), enumXML
        )

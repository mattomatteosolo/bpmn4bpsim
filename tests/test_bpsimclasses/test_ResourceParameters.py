import unittest
from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.ParameterGroups.ResourceParameters import (
    ResourceParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType

selection = Parameter("Selection", value=[test_init.stringValue])
availability = Parameter("Availability", value=[test_init.booleanValue])
quantity = Parameter("Quantity", value=[test_init.numericValue])
role1 = Parameter("Role", value=[test_init.stringValue])
role2 = Parameter("Role", value=[test_init.StringParameter("cuoco")])
role3 = Parameter("Role", value=[test_init.StringParameter("capo chef")])
selectionInvalidValue = Parameter("Selection", value=[test_init.floatingValue])
selectionResult = Parameter(
    "Selection",
    value=[test_init.stringValue],
    resultRequest=[ResultType.MIN, ResultType.MAX],
)
selectionInvalidResult = Parameter(
    "Selection",
    value=[test_init.stringValue],
    resultRequest=[ResultType.MIN, ResultType.MAX, ResultType.SUM],
)
roleInvalidResult = Parameter(
    "Role", value=[test_init.stringValue], resultRequest=[ResultType.MIN]
)
quantityInvalidrResult = Parameter(
    "Quantity", value=[test_init.numericValue], resultRequest=[ResultType.MIN]
)
availabilityInvalidrResult = Parameter(
    "Availability",
    value=[test_init.booleanValue],
    resultRequest=[ResultType.MIN],
)

resourceParams = ResourceParameters(
    test_init.resourceElement, parameters=[availability, role2, role3, quantity]
)
resourceXML = """<bpsim:ResourceParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0">
  <bpsim:Availability>
    <bpsim:BooleanParameter value="true"/>
  </bpsim:Availability>
  <bpsim:Quantity>
    <bpsim:NumericParameter value="2" timeUnit="ms"/>
  </bpsim:Quantity>
  <bpsim:Role>
    <bpsim:StringParameter value="cuoco"/>
  </bpsim:Role>
  <bpsim:Role>
    <bpsim:StringParameter value="capo chef"/>
  </bpsim:Role>
</bpsim:ResourceParameters>
"""


class test_PriorityParameters(unittest.TestCase):
    def test_chekParameter(self):
        self.assertTrue(ResourceParameters.checkParameter(selection))
        self.assertTrue(ResourceParameters.checkParameter(availability))
        self.assertTrue(ResourceParameters.checkParameter(quantity))
        self.assertTrue(ResourceParameters.checkParameter(role1))
        self.assertFalse(
            ResourceParameters.checkParameter(test_init.unknownParamName)
        )

    def test_isValidResult(self):
        self.assertTrue(ResourceParameters.isValidResult(selection))
        self.assertTrue(ResourceParameters.isValidResult(availability))
        self.assertTrue(ResourceParameters.isValidResult(quantity))
        self.assertTrue(ResourceParameters.isValidResult(role2))
        self.assertTrue(ResourceParameters.isValidResult(selectionResult))
        self.assertFalse(
            ResourceParameters.isValidResult(selectionInvalidResult)
        )
        self.assertFalse(ResourceParameters.isValidResult(roleInvalidResult))
        self.assertFalse(
            ResourceParameters.isValidResult(quantityInvalidrResult)
        )
        self.assertFalse(
            ResourceParameters.isValidResult(availabilityInvalidrResult)
        )

    def test_isValidEnum(self):
        self.assertFalse(
            ResourceParameters.isValidEnum("Selection", test_init.floatingEnum)
        )
        self.assertTrue(
            ResourceParameters.isValidEnum("Selection", test_init.stringEnum)
        )

    def test_isValidDistribution(self):
        pass

    def test_parameterHasValidValues(self):
        self.assertTrue(ResourceParameters.parameterHasValidValues(selection))
        self.assertTrue(
            ResourceParameters.parameterHasValidValues(availability)
        )
        self.assertTrue(ResourceParameters.parameterHasValidValues(quantity))
        self.assertTrue(ResourceParameters.parameterHasValidValues(role1))
        self.assertFalse(
            ResourceParameters.parameterHasValidValues(selectionInvalidValue)
        )

    def test_isApplicable(self):
        self.assertTrue(
            ResourceParameters.isApplicable(
                test_init.resourceRoleElement, selection
            )
        )
        self.assertTrue(
            ResourceParameters.isApplicable(
                test_init.resourceElement, availability
            )
        )
        self.assertFalse(
            ResourceParameters.isApplicable(
                test_init.resourceElement, selection
            )
        )
        self.assertFalse(
            ResourceParameters.isApplicable(
                test_init.resourceRoleElement, availability
            )
        )
        self.assertFalse(
            ResourceParameters.isApplicable(
                test_init.resourceRoleElement, quantity
            )
        )
        self.assertFalse(
            ResourceParameters.isApplicable(
                test_init.resourceRoleElement, role1
            )
        )

    def test_initTypeCheck(self):
        self.assertRaises(
            ParamNonValido,
            ResourceParameters,
            test_init.subprocessWithDecompositionElement,
            [role1, role2, quantity],
        )
        self.assertRaises(TypeError, ResourceParameters, "dadaas", [role1])
        # self.assertRaises(
        #     TypeError, ResourceParameters, test_init.resourceRoleElement, dict()
        # )
        # self.assertRaises(
        #     TypeError, ResourceParameters, test_init.resourceElement, 1
        # )
        self.assertRaises(
            ValueError, ResourceParameters, test_init.resourceElement, []
        )

    def test_toXML(self):
        self.assertEqual(
            etree.tostring(
                resourceParams.toXML(), encoding="unicode", pretty_print=True
            ),
            resourceXML,
        )

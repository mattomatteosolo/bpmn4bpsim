"""Modulo per il test di CostParameters
"""
import unittest
from lxml import etree
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.ParameterGroups.CostParameters import (
    CostParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType

costParamsXML = '<bpsim:CostParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0"><bpsim:FixedCost><bpsim:NumericParameter value="2" timeUnit="ms"/></bpsim:FixedCost><bpsim:UnitCost><bpsim:ResultRequest>sum</bpsim:ResultRequest><bpsim:FloatingParameter value="2.5"/></bpsim:UnitCost></bpsim:CostParameters>'
unitCost = Parameter(
    "UnitCost",
    value=[test_init.FloatingParameter(value=2.5)],
    resultRequest=[ResultType.SUM],
)
fixedCost = Parameter("FixedCost", value=[test_init.numericValue])
unitCostInvalidValue = Parameter(
    "UnitCost",
    value=[test_init.BooleanParameter(value=True)],
    resultRequest=[ResultType.SUM],
)
fixedCostInvalidValue = Parameter("FixedCost", value=[test_init.booleanValue])
costParams = CostParameters(test_init.taskElement, [unitCost, fixedCost])


class test_CostParameters(unittest.TestCase):
    def test_checkParameter(self):
        pass

    def test_isValidResult(self):
        self.assertTrue(
            CostParameters.isValidResult(
                Parameter("FixedCost", resultRequest=[ResultType.SUM])
            )
        )
        self.assertTrue(
            CostParameters.isValidResult(
                Parameter("UnitCost", resultRequest=[ResultType.SUM])
            )
        )
        self.assertFalse(
            CostParameters.isValidResult(
                Parameter("UnitCost", resultRequest=[ResultType.COUNT])
            )
        )

    def test_isValidEnum(self):
        self.assertTrue(
            CostParameters.isValidEnum("FixedCost", test_init.floatingEnum)
        )
        self.assertTrue(
            CostParameters.isValidEnum("UnitCost", test_init.numericEnum)
        )
        self.assertFalse(
            CostParameters.isValidEnum("FixedCost", test_init.numericStringEnum)
        )
        self.assertFalse(
            CostParameters.isValidEnum("UnitCost", test_init.stringEnum)
        )

    # def test_isValidDistribution(self):
    #     pass

    def test_paramHasValidValues(self):
        self.assertTrue(CostParameters.parameterHasValidValues(unitCost))
        self.assertTrue(CostParameters.parameterHasValidValues(fixedCost))
        self.assertFalse(
            CostParameters.parameterHasValidValues(unitCostInvalidValue)
        )
        self.assertFalse(
            CostParameters.parameterHasValidValues(fixedCostInvalidValue)
        )

    def test_isApplicable(self):
        self.assertTrue(
            CostParameters.isApplicable(test_init.taskElement, fixedCost)
        )
        self.assertFalse(
            CostParameters.isApplicable(test_init.startEventElement, fixedCost)
        )
        self.assertFalse(
            CostParameters.isApplicable(
                test_init.boundaryEventElement, fixedCostInvalidValue
            )
        )
        self.assertTrue(
            CostParameters.isApplicable(test_init.resourceElement, unitCost)
        )

    def test_initTypeCheck(self):
        # self.assertRaises(TypeError, CostParameters, "dasd", [fixedCost])
        # self.assertRaises(
        #     TypeError, CostParameters, test_init.taskElement, "dad"
        # )
        # self.assertRaises(
        #     TypeError, CostParameters, test_init.taskElement, dict()
        # )
        self.assertRaises(
            ValueError, CostParameters, test_init.taskElement, list()
        )

    def test_toXML(self):
        self.assertEqual(
            etree.tostring(costParams.toXML(), encoding="unicode"),
            costParamsXML,
        )

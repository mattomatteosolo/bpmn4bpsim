from bson import ObjectId
from datetime import datetime
from lxml import etree
from os import path
import unittest
from bpmn4bpsim.bpsimclasses.Modello import Modello
from bpmn4bpsim.bpsimclasses.Scenario import Scenario
from bpmn4bpsim.bpsimclasses.ParameterGroups.CostParameters import (
    CostParameters,
)
from bpmn4bpsim.bpsimclasses.ParameterGroups.TimeParameters import (
    TimeParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from tests.test_bpsimclasses.test_Scenario import scenario, _id
from tests.test_bpsimclasses.test_ScenarioParameters import scenarioParameters
from tests.test_bpsimclasses import test_init

parser = etree.XMLParser(remove_blank_text=True)
proj_dir = path.abspath(".")
testDir = path.join(proj_dir, "tests")
modelsDir = path.join(testDir, "data", "models")
schemaDir = path.join(testDir, "data", "schemas")
with open(path.join(modelsDir, "simple.bpmn")) as f:
    sempliceXML = etree.parse(f, parser=parser).getroot()

with open(path.join(schemaDir, "Semantic.xsd")) as f:
    schemaDoc = etree.parse(f)
with open(path.join(schemaDir, "BPMN.xsd")) as f:
    bpmnDoc = etree.parse(f)
schema = etree.XMLSchema(bpmnDoc)
schema.assert_(sempliceXML)


class test_Modello(unittest.TestCase):
    @classmethod
    def setUp(cls):
        transferTime = Parameter(
            parameterName="TransferTime",
            resultRequest=[ResultType.MAX],
            value=[NumericParameter(value=2, timeUnit="ms")],
        )
        tParams = TimeParameters(
            test_init.taskElement, parameters=[transferTime]
        )
        cls.elParams = ElementParameters(
            test_init.taskElement,
            parametersId="dsadsd",
            parametersGroup=[tParams],
        )
        cls.model = Modello(sempliceXML, "sei un culo", "mela", [scenario])
        cls.model2 = Modello(sempliceXML, "sei un culo", "mela", [scenario])
        cls.scenari = [scenario]
        cls.scenarioId = ObjectId()
        dt = datetime(2020, 1, 9, 3, 43, 0)
        cls.scenarioDict = {
            "_id": str(cls.scenarioId),
            "name": "sc1",
            "description": "Primo tentativo di fare uno scenario con le classi da me costruite",
            "created": dt,
            "modified": dt,
            "scenarioParameters": scenarioParameters,
            "elementParameters": [cls.elParams],
        }
        cls.scenarioDict2 = {
            "_id": str(cls.scenarioId),
            "name": "scenario1",
            "description": "Scenario da cambiare",
            "created": dt,
            "modified": dt,
            "scenarioParameters": scenarioParameters,
            "elementParameters": [cls.elParams],
        }
        cls.scenario2 = Scenario(**cls.scenarioDict)
        cls.scenario3 = Scenario(**cls.scenarioDict2)

    # def test_init(self):
    #     self.assertRaises(
    #         TypeError, Modello, "sdda", "dsada", "dsadsh", [scenario]
    #     )
    #     self.assertRaises(
    #         TypeError, Modello, sempliceXML, "sddasd", "dsasda", scenario
    #     )
    #     self.assertRaises(
    #         ValueError, Modello, sempliceXML, "dassda", "sdada", [1, 2, 3]
    #     )
    #     self.assertRaises(
    #         TypeError, Modello, sempliceXML, 1, "sdads", [scenario]
    #     )

    def test_scenario(self):
        self.assertEqual(self.model.scenario(str(_id)), scenario)

    # def test_scenari(self):
    #     for i in range(len(self.scenari)):
    #         self.assertEqual(
    #             etree.tostring(self.scenari[i].toXML()),
    #             etree.tostring(self.model.scenari[i].toXML()),
    #         )

    def test_modello(self):
        self.assertEqual(
            etree.tostring(self.model.modello, encoding="unicode"),
            etree.tostring(sempliceXML, encoding="unicode"),
        )

    # def test_addScenario(self):
    #     beforeLen = len(model2.scenari)
    #     model2.addScenario(scenario2)
    #     afterLen = len(model2.scenari)
    #     self.assertGreater(afterLen,beforeLen)
    #     self.assertRaises(ValueError,model2.addScenario,scenarioJSON)
    #     afterAfterLen = len(model2.scenari)
    #     # self.assertEqual(afterAfterLen,afterLen)
    #     self.assertGreater(afterAfterLen,beforeLen)

    def test_updateIdScenario(self):
        model = Modello(
            modello=sempliceXML,
            nomeModello="Modello",
            _id="mod1",
            scenari=[self.scenario2, self.scenario3],
        )
        model.updateIdScenario(str(self.scenarioId), "pera")
        scenario = model.scenario("pera")
        if scenario == None:
            raise ValueError()
        self.assertEqual(scenario._id, "pera")

    def test_updateScenario(self):
        model = Modello(
            modello=sempliceXML,
            nomeModello="Modello",
            _id="mod1",
            scenari=[scenario, self.scenario2],
        )
        model.updateScenario(self.scenario3)
        self.assertEqual(model.scenario(str(self.scenarioId)), self.scenario3)

    # def test_addElementParameterFromJSON(self):
    # model2.addElementParameter(scenario._id, elParamsJSON)
    # self.assertEqual(
    #     len(model2.scenario(scenario._id).elementParameters), 2)

    def test_removeScenario(self):
        beforeLen = len(self.model2.scenari)
        self.model2.removeScenario(scenario._id)
        afterLen = len(self.model2.scenari)
        self.assertLess(afterLen, beforeLen)

    def test_addGroupParameter(self):
        model = Modello(
            modello=sempliceXML,
            nomeModello="sei un culo",
            _id="mela",
            scenari=[scenario],
        )
        cost_parameters = CostParameters(
            bpmElement=test_init.taskElement,
            parameters=[
                Parameter(
                    parameterName="UnitCost", value=[NumericParameter(value=2)]
                )
            ],
        )
        sc = model.scenario(scenario._id)
        if sc == None:
            raise ValueError()
        elParams = sc.elementParameter(self.elParams.parametersId)
        if elParams == None:
            raise ValueError()
        prevLen = len(elParams.parametersGroup)
        model.addGroupParameter(
            scenario._id, self.elParams.parametersId, cost_parameters
        )
        sc2 = self.model2.scenario(scenario._id)
        if sc2 == None:
            raise ValueError()
        elParams2 = sc2.elementParameter(self.elParams.parametersId)
        if elParams2 == None:
            raise ValueError()
        nextLen = len(elParams2.parametersGroup)
        self.assertGreater(
            nextLen,
            prevLen,
        )

    def test_removeGroupParameter(self):
        # cost_parameters = CostParameters(
        #     bpmElement=test_init.taskElement,
        #     parameters=[
        #         Parameter(
        #             parameterName='UnitCost',
        #             value=[
        #                 NumericParameter(value=2)
        #             ]
        #         )
        #     ]
        # )
        # scenario.elementParameters[0].addGroupParameter(cost_parameters)
        model = Modello(
            modello=sempliceXML,
            nomeModello="sei un culo",
            _id="mela",
            scenari=[scenario],
        )
        sc = model.scenario(scenario._id)
        if sc == None:
            raise ValueError()
        elParams = sc.elementParameters
        if elParams == None:
            raise ValueError()
        prev_len = len(elParams[0].parametersGroup)
        elParams[0].removeGroupParameter("CostParameters")
        self.assertLess(
            len(elParams[0].parametersGroup),
            prev_len,
        )

    def test_updateGroupParameter(self):
        model = Modello(
            modello=sempliceXML,
            nomeModello="sei un culo",
            _id="mela",
            scenari=[scenario],
        )
        time_parameters = TimeParameters(
            bpmElement=test_init.taskElement,
            parameters=[
                Parameter(
                    parameterName="SetUpTime",
                    value=[NumericParameter(value=2)],
                )
            ],
        )
        if scenario.elementParameters == None:
            raise ValueError()
        model.updateGroupParameter(
            idScenario=scenario._id,
            idElementParameter=scenario.elementParameters[0].parametersId,
            groupParameter=time_parameters,
        )
        sc = model.scenario(scenario._id)
        if sc == None:
            raise ValueError()
        elParams = sc.elementParameters
        if elParams == None:
            raise ValueError()
        self.assertEqual(
            time_parameters,
            elParams[0].parameterGroup("TimeParameters"),
        )

    def test_toXML(self):
        print(
            etree.tostring(
                self.model.toXML([str(_id)]),
                encoding="unicode",
                pretty_print=True,
            )
        )

        print(
            etree.tostring(
                self.model.toXML([]), pretty_print=True, encoding="unicode"
            )
        )

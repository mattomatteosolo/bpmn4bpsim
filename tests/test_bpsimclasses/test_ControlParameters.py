"""
Modulo per il test di ControlParameters
"""
import unittest
from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.ParameterGroups.ControlParameters import (
    ControlParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType

controlXMLstr = """<bpsim:ControlParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0">
  <bpsim:InterTriggerTimer>
    <bpsim:ResultRequest>min</bpsim:ResultRequest>
    <bpsim:FloatingParameter value="2.5"/>
  </bpsim:InterTriggerTimer>
  <bpsim:TriggerCount>
    <bpsim:ResultRequest>count</bpsim:ResultRequest>
    <bpsim:NumericParameter value="2"/>
  </bpsim:TriggerCount>
</bpsim:ControlParameters>
"""

interTriggerTimer = Parameter(
    "InterTriggerTimer",
    value=[test_init.FloatingParameter(value=2.5)],
    resultRequest=[ResultType.MIN],
)
triggerCount = Parameter(
    "TriggerCount",
    value=[test_init.NumericParameter(value=2)],
    resultRequest=[ResultType.COUNT],
)
condition = Parameter(
    "Condition", value=[test_init.BooleanParameter(value=True)]
)
probability = Parameter("Probability", value=[test_init.floatingValue])
interTriggerResultOnly = Parameter(
    "InterTriggerTimer", resultRequest=[ResultType.MIN, ResultType.MAX]
)
interTriggerTimerValueError = Parameter(
    "InterTriggerTimer",
    value=[test_init.FloatingParameter(value=2.5), test_init.stringValue],
    resultRequest=[ResultType.MIN],
)
triggerCountValueError = Parameter(
    "TriggerCount",
    value=[
        test_init.NumericParameter(value=2),
        test_init.BooleanParameter(value=True, validFor="cscs"),
    ],
    resultRequest=[ResultType.COUNT],
)
controlParams = ControlParameters(
    test_init.startEventElement, parameters=[triggerCount, interTriggerTimer]
)


class test_ControlParameters(unittest.TestCase):
    def test_checkParameter(self):
        self.assertTrue(ControlParameters.checkParameter(interTriggerTimer))
        self.assertTrue(ControlParameters.checkParameter(triggerCount))
        self.assertFalse(
            ControlParameters.checkParameter(test_init.unknownParamName)
        )
        self.assertFalse(ControlParameters.checkParameter("dsas"))
        self.assertFalse(ControlParameters.checkParameter(dict()))

    def test_isValidResult(self):
        self.assertTrue(
            ControlParameters.isValidResult(
                Parameter(
                    "InterTriggerTimer",
                    resultRequest=[ResultType.MIN, ResultType.MAX],
                )
            )
        )
        self.assertFalse(
            ControlParameters.isValidResult(
                Parameter(
                    "InterTriggerTimer",
                    resultRequest=[ResultType.MIN, ResultType.COUNT],
                )
            )
        )
        self.assertFalse(
            ControlParameters.isValidResult(
                Parameter("Probability", resultRequest=[ResultType.MIN])
            )
        )
        self.assertFalse(
            ControlParameters.isValidResult(
                Parameter(
                    "Probability",
                    value=[test_init.floatingValue],
                    resultRequest=[ResultType.MIN],
                )
            )
        )

    def test_isValidEnum(self):
        self.assertTrue(
            ControlParameters.isValidEnum(
                parameterName="InterTriggerTimer",
                enumParameter=test_init.numericEnum,
            )
        )
        self.assertFalse(
            ControlParameters.isValidEnum(
                "InterTriggerTimer", test_init.stringEnum
            )
        )
        self.assertFalse(
            ControlParameters.isValidEnum(
                "TriggerCount", test_init.floatingEnum
            )
        )
        self.assertFalse(
            ControlParameters.isValidEnum(
                "InterTriggerTimer", test_init.numericStringEnum
            )
        )
        self.assertTrue(
            ControlParameters.isValidEnum(
                "InterTriggerTimer", test_init.floatingEnum
            )
        )
        self.assertTrue(
            ControlParameters.isValidEnum("Condition", test_init.booleanEnum)
        )

    # def test_isValidDistribution(self):
    #     pass

    def test_parameterHasValidValues(self):
        self.assertTrue(
            ControlParameters.parameterHasValidValues(interTriggerTimer)
        )
        self.assertTrue(
            ControlParameters.parameterHasValidValues(interTriggerResultOnly)
        )
        self.assertTrue(ControlParameters.parameterHasValidValues(triggerCount))
        self.assertFalse(
            ControlParameters.parameterHasValidValues(
                interTriggerTimerValueError
            )
        )
        self.assertFalse(
            ControlParameters.parameterHasValidValues(triggerCountValueError)
        )

    def test_isApplicable(self):
        self.assertTrue(
            ControlParameters.isApplicable(
                test_init.processElement,
                (Parameter("TriggerCount", resultRequest=[ResultType.COUNT])),
            )
        )
        self.assertFalse(
            ControlParameters.isApplicable(
                test_init.processElement,
                (
                    Parameter(
                        "InterTriggerTimer", resultRequest=[ResultType.COUNT]
                    )
                ),
            )
        )
        self.assertFalse(
            ControlParameters.isApplicable(
                test_init.processElement,
                (
                    Parameter(
                        "InterTriggerTimer", resultRequest=[ResultType.MIN]
                    )
                ),
            )
        )
        self.assertFalse(
            ControlParameters.isApplicable(
                test_init.processElement,
                (
                    Parameter(
                        "Probability",
                        value=[test_init.floatingValue],
                        resultRequest=[ResultType.MIN],
                    )
                ),
            )
        )
        self.assertTrue(
            ControlParameters.isApplicable(
                test_init.boundaryEventElement,
                (Parameter("Condition", value=[test_init.booleanValue])),
            )
        )

    def test_initTypeCheck(self):
    #     self.assertRaises(
    #         TypeError, ControlParameters, "hjhjsj", [interTriggerTimer]
    #     )
    #     self.assertRaises(
    #         TypeError, ControlParameters, test_init.taskElement, "ssdadsa"
    #     )
    #     self.assertRaises(
    #         ValueError, ControlParameters, test_init.taskElement, []
    #     )
        self.assertRaises(
            ParamNonValido,
            ControlParameters,
            test_init.resourceElement,
            [interTriggerTimer, triggerCount],
        )
        self.assertRaises(
            ParamNonValido,
            ControlParameters,
            test_init.startEventElement,
            [condition],
        )
        self.assertRaises(
            ParamNonValido,
            ControlParameters,
            test_init.boundaryEventElement,
            [condition, probability],
        )

    def test_toXML(self):
        self.assertEqual(
            controlXMLstr,
            etree.tostring(
                controlParams.toXML(), encoding="unicode", pretty_print=True
            ),
        )

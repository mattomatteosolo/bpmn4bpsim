from datetime import timedelta
from lxml import etree
import unittest
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    DurationParameter,
)

scenarioParameters = ScenarioParameters(
    **{
        "baseTimeUnit": TimeUnit.MS,
        "replication": 1,
        "traceOutput": True,
        "duration": Parameter(
            "Duration", value=[DurationParameter(value=timedelta(hours=8))]
        ),
    }
)
# print(etree.tostring(scenarioParameters.toXML(),encoding='unicode',pretty_print=True))

scenarioParametersXML = """<bpsim:ScenarioParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0" replication="1" baseTimeUnit="ms" traceOutput="true">
  <bpsim:Duration>
    <bpsim:DurationParameter value="PT8H"/>
  </bpsim:Duration>
</bpsim:ScenarioParameters>
"""


class test_ScenarioParameters(unittest.TestCase):
    def test_intin_checks(self):
        # TypeErrors
        # self.assertRaises(TypeError,,)
        # self.assertRaises(TypeError, ScenarioParameters, start=1)
        # self.assertRaises(TypeError, ScenarioParameters, duration="sda")
        # eccetera eccetera
        # ValueErrors
        # self.assertRaises(ValueError,,)
        self.assertRaises(ValueError, ScenarioParameters, seed=-1)
        self.assertRaises(ValueError, ScenarioParameters, replication=0)

    def test_toXML(self):
        self.assertEqual(
            scenarioParametersXML,
            etree.tostring(
                scenarioParameters.toXML(),
                encoding="unicode",
                pretty_print=True,
            ),
        )

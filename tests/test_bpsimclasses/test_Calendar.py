import unittest
import uuid
from lxml import etree
from datetime import datetime
from bpmn4bpsim.bpsimclasses.Calendar import Calendar


class test_Calendar(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls.dt1 = datetime.isoformat(
            datetime(year=2012, month=5, day=25, hour=14, minute=27, second=4)
        )
        cls.dt2 = datetime.isoformat(
            datetime(year=2010, month=5, day=25, hour=14, minute=27, second=4)
        )
        cls.dt3 = datetime.isoformat(
            datetime(year=2010, month=5, day=25, hour=18, minute=27, second=4)
        )
        cls.eventUUID = uuid.uuid1()
        cls.event_dict = {
            "dtstamp": cls.dt1,
            "dtstart": cls.dt2,
            "dtend": cls.dt3,
            "uid": cls.eventUUID,
            "rrule": {
                "freq": "weekly",
                "byday": ["mo", "tu", "we", "th", "fr"],
            },
        }
        cls.calendar_dict = {
            "version": 2.0,
            "prodid": "BPSimaaS",
            "event": [cls.event_dict],
        }
        cls.calendarXML = (
            '<bpsim:Calendar xmlns:bpsim="http://www.bpsim.org/'
            'schemas/2.0" id="id1" name="c1">'
            "BEGIN:VCALENDAR\n"
            "VERSION:2.0\n"
            "PRODID:BPSimaaS\n"
            "BEGIN:VEVENT\n"
            "DTSTART;VALUE=DATE-TIME:20100525T142704\n"
            "DTEND;VALUE=DATE-TIME:20100525T182704\n"
            "DTSTAMP;VALUE=DATE-TIME:20120525T142704Z\n"
            f"UID:{str(cls.eventUUID)}\n"
            "RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR\n"
            "END:VEVENT\n"
            "END:VCALENDAR\n"
            "</bpsim:Calendar>"
        )  # cls.calendarXML = '<bpsim:Calendar xmlns:bpsim="http://www.bpsim.org/' \
        #     'schemas/2.0" id="id1" name="c1">'\
        #     'BEGIN:VCALENDAR\r\n'\
        #     'VERSION:2.0\r\n'\
        #     'PRODID:BPSimaaS\r\n'\
        #     'BEGIN:VEVENT\r\n'\
        #     'DTSTART;VALUE=DATE-TIME:20100525T142704\r\n'\
        #     'DTEND;VALUE=DATE-TIME:20100525T182704\r\n'\
        #     'DTSTAMP;VALUE=DATE-TIME:20120525T142704Z\r\n'\
        #     f'UID:{str(cls.eventUUID)}\r\n'\
        #     'RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR\r\n'\
        #     'END:VEVENT\r\n'\
        #     'END:VCALENDAR\r\n'\
        #     '</bpsim:Calendar>'\

    # def test_FromDictError(self):
    #     self.assertRaises(TypeError, Calendar.calendarFromDict, 2)
    #     self.assertRaises(TypeError, Calendar.calendarFromDict, [])
    #     self.assertRaises(TypeError, Calendar.calendarFromDict, tuple())
    #     self.assertRaises(TypeError, Calendar.calendarFromDict, set())
    #     self.assertRaises(TypeError, Calendar.calendarFromDict, 2.3)
    #     self.assertRaises(TypeError, Calendar.calendarFromDict, True)

    @unittest.expectedFailure
    def test_FromDictNoError(self):
        self.assertRaises(TypeError, Calendar.calendarFromDict, dict())
        self.assertRaises(TypeError, Calendar.calendarFromDict, str())

    def test_toXML(self):
        self.maxDiff = None
        ical = Calendar("c1", self.calendar_dict, "id1")
        self.assertEqual(
            etree.tostring(
                ical.toXML(), encoding="unicode"
            ),  # .replace('&#13;\n', '\r\n'),
            self.calendarXML,
        )

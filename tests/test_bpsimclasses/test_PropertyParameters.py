import unittest
from lxml import etree
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.ParameterGroups.PropertyParameters import (
    PropertyParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter, Property, PropertyType
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.DistributionParameter import (
    TruncatedNormalDistribution,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)


property1 = Property(
    propertyName="nOfIssues",
    value=[
        TruncatedNormalDistribution(
            mean=2.0, minV=1.0, maxV=1000.0, standardDeviation=1.0
        )
    ],
    propertyType=PropertyType.LONG,
)
property2 = Property(
    "nOfIssues",
    value=[ExpressionParameter(value="bpsim:getProperty('nOfIssues') -1")],
)
# Può passare qualche controllo, mica tuttiti
propertyParam = Parameter("Property", resultRequest=[ResultType.MIN])
# Può passare qualche controllo, mica tuttiti
propertyParamValue = Parameter("Property", value=[NumericParameter(2)])
queueLength = Parameter("QueueLength", resultRequest=[ResultType.MIN])
queueLengthInvalid = Parameter("QueueLength", resultRequest=[ResultType.SUM])
queueLengthInvalidValue = Parameter(
    "QueueLength", value=[NumericParameter(value=2)]
)

propertyParamsXML = """<bpsim:PropertyParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0">
  <bpsim:Property name="nOfIssues">
    <bpsim:ExpressionParameter value="bpsim:getProperty('nOfIssues') -1"/>
  </bpsim:Property>
  <bpsim:Property name="nOfIssues" type="long">
    <bpsim:TruncatedNormalDistribution mean="2.0" standardDeviation="1.0" min="1.0" max="1000.0"/>
  </bpsim:Property>
  <bpsim:QueueLength>
    <bpsim:ResultRequest>min</bpsim:ResultRequest>
  </bpsim:QueueLength>
</bpsim:PropertyParameters>
"""
propertyParameters = PropertyParameters(
    test_init.taskElement, parameters=[queueLength, property2, property1]
)

property_parameters_scenario = PropertyParameters(
    None,
    parameters=[queueLength, property2, property1],
    scenario_parameters=True,
)


class test_PropertyParameters(unittest.TestCase):
    def test_chekParameter(self):
        self.assertTrue(PropertyParameters.checkParameter(property1))
        self.assertTrue(PropertyParameters.checkParameter(property2))
        self.assertTrue(PropertyParameters.checkParameter(queueLength))
        self.assertTrue(PropertyParameters.checkParameter(propertyParam))
        self.assertTrue(PropertyParameters.checkParameter(propertyParamValue))
        self.assertFalse(
            PropertyParameters.checkParameter(test_init.unknownParamName)
        )

    def test_isValidResult(self):
        self.assertTrue(PropertyParameters.isValidResult(queueLength))
        self.assertTrue(PropertyParameters.isValidResult(property1))
        self.assertTrue(PropertyParameters.isValidResult(property2))
        self.assertFalse(PropertyParameters.isValidResult(queueLengthInvalid))
        self.assertTrue(PropertyParameters.isValidResult(propertyParamValue))
        self.assertFalse(PropertyParameters.isValidResult(propertyParam))

    def test_isValidEnum(self):
        pass

    def test_isValidDistribution(self):
        pass

    def test_parameterHasValidValues(self):
        self.assertTrue(PropertyParameters.parameterHasValidValues(property1))
        self.assertTrue(PropertyParameters.parameterHasValidValues(property2))
        self.assertTrue(PropertyParameters.parameterHasValidValues(queueLength))
        self.assertFalse(
            PropertyParameters.parameterHasValidValues(queueLengthInvalidValue)
        )

    def test_isApplicable(self):
        self.assertTrue(
            PropertyParameters.isApplicable(test_init.taskElement, property1)
        )
        self.assertTrue(
            PropertyParameters.isApplicable(test_init.taskElement, queueLength)
        )
        self.assertTrue(
            PropertyParameters.isApplicable(
                test_init.taskElement, propertyParamValue
            )
        )
        self.assertTrue(
            PropertyParameters.isApplicable(
                test_init.subprocessWithDecompositionElement, property1
            )
        )
        self.assertFalse(
            PropertyParameters.isApplicable(
                test_init.subprocessWithDecompositionElement, queueLength
            )
        )
        self.assertFalse(
            PropertyParameters.isApplicable(
                test_init.startEventElement, queueLength
            )
        )

    def test_initTypeCheck(self):
        # lancia eccezione perchè è di tipo Parameter e non Property, ma ha nome Property, eh no
        self.assertRaises(
            ParamNonValido,
            PropertyParameters,
            test_init.taskElement,
            [propertyParamValue],
        )
        self.assertRaises(
            ParamNonValido,
            PropertyParameters,
            test_init.subprocessWithDecompositionElement,
            [propertyParamValue, queueLength],
        )
        # self.assertRaises(TypeError, PropertyParameters, "dadaas", [property1])
        # self.assertRaises(
        #     TypeError, PropertyParameters, test_init.taskElement, dict()
        # )
        # self.assertRaises(
        #     TypeError, PropertyParameters, test_init.taskElement, 1
        # )
        self.assertRaises(
            ValueError, PropertyParameters, test_init.taskElement, []
        )

    def test_toXML(self):
        self.assertEqual(
            etree.tostring(
                propertyParameters.toXML(),
                encoding="unicode",
                pretty_print=True,
            ),
            propertyParamsXML,
        )

    def test_scenario_properties(self):
        self.assertTrue(
            PropertyParameters.isApplicable(
                None, property2, scenario_parameters=True
            )
        )

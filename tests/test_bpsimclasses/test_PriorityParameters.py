"""
Modulo di test per PriorityParameters
"""
import unittest
from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from tests.test_bpsimclasses import test_init
from bpmn4bpsim.bpsimclasses.ParameterGroups.PriorityParameters import (
    PriorityParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)

interruptible = Parameter("Interruptible", value=[test_init.booleanValue])
priority = Parameter("Priority", value=[NumericParameter(value=2)])
interruptibleExpression = Parameter(
    "Interruptible",
    value=[ExpressionParameter(value="bpsim:getProperty('mela')")],
)
priorityInvalidResult = Parameter("Priority", resultRequest=[ResultType.MIN])
interruptibleInvalidResult = Parameter(
    "Interruptible",
    value=[test_init.booleanValue],
    resultRequest=[ResultType.MIN],
)
priorityInvalidValue = Parameter("Priority", value=[test_init.booleanValue])
interruptibleInvalidValue = Parameter(
    "Interruptible", value=[test_init.numericValue]
)

priorityXML = """<bpsim:PriorityParameters xmlns:bpsim="http://www.bpsim.org/schemas/2.0">
  <bpsim:Interruptible>
    <bpsim:BooleanParameter value="true"/>
  </bpsim:Interruptible>
  <bpsim:Priority>
    <bpsim:NumericParameter value="2"/>
  </bpsim:Priority>
</bpsim:PriorityParameters>
"""

priorityParam = PriorityParameters(
    test_init.taskElement, parameters=[priority, interruptible]
)


class test_PriorityParameters(unittest.TestCase):
    def test_chekParameter(self):
        self.assertFalse(
            PriorityParameters.checkParameter(test_init.unknownParamName)
        )
        self.assertTrue(PriorityParameters.checkParameter(priority))
        self.assertTrue(PriorityParameters.checkParameter(interruptible))

    def test_isValidResult(self):
        self.assertFalse(
            PriorityParameters.isValidResult(priorityInvalidResult)
        )
        self.assertFalse(
            PriorityParameters.isValidResult(interruptibleInvalidResult)
        )
        self.assertTrue(PriorityParameters.isValidResult(interruptible))
        self.assertTrue(PriorityParameters.isValidResult(priority))

    def test_isValidEnum(self):
        self.assertTrue(
            PriorityParameters.isValidEnum(
                "Interruptible", test_init.booleanEnum
            )
        )
        self.assertTrue(
            PriorityParameters.isValidEnum("Priority", test_init.numericEnum)
        )
        self.assertTrue(
            PriorityParameters.isValidEnum("Priority", test_init.floatingEnum)
        )
        self.assertFalse(
            PriorityParameters.isValidEnum("Priority", test_init.booleanEnum)
        )
        self.assertFalse(
            PriorityParameters.isValidEnum("Priority", test_init.stringEnum)
        )
        self.assertFalse(
            PriorityParameters.isValidEnum(
                "Interruptible", test_init.numericStringEnum
            )
        )

    def test_isValidDistribution(self):
        pass

    def test_parameterHasValidValues(self):
        self.assertTrue(
            PriorityParameters.parameterHasValidValues(interruptible)
        )
        self.assertTrue(PriorityParameters.parameterHasValidValues(priority))
        self.assertTrue(
            PriorityParameters.parameterHasValidValues(interruptibleExpression)
        )
        self.assertFalse(
            PriorityParameters.parameterHasValidValues(priorityInvalidValue)
        )
        self.assertFalse(
            PriorityParameters.parameterHasValidValues(
                interruptibleInvalidValue
            )
        )

    def test_isApplicable(self):
        self.assertTrue(
            PriorityParameters.isApplicable(test_init.taskElement, priority)
        )
        self.assertTrue(
            PriorityParameters.isApplicable(
                test_init.taskElement, interruptible
            )
        )
        self.assertFalse(
            PriorityParameters.isApplicable(
                test_init.subprocessWithDecompositionElement, interruptible
            )
        )
        self.assertFalse(
            PriorityParameters.isApplicable(
                test_init.startEventElement, interruptible
            )
        )

    def test_initTypeCheck(self):
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement="sdasd",
    #         parameters=[priority],
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters="sdasd",
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=True,
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=1,
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=2.5,
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=dict(),
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=tuple(),
    #     )
    #     self.assertRaises(
    #         TypeError,
    #         PriorityParameters,
    #         bpmElement=test_init.taskElement,
    #         parameters=set(),
    #     )
        self.assertRaises(
            ValueError,
            PriorityParameters,
            bpmElement=test_init.taskElement,
            parameters=list(),
        )
        self.assertRaises(
            ParamNonValido,
            PriorityParameters,
            bpmElement=test_init.taskElement,
            parameters=[test_init.unknownParamName],
        )

    def test_toXML(self):
        self.assertEqual(
            etree.tostring(
                priorityParam.toXML(), encoding="unicode", pretty_print=True
            ),
            priorityXML,
        )

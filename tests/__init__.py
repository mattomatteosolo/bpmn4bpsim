from os import path
import unittest
from lxml import etree


class LoadingModelsBaseTestCase(unittest.TestCase):
    def setUp(self):
        base_data_models = path.join("tests", "data", "models")
        with open(path.abspath(path.join(base_data_models,"everythingRes.bpmn"))) as f:
            self.everythingRes = etree.parse(f)
        with open(path.abspath(path.join(base_data_models, "everything.bpmn"))) as f:
            self.everything = etree.parse(f)
        with open(path.abspath(path.join(base_data_models, "car_repair.bpmn"))) as f:
            self.carRepair = etree.parse(f).getroot()
        with open(path.abspath(path.join(base_data_models, "inclusivegateway.bpmn"))) as f:
            self.inclusivegateway = etree.parse(f)
        with open(path.abspath(path.join(base_data_models, "simple.bpmn"))) as f:
            self.simple = etree.parse(f).getroot()
        with open(path.abspath(path.join(base_data_models, "event_subprocess.bpmn"))) as f:
            self.eventSubprocess = etree.parse(f)

    def tearDown(self):
        pass

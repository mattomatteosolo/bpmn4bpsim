from lxml import etree

from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)
from bpmn4bpsim.bpmnfacilities import BPMN_NAMESPACES
from bpmn4bpsim.bpmnfacilities.bpmn_enums import BPMNEvents
from bpmn4bpsim.parser.parseroutputs import (
    InputOnCondition,
    PossibleInputs,
    Possible,
    PossibleOutputs,
    OutputParameters,
)

from tests import LoadingModelsBaseTestCase


class ParserBaseTestCase(LoadingModelsBaseTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.starteventio = InputOnCondition(
            BPMNEvents.startevent,
            [
                PossibleInputs(
                    ParameterGroupsName.ControlParameters,
                    ["InterTriggerTimer", "TriggerCount"],
                ),
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters, ["Property"]
                ),
            ],
            nestedConditions=[
                InputOnCondition(
                    BPMNEvents.ineventsubprocess,
                    [
                        PossibleInputs(
                            ParameterGroupsName.ControlParameters,
                            ["Probability", "Condition"],
                        )
                    ],
                ),
            ],
        )

        self.starteventPI = [
            PossibleInputs(
                ParameterGroupsName.ControlParameters,
                ["InterTriggerTimer", "TriggerCount"],
            ),
            PossibleInputs(
                ParameterGroupsName.PropertyParameters, ["Property"]
            ),
        ]

        element = self.simple.find(
            ".//startEvent[@id='StartEvent_0fj6ok1']",
            namespaces=BPMN_NAMESPACES,
        )
        self.simpleStartevent = (
            element if element != None else etree.Element("x")
        )

        element = self.simple.find(
            ".//subProcess[@id='Task_0au6mvd']",
            namespaces=BPMN_NAMESPACES,
        )
        self.simpleEventSubProcess = (
            element if element != None else etree.Element("x")
        )

        element = self.simple.find(
            ".//task[@id='Task_0yez3qj']", namespaces=BPMN_NAMESPACES
        )
        self.simpleTask = element if element != None else etree.Element("x")

        element = self.simple.find(
            ".//endEvent[@id='EndEvent_1isyp22']", namespaces=BPMN_NAMESPACES
        )
        self.simpleEndEvent = element if element != None else etree.Element("x")

        element = self.simple.find(
            ".//process[@id='Process_10fvunj']", namespaces=BPMN_NAMESPACES
        )
        self.simpleProcess = element if element != None else etree.Element("x")

        self.simpleSePossible = Possible(
            "//*[@id='StartEvent_0fj6ok1']",
            [
                PossibleInputs(
                    ParameterGroupsName.ControlParameters,
                    [
                        "InterTriggerTimer",
                        "TriggerCount",
                        "Probability",
                        "Condition",
                    ],
                ),
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters, ["Property"]
                ),
            ],
            [
                PossibleOutputs(
                    ParameterGroupsName.ControlParameters,
                    [
                        OutputParameters(
                            "InterTriggerTimer",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters("TriggerCount", [ResultType.COUNT]),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.PropertyParameters,
                    [
                        OutputParameters(
                            "QueueLength",
                            [ResultType.MIN, ResultType.MAX, ResultType.MEAN],
                        )
                    ],
                ),
            ],
        )

        self.simpleSpPossible = Possible(
            "//*[@id='Task_0au6mvd']",
            [
                PossibleInputs(
                    ParameterGroupsName.CostParameters,
                    ["FixedCost", "UnitCost"],
                ),
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters, ["Property"]
                ),
            ],
            [
                PossibleOutputs(
                    ParameterGroupsName.CostParameters,
                    [
                        OutputParameters("FixedCost", [ResultType.SUM]),
                        OutputParameters("UnitCost", [ResultType.SUM]),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.PropertyParameters,
                    [
                        OutputParameters(
                            "QueueLength",
                            [ResultType.MIN, ResultType.MAX, ResultType.MEAN],
                        )
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.TimeParameters,
                    [
                        OutputParameters(
                            "TransferTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "QueueTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "WaitTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "SetupTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ProcessingTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ValidationTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ReworkTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "LagTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "Duration",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ElapsedTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.ControlParameters,
                    [OutputParameters("TriggerCount", [ResultType.COUNT])],
                ),
            ],
        )

        self.simpleTaskPossible = Possible(
            "//*[@id='Task_0yez3qj']",
            [
                PossibleInputs(
                    ParameterGroupsName.TimeParameters,
                    [
                        "TransferTime",
                        "QueueTime",
                        "WaitTime",
                        "SetupTime",
                        "ProcessingTime",
                        "ValidationTime",
                        "ReworkTime",
                    ],
                ),
                PossibleInputs(
                    ParameterGroupsName.CostParameters,
                    ["FixedCost", "UnitCost"],
                ),
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters,
                    ["Property", "QueueLength"],
                ),
                PossibleInputs(
                    ParameterGroupsName.PriorityParameters,
                    ["Interruptible", "Priority"],
                ),
            ],
            [
                PossibleOutputs(
                    ParameterGroupsName.TimeParameters,
                    [
                        OutputParameters(
                            "TransferTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "QueueTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "WaitTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "SetupTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ProcessingTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ValidationTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ReworkTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "LagTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "Duration",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ElapsedTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.CostParameters,
                    [
                        OutputParameters("FixedCost", [ResultType.SUM]),
                        OutputParameters("UnitCost", [ResultType.SUM]),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.PropertyParameters,
                    [
                        OutputParameters(
                            "QueueLength",
                            [ResultType.MIN, ResultType.MAX, ResultType.MEAN],
                        )
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.ControlParameters,
                    [OutputParameters("TriggerCount", [ResultType.COUNT])],
                ),
            ],
        )

        self.simpleEEPossible = Possible(
            "//*[@id='EndEvent_1isyp22']",
            [
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters, ["Property"]
                ),
            ],
            [
                PossibleOutputs(
                    ParameterGroupsName.PropertyParameters,
                    [
                        OutputParameters(
                            "QueueLength",
                            [ResultType.MIN, ResultType.MAX, ResultType.MEAN],
                        )
                    ],
                ),
            ],
        )

        self.simpleProcessPossible = Possible(
            "//*[@id='Process_10fvunj']",
            [
                PossibleInputs(
                    ParameterGroupsName.CostParameters,
                    ["FixedCost", "UnitCost"],
                )
            ],
            [
                PossibleOutputs(
                    ParameterGroupsName.CostParameters,
                    [
                        OutputParameters("FixedCost", [ResultType.SUM]),
                        OutputParameters("UnitCost", [ResultType.SUM]),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.TimeParameters,
                    [
                        OutputParameters(
                            "TransferTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "QueueTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "WaitTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "SetupTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ProcessingTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ValidationTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ReworkTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "LagTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "Duration",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters(
                            "ElapsedTime",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.COUNT,
                                ResultType.SUM,
                            ],
                        ),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.ControlParameters,
                    [OutputParameters("TriggerCount", [ResultType.COUNT])],
                ),
            ],
        )

        self.simpleParserOutput = [
            self.simpleSePossible,
            self.simpleTaskPossible,
            self.simpleEEPossible,
            self.simpleSpPossible,
            self.simpleProcessPossible,
        ]

from typing import List
from tests.test_parser import ParserBaseTestCase

from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpmnfacilities.bpmn_enums import (
    BPMNEvents,
    BPMNAllParameterizable,
)
from bpmn4bpsim.parser.parseroutputs import (
    PossibleOutputs,
    OutputParameters,
)
from bpmn4bpsim.parser import parser


class ParserFunctionsTestCase(ParserBaseTestCase):
    def test_possibleInputFromConditions(self):
        starteventCondition: List[BPMNAllParameterizable] = [
            BPMNEvents.startevent
        ]
        self.assertEqual(
            parser.possibleInputFromConditions(starteventCondition),
            self.starteventPI,
        )
        starteventCondition: List[BPMNAllParameterizable] = [
            BPMNEvents.startevent,
            BPMNEvents.ineventsubprocess,
        ]
        self.assertNotEqual(
            parser.possibleInputFromConditions(starteventCondition),
            self.starteventPI,
        )

    def test_possibleOutputFromConditions(self):
        starteventCondition: List[BPMNAllParameterizable] = [
            BPMNEvents.startevent
        ]
        self.assertEqual(
            parser.possibleOutputFromConditions(
                self.starteventPI, starteventCondition
            ),
            [
                PossibleOutputs(
                    ParameterGroupsName.ControlParameters,
                    [
                        OutputParameters(
                            "InterTriggerTimer",
                            [
                                ResultType.MIN,
                                ResultType.MAX,
                                ResultType.MEAN,
                                ResultType.SUM,
                            ],
                        ),
                        OutputParameters("TriggerCount", [ResultType.COUNT]),
                    ],
                ),
                PossibleOutputs(
                    ParameterGroupsName.PropertyParameters,
                    [
                        OutputParameters(
                            "QueueLength",
                            [ResultType.MIN, ResultType.MAX, ResultType.MEAN],
                        )
                    ],
                ),
            ],
        )


class FromElementTestCase(ParserBaseTestCase):
    def test_getPossibleFromElement(self):
        self.assertEqual(
            parser.getPossibleFromElement(self.simpleStartevent),
            self.simpleSePossible,
        )

        self.assertEqual(
            parser.getPossibleFromElement(self.simpleEventSubProcess),
            self.simpleSpPossible,
        )

        self.assertEqual(
            parser.getPossibleFromElement(self.simpleTask),
            self.simpleTaskPossible,
        )

        self.assertEqual(
            parser.getPossibleFromElement(self.simpleEndEvent),
            self.simpleEEPossible
        )

        self.assertEqual(
            parser.getPossibleFromElement(self.simpleProcess),
            self.simpleProcessPossible
        )


class ParserTestCase(ParserBaseTestCase):
    def test_parseBPMN(self):
        self.maxDiff = None
        fromParser = parser.parseBPMN(self.simple)

        self.assertEqual(self.simpleParserOutput, fromParser)

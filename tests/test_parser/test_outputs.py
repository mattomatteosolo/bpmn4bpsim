from typing import List
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)
from bpmn4bpsim.bpmnfacilities.bpmn_enums import (
    BPMNEvents,
    BPMNAllParameterizable,
)
from bpmn4bpsim.parser.parseroutputs import PossibleInputs

from tests.test_parser import ParserBaseTestCase


class InputOnConditionTestCase(ParserBaseTestCase):
    def test_allPossibleInputsSE(self):
        condition: List[BPMNAllParameterizable] = [BPMNEvents.startevent]
        self.assertListEqual(
            self.starteventPI,
            self.starteventio.allPossibleInputs(condition),
        )

    def test_allPossibleInputsSEIESP(self):
        condition: List[BPMNAllParameterizable] = [
            BPMNEvents.startevent,
            BPMNEvents.ineventsubprocess,
        ]
        self.assertNotEqual(
            [
                PossibleInputs(
                    ParameterGroupsName.ControlParameters,
                    ["InterTriggerTimer", "TriggerCount"],
                ),
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters, ["Property"]
                ),
            ],
            self.starteventio.allPossibleInputs(condition),
        )
        self.assertEqual(
            [
                PossibleInputs(
                    ParameterGroupsName.ControlParameters,
                    ["InterTriggerTimer", "TriggerCount", "Probability", "Condition"],
                ),
                PossibleInputs(
                    ParameterGroupsName.PropertyParameters, ["Property"]
                ),
            ],
            self.starteventio.allPossibleInputs(condition),
        )

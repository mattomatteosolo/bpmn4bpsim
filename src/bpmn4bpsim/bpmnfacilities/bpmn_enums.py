import enum


class BPMNEvents(enum.Enum):
    startevent = "startevent"
    intermediate = "intermediate"
    endevent = "endevent"
    ineventsubprocess = "ineventsubprocess"
    catch = "catch"
    boundary = "boundary"


class BPMNActivities(enum.Enum):
    task = "task"
    subprocess = "subprocess"
    transaction = "transaction"
    callactivity = "callactivity"
    eventsubprocess = "eventsubprocess"
    process = "process"


class BPMNConnObjects(enum.Enum):
    sequenceflow = "sequenceflow"
    messageflow = "messageflow"


class BPMNAttributes(enum.Enum):
    resourcerole = "resourcerole"
    resource = "resource"


class BPMNGateways(enum.Enum):
    eventbasedgateway = "eventbasedgateway"
    instantiatediverging = "instantiatediverging"


class BPMNActivityConditions(enum.Enum):
    nodecomposition = "nodecomposition"
    noincoming = "noincoming"


BPMNAllParameterizable = (
    BPMNEvents
    | BPMNActivities
    | BPMNConnObjects
    | BPMNActivityConditions
    | BPMNAttributes
    | BPMNGateways
)

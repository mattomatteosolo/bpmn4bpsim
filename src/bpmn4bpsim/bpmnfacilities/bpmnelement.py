from lxml import etree
from typing import List
from bpmn4bpsim.bpmnfacilities import bpmn_types, bpmn_conditional_types
import bpmn4bpsim.bpmnfacilities.bpmn_enums as enums


def getConditions(
    xmlElement: etree._Element,
) -> List[enums.BPMNAllParameterizable]:
    """
    Given an element, this function will return a list of
    `BPMNAllParameterizable` according to the element type

    Parameters
    ----------
    `xmlElement`: `etree._Element`
        bpmn element to check

    Outputs
    -------
    `List[bpmn_enums.BPMNAllParameterizable]`
        list of conditions that `xmlElement` satisfies.
    """
    l: List[enums.BPMNAllParameterizable] = list()

    if bpmn_types.isStartEvent(xmlElement):
        l.append(enums.BPMNEvents.startevent)
        if bpmn_conditional_types.startInEventSubProcess(xmlElement):
            l.append(enums.BPMNEvents.ineventsubprocess)

    elif bpmn_types.isIntermediateEvent(xmlElement):
        l.append(enums.BPMNEvents.intermediate)
        if bpmn_conditional_types.isCatchEvent(xmlElement):
            l.append(enums.BPMNEvents.catch)
        if bpmn_conditional_types.isBoundaryEvent(xmlElement):
            l.append(enums.BPMNEvents.boundary)

    elif bpmn_types.isEndEvent(xmlElement):
        l.append(enums.BPMNEvents.endevent)

    elif bpmn_types.isTask(xmlElement):
        l.append(enums.BPMNActivities.task)
        if not bpmn_conditional_types.isDecomposable(xmlElement):
            l.append(enums.BPMNActivityConditions.nodecomposition)
        if not bpmn_conditional_types.hasIncoming(xmlElement):
            l.append(enums.BPMNActivityConditions.noincoming)

    elif bpmn_types.isEventSubProcess(xmlElement):
        l.append(enums.BPMNActivities.eventsubprocess)
        if not bpmn_conditional_types.isDecomposable(xmlElement):
            l.append(enums.BPMNActivityConditions.nodecomposition)

    elif bpmn_types.isSubProcess(xmlElement):
        l.append(enums.BPMNActivities.subprocess)
        if not bpmn_conditional_types.isDecomposable(xmlElement):
            l.append(enums.BPMNActivityConditions.nodecomposition)
        if not bpmn_conditional_types.hasIncoming(xmlElement):
            l.append(enums.BPMNActivityConditions.noincoming)

    elif bpmn_types.isTransaction(xmlElement):
        l.append(enums.BPMNActivities.transaction)
        if not bpmn_conditional_types.isDecomposable(xmlElement):
            l.append(enums.BPMNActivityConditions.nodecomposition)
            if not bpmn_conditional_types.hasIncoming(xmlElement):
                l.append(enums.BPMNActivityConditions.noincoming)

    elif bpmn_types.isCallActivity(xmlElement):
        l.append(enums.BPMNActivities.callactivity)
        if not bpmn_conditional_types.isDecomposable(xmlElement):
            l.append(enums.BPMNActivityConditions.nodecomposition)
            if not bpmn_conditional_types.hasIncoming(xmlElement):
                l.append(enums.BPMNActivityConditions.noincoming)

    elif bpmn_conditional_types.isEventBaseGatewayStarting(xmlElement):
        l.append(enums.BPMNGateways.eventbasedgateway)

    elif bpmn_types.isSequenceFlow(xmlElement):
        l.append(enums.BPMNConnObjects.sequenceflow)

    elif bpmn_types.isMessageFlow(xmlElement):
        l.append(enums.BPMNConnObjects.messageflow)

    elif bpmn_types.isResource(xmlElement):
        l.append(enums.BPMNAttributes.resource)

    elif bpmn_types.isResourceRole(xmlElement):
        l.append(enums.BPMNAttributes.resourcerole)

    elif bpmn_types.isProcess(xmlElement):
        l.append(enums.BPMNActivities.process)

    return l

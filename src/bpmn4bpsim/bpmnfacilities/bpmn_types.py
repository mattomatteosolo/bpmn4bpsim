"""Function used to detect bpmn element type
"""
from lxml.etree import _Element, QName


def isEventSubProcess(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is of type supbrocess and it's triggered by event

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is subProcess and has an attribute
        triggeredByEvent set to `True`, false otherwise
    """
    if QName(bpmElement).localname.lower() == "subprocess":
        for attr in bpmElement.attrib.items():
            if (
                attr[0].lower() == "triggeredbyevent"
                and attr[1].lower() == "true"
            ):
                return True
        return False

    else:
        return False


def isSequenceFlow(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is of type sequenceFlow and its attribute
    sourceRef refers to a gateway of type exclusive, inclusive or
    complex

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is sequenceFlow and it's coming from a
        gateway of type exclusive, inclusive or complex, `false` otherwise
    """

    if QName(bpmElement).localname.lower() == "sequenceflow":
        parent = bpmElement.getparent()
        if parent is None:
            raise ValueError("Couldn't find sourceRef for given element")

        sourceRef = parent.find(
            ".//*[@id='{}']".format(bpmElement.get("sourceRef"))
        )
        if sourceRef is None:
            raise ValueError("Couldn't find sourceRef for given sequenceFlow")

        prevoiusElementTag = QName(sourceRef).localname.lower()
        if (
            prevoiusElementTag == "inclusivegateway"
            or prevoiusElementTag == "exclusivegateway"
            or prevoiusElementTag == "complexgateway"
        ):
            return True
        return False

    return False


def isResource(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a resource

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a resource, `false` otherwise
    """
    if QName(bpmElement).localname.lower() == "resource":
        return True
    return False


def isResourceRole(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a resourceRole

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a resourceRole, `false` otherwise
    """
    if QName(bpmElement).localname.lower() == "resourcerole":
        return True

    return False


def isMessageFlow(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a messageflow

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a messageflow, `false` otherwise
    """
    if QName(bpmElement).localname.lower() == "messageflow":
        return True

    return False


def isStartEvent(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a startEvent

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a startevent, `false` otherwise
    """
    if QName(bpmElement).localname.lower() == "startevent":
        return True

    return False


def isIntermediateEvent(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is an intermediateEvent

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is an intermediateEvent, `false` otherwise
    """
    if (
        "intermediate" in QName(bpmElement).localname.lower()
        and "event" in QName(bpmElement).localname.lower()
    ):
        return True

    return False


def isEndEvent(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is an endEvent

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is an endEvent, `false` otherwise
    """
    if QName(bpmElement).localname.lower() == "endevent":
        return True

    return False


def isTask(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a task

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a task, `false` otherwise
    """
    if "task" in QName(bpmElement).localname.lower():
        return True
    return False


def isSubProcess(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a subProcess

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a subProcess, `false` otherwise
    """
    if "subprocess" == QName(bpmElement).localname.lower():
        return True
    return False


def isTransaction(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a transaction

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a transaction, `false` otherwise
    """
    if "transaction" == QName(bpmElement).localname.lower():
        return True
    return False


def isCallActivity(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is a callactivity

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is a callactivity, `false` otherwise
    """
    if "callactivity" == QName(bpmElement).localname.lower():
        return True
    return False


def isProcess(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is of type process

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is of type process, `false` otherwise
    """
    if "process" == QName(bpmElement).localname.lower():
        return True
    return False


def isActivity(bpmElement: _Element) -> bool:
    """
    Check if bpmElement is any kind of activity

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        Element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is any kind of activity, `False` otherwise
    """
    if (
        isTask(bpmElement)
        or isSubProcess(bpmElement)
        or isTransaction(bpmElement)
        or isCallActivity(bpmElement)
        or isEventSubProcess(bpmElement)
    ):
        return True

    return False

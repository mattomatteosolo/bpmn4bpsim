__bpmn_dc = "http://www.omg.org/spec/DD/20100524/DC"
__bpmn_di = "http://www.omg.org/spec/DD/20100524/DI"
__bpmn_semantic = "http://www.omg.org/spec/BPMN/20100524/MODEL"
__bpmn_bpmndi = "http://www.omg.org/spec/BPMN/20100524/DI"

SEMANTIC = "{%s}" % __bpmn_semantic

BPMN_NAMESPACES = {
    None: __bpmn_semantic,
    "bpmn2": __bpmn_semantic,
    "semantic": __bpmn_semantic,
    "bpmndi": __bpmn_bpmndi,
    "di": __bpmn_di,
    "dc": __bpmn_dc,
}

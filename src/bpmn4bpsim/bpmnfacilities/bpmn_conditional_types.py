"""
Utility function to find out bpmn element nature
"""
from lxml import etree
from bpmn4bpsim.bpmnfacilities import BPMN_NAMESPACES
from bpmn4bpsim.bpmnfacilities.bpmn_types import isActivity, isEventSubProcess


def isDecomposable(bpmElement: etree._Element) -> bool:
    """
    Check wether on not bpmElement is an activity with decomposition

    Parameters
    ----------
    `bpmElement`: `etree._Element`
        bmpn element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is decomposable, `False` otherwise
    """
    # If has no children, then it's not decomposable
    if len(bpmElement) == 0:
        return False

    # is decomposable if it contains at least an "activity" or a kind of start
    # event
    for element in bpmElement:
        elementLocalName = etree.QName(element).localname.lower()
        if (
            "task" in elementLocalName
            or "activity" in elementLocalName
            or "sub" in elementLocalName
            or "callactivity" in elementLocalName
            or "subprocess" in elementLocalName
        ):
            return True
        if (
            "startevent" in elementLocalName
            or "eventbasedgateway" in elementLocalName
        ):
            return True

    return False


def isBoundaryEvent(bpmElement: etree._Element) -> bool:
    """
    Check wether on not bpmElement is a boundary event

    Prameters
    ---------
    `bpmElement`: `etree._Element`
        bpmn element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is boundary event, `False` otherwise
    """
    if etree.QName(bpmElement).localname.lower() == "boundaryevent":
        return True

    return False


def isCatchEvent(bpmElement: etree._Element) -> bool:
    """
    Check wether on not bpmElement is an intermediate catch event

    Prameters
    ---------
    `bpmElement`: `etree._Element`
        bpmn element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is intermediate catch event, `False` otherwise
    """
    if (
        "catch" in etree.QName(bpmElement).localname.lower()
        and "event" in etree.QName(bpmElement).localname.lower()
    ):
        return True
    return False


def startInEventSubProcess(bpmElement: etree._Element) -> bool:
    """
    Check wether on not bpmElement is of type start event and its immediate
    ancestor is of type eventsubprocess

    Prameters
    ---------
    `bpmElement`: `etree._Element`
        bpmn element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is start event and its parent node is of type
        eventsubprocess, `False` otherwise
    """
    parentNode = bpmElement.getparent()
    if parentNode is None:
        return False
    elementLocalName = etree.QName(bpmElement).localname.lower()
    if (
        "start" in elementLocalName
        and "event" in elementLocalName
        and isEventSubProcess(parentNode)
    ):
        return True

    return False


def hasIncoming(bpmElement: etree._Element) -> bool:
    """
    Check wether on not bpmElement is an activity and none of the sequenceFlow
    from inside its parent node has targetRef set to its id.

    Prameters
    ---------
    `bpmElement`: `etree._Element`
        bpmn element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` is an activity and has no incoming sequenceFlow,
        `False` otherwise
    """
    if not isActivity(bpmElement):
        return False

    bpmElementId = bpmElement.get("id")
    seqFlow = bpmElement.getroottree().getroot().find(
        f".//sequenceFlow[@targetRef='{bpmElementId}']",
        namespaces=BPMN_NAMESPACES,
    )
    if seqFlow is not None:
        return True

    return False


def isEventBaseGatewayStarting(bpmElement: etree._Element) -> bool:
    """
    Check if bmpElement is of type eventBasedGateway with no incoming flows,
    and the following attribute: instantiate='true',
    gatewayDirection='Diverging'

    Prameters
    ---------
    `bpmElement`: `etree._Element`
        bpmn element to analyse

    Returns
    -------
    `bool`
        `True` if `bpmElement` eventBasedGateway, `False` otherwise
    """
    if hasIncoming(bpmElement):
        return False

    if etree.QName(bpmElement).localname.lower() == "eventbasedgateway":
        instantiate = bpmElement.get("instantiate")
        gwDirection = bpmElement.get("gatewayDirection")
        if instantiate is not None and gwDirection is not None:
            if (
                instantiate.lower() == "true"
                and gwDirection.lower() == "diverging"
            ):
                return True
        return False

    return False

from copy import deepcopy
from typing import List


import bpmn4bpsim.bpmnfacilities.bpmn_enums as enums
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)


class PossibleInputs(object):
    groupParameter: ParameterGroupsName
    parameters: List[str]

    def __init__(
        self, groupParameter: ParameterGroupsName, parameters: List[str]
    ) -> None:
        self.groupParameter = groupParameter
        self.parameters = parameters

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, PossibleInputs):
            return False
        return (
            self.groupParameter == other.groupParameter
            and self.parameters == other.parameters
        )


class OutputParameters:
    """
    Class that represents output parameter for parser output
    """

    parameterName: str
    possibleOutputs: List[ResultType]

    def __init__(
        self, parameterName: str, possibleOutputs: List[ResultType]
    ) -> None:
        self.parameterName = parameterName
        self.possibleOutputs = possibleOutputs

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, OutputParameters):
            return False
        return (
            self.parameterName == other.parameterName
            and self.possibleOutputs == other.possibleOutputs
        )


class PossibleOutputs:
    """
    Class that represents output groupParameter for parser output
    """

    groupParameter: ParameterGroupsName
    parameters: List[OutputParameters]

    def __init__(
        self,
        groupParameter: ParameterGroupsName,
        parameters: List[OutputParameters],
    ) -> None:
        self.groupParameter = groupParameter
        self.parameters = parameters

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, PossibleOutputs):
            return False
        return (
            self.groupParameter == other.groupParameter
            and self.parameters == other.parameters
        )


class Possible:
    """
    Class that represents possible input and output for a given bpmn model
    """

    xpathID: str
    input: List[PossibleInputs]
    output: List[PossibleOutputs]

    def __init__(
        self,
        xpathID: str,
        input: List[PossibleInputs],
        output: List[PossibleOutputs],
    ):
        self.xpathID = xpathID
        self.input = input
        self.output = output

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Possible):
            return False
        return (
            self.xpathID == other.xpathID
            and self.input == other.input
            and self.output == other.output
        )


class InputOnCondition:
    condition: enums.BPMNAllParameterizable
    inputs: List[PossibleInputs]
    nestedConditions: List["InputOnCondition"]

    def __init__(
        self,
        condition: enums.BPMNAllParameterizable,
        inputs: List[PossibleInputs] = [],
        nestedConditions: List["InputOnCondition"] = [],
    ) -> None:
        self.condition = condition
        self.inputs = inputs
        self.nestedConditions = nestedConditions

    def allPossibleInputs(
        self, conditions: List[enums.BPMNAllParameterizable]
    ) -> List[PossibleInputs]:
        if self.condition not in conditions:
            return []

        possibleInputs = [deepcopy(pi) for pi in self.inputs]
        for nestedCondition in self.nestedConditions:
            for possibleInput in nestedCondition.allPossibleInputs(conditions):
                existing = [
                    pi
                    for pi in possibleInputs
                    if pi.groupParameter == possibleInput.groupParameter
                ]
                if len(existing) > 0:
                    existing[0].parameters.extend(
                        deepcopy(possibleInput.parameters)
                    )
                else:
                    possibleInputs.append(deepcopy(possibleInput))

        return possibleInputs

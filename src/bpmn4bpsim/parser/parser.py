from lxml import etree
from copy import deepcopy
from typing import List, Optional


from bpmn4bpsim.bpmnfacilities.bpmn_enums import (
    BPMNActivities,
    BPMNAllParameterizable,
    BPMNAttributes,
)
from bpmn4bpsim.bpmnfacilities.bpmnelement import getConditions
from bpmn4bpsim.parser.parseroutputs import Possible
from bpmn4bpsim.parser.applicability import (
    INPUT_APPLICABILITY,
    OUTPUT_APPLICABITY,
    OUTPUT_DECOMPOSABLE,
    OUTPUT_RESOURCE,
)
from bpmn4bpsim.parser.parseroutputs import PossibleInputs, PossibleOutputs

##########################################################################
# Algoritmo che restituisce la lista degli elementi parametrizzabili di un
# modello BPMN
##########################################################################

##############################
# Formalizzazione problema
##############################

# Input: Modello BPM serializzato secondo XML

# Soluzioni Ammissibili: Mappa < stringheXPATH, Mappa<'NomeGroupParams',
# Liszt<'NomiParametri'> > >

# Predicato di correttezza: mappa in Sol.Amm.[
#   per ogni Item in map.items()[
#       l'elemento identificato da x(0) ammette tutti e soli i parametri
#       contenuti in x(1)
#   ]
# ]

#################
# Spiegazione
#################

# Si tratta di realizzare una visita in profondità dell'albero, portandosi
# dietro ogni volta la stringa XPATH generata fino a quel punto dalla radice,
# un riferimento alla mappa completa finora generata, e, chiaramente, un
# riferimento all'elemento fin'ora raggiunto.
# L'algoritmo sarà diviso in due parti, una parte che prende in input soltanto
# il modello, genera gli elementi necessari all'esecuzione della seconda parte,
# che sarà l'effettiva visita in profondita nel DOM XML

##############################
# Pseudo-codice
##############################

##########################################################################
# Algo parseBPMForParameterizable(ModelloBPM model) -> Mappa < stringheXPATH, Mappa<'NomeGroupParams', Liszt<'NomiParametri'> > >
#   Mappa <stringheXPATH, Mappa<'NomeGroupParams', List<'NomiParametri'>>> input = new Mappa<>()
#   Mappa <stringheXPATH, Mappa<'NomeGroupParams', List<'NomiParametri'>>> output = new Mappa<>()
#   xpath = '/'
#   inputParseBPMRecursive(model, xpath, input)
#   outputParseBPMRecursive(model, xpath, output)
#   return (input,output)
##########################################################################

##########################################################################
# Algo parseBPMRecursive(ElementoBPM elem, string xpath, ...Mappa output) -> Mappa
#   if 'bpmndiagram' in elem.tag:
#       return {}
#
#   xpatElem = xpath + "/" + elem.tag
#   se elem ha figli:
#       per ogni figlio di elem:
#           xpathFiglio = xpathElem
#           mappa = parseBPMRecursive(figlio, xpath, ...)
#           se mappa non è vuota:
#               output.update( mappa )
#
#   se elem è parametrizzabile:
#       aggiorna la mappa con chiave=xpathElem, valore=parametri validi per elem come input
#
#   return output
##########################################################################

##############################
# Correttezza dell'algoritmo
##############################

################################################################################
# Si tratta di una visita in profodità in pre-ordine sul DOM XML del modello BPMN.
# Ricordiamo che il DOM BPMN è simile a questo <immagine del DOM>
# pertanto, con una visita in pre-ordine (realizzabile tramite etree._Element.iterchildren)
# ci assicuriamo di controllare quali elementi definiti nel modello sono parametrizzabili,
# escludendo quelli che non riguardano la semantica del modello stesso.
################################################################################


def parseBPMN(
    bpmnModel: str | etree._ElementTree | etree._Element,
) -> List[Possible]:
    """
    Given a BP model, this function will return a list of the parameters
    that can be assigned to it.

    Parameters
    ----------
    `bpmnModel` : `Union[str, etree._ElementTree, etree._Element]`
        Modello BPMN

    Returns
    -------
    `List[Possible]`
        List of parameters that can be assigned to the given BP model

    Raises
    ------
    `TypeError`
        If the given element is not a valid XML file
    """
    # TODO: some check against schema to assert if the given xml is a valid
    # BPMN model

    if isinstance(bpmnModel, str):
        bpmnModel = etree.XML(bpmnModel)

    elif isinstance(bpmnModel, etree._ElementTree):
        bpmnModel = bpmnModel.getroot()

    if not etree.iselement(bpmnModel):
        raise TypeError("Given bpmnModel is not a valid xml element.")

    return _parseBPMRecursive(bpmnModel)


def _parseBPMRecursive(bpmElement: etree._Element) -> List[Possible]:
    """
    Recursive function that actually perform the parse algorithm with a
    depth first search approach

    Parameters
    ----------
    `bpmElement` : `etree._Element`
        Current element to analyze

    Returns
    -------
    `List[Possible]`
        List of `Possible` that can be assigned to the given element
    """
    # if we went pass "semantic" section, there's no meaning in going further
    if "bpmndiagram" in etree.QName(bpmElement).localname.lower():
        return []

    outputList: List[Possible] = list()
    # if bpmElement has childern, iterate over them
    if len(bpmElement) > 0:
        for childElement in bpmElement.iterchildren():
            # if we went pass "semantic" section, there's no meaning in going
            # further
            if "bpmndiagram" in etree.QName(childElement).localname.lower():
                return outputList
            childPossibles = _parseBPMRecursive(childElement)
            if isinstance(childPossibles, list) and len(childPossibles) > 0:
                outputList.extend(childPossibles)

    # else provide a list of Possible for it
    possible = getPossibleFromElement(bpmElement)
    if possible is not None:
        outputList.append(possible)

    return outputList


def getPossibleFromElement(element: etree._Element) -> Optional[Possible]:
    """
    Function that return a Possible for the given element

    Parameters
    ----------
    `element`: `etree._Element`
        Element for which perform the analysis

    Returns
    -------
    `None`
        If the given element appears to not be parameterizable, that is,
        it generate no `conditions`

    `Possible`
        The possible parameters assignable to the given element
    """
    conditions = getConditions(element)
    if len(conditions) == 0:
        return None

    xpathElement = f"//*[@id='{element.attrib.get('id')}']"
    possibleInputs = possibleInputFromConditions(conditions)
    possibleOutputs = possibleOutputFromConditions(possibleInputs, conditions)

    return Possible(
        xpathID=xpathElement, input=possibleInputs, output=possibleOutputs
    )


def possibleInputFromConditions(
    conditions: List[BPMNAllParameterizable],
) -> List[PossibleInputs]:
    """
    possibleInputFromConditions take a list of condition and return the
    corresponding list of possible inputs

    Parameters
    ----------
    `conditions`: `List[BPMNAllParameterizable]`
        List of condition for which provide a list of possible BPSim
        input parameters

    Returns
    -------
    `List[PossibleInputs]`
        List of possible BPSim input associated with the given list of
        conditions

    Raises
    ------
    `ValueError`
        If this function get an empty list
    """
    if len(conditions) == 0:
        raise ValueError("Expected non empy list of conditions")

    # List of InputOnCondition
    possibleInputs = [
        ioc
        for ioc in INPUT_APPLICABILITY
        for c in conditions
        if ioc.condition == c
    ]
    possibleInputs = [
        pi
        for ioc in possibleInputs
        for pi in ioc.allPossibleInputs(conditions)
    ]
    return possibleInputs


def possibleOutputFromConditions(
    possibleInputs: List[PossibleInputs],
    conditions: List[BPMNAllParameterizable],
) -> List[PossibleOutputs]:
    """
    possibleOutputFromConditions take a list of condition and a list of
    possible BPSim input parameters for the list and return the
    corresponding list of possible ouptuts

    Parameters
    ----------
    `possibleInputs`: `List[PossibleInputs]`
        List of possible inputs for which to retrieve the list of
        possible BPSim output parameters

    `conditions`: `List[BPMNAllParameterizable]`
        List of condition for which provide a list of possible BPSim
        output parameters

    Returns
    -------
    `List[PossibleOutputs]`
        List of possible BPSim output parameters associated with the given
        list of conditions

    Raises
    ------
    `ValueError`
        If this function get an empty conditions' list
    """
    if len(conditions) == 0:
        raise ValueError("Expected non empy list of conditions")

    possibleOutputs = [
        deepcopy(possibleOutput)
        for possibleInput in possibleInputs
        for possibleOutput in OUTPUT_APPLICABITY
        if possibleOutput.groupParameter == possibleInput.groupParameter
    ]

    if (
        BPMNAttributes.resource in conditions
        or BPMNAttributes.resourcerole in conditions
    ):
        for possibleOutput in OUTPUT_RESOURCE:
            if all(
                po.groupParameter != possibleOutput.groupParameter
                for po in possibleOutputs
            ):
                possibleOutputs.append(deepcopy(possibleOutput))

    elif BPMNActivities.process in conditions:
        for possibleOutput in OUTPUT_DECOMPOSABLE:
            if all(
                po.groupParameter != possibleOutput.groupParameter
                for po in possibleOutputs
            ):
                possibleOutputs.append(deepcopy(possibleOutput))
    elif (
        BPMNActivities.transaction in conditions
        or BPMNActivities.task in conditions
        or BPMNActivities.subprocess in conditions
        or BPMNActivities.eventsubprocess in conditions
        or BPMNActivities.callactivity in conditions
    ):
        for possibleOutput in OUTPUT_DECOMPOSABLE:
            if all(
                po.groupParameter != possibleOutput.groupParameter
                for po in possibleOutputs
            ):
                possibleOutputs.append(deepcopy(possibleOutput))

    return possibleOutputs

from enum import Enum


class TimeUnit(Enum):
    '''
    Classe Per gestire time unit
    '''
    MS = 'ms'
    S = 's'
    MIN = 'min'
    HOUR = 'hour'
    DAY = 'day'
    YEAR = 'year'

    @classmethod
    def isTimeUnit(cls, value: str):
        if value.upper() in cls.__members__.keys():
            return True
        return False

    @classmethod
    def ms(cls):
        return cls.MS

    @classmethod
    def s(cls):
        return cls.S

    @classmethod
    def min(cls):
        return cls.MIN

    @classmethod
    def hour(cls):
        return cls.HOUR

    @classmethod
    def day(cls):
        return cls.DAY

    @classmethod
    def year(cls):
        return cls.YEAR

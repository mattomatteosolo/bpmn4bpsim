from abc import ABC
from lxml import etree
from datetime import datetime, timedelta
from isodate import duration_isoformat
from typing import Union
from bpmn4bpsim.bpsimclasses import BPSIM, BPSIM_NSMAP
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.ParameterValues.ParameterValue import ParameterValue


class ConstantParameter(ParameterValue, ABC):
    '''Classe dei parametri "costanti" di BPSim. Estende `ParameterValue`.

    Parameters
    validFor : `Calendar`
        Come per ParameterValue
    value : `object`
        Valore del parametro. Il tipo dipende da quale specializzazione si considera
    '''

    def __init__(self, value, validFor=None):
        super().__init__(validFor=validFor)
        self.__value = value

    def __eq__(self, other: 'ConstantParameter'):
        return super().__eq__(other) and self.value == other.value

    def __hash__(self):
        return hash((super().__hash__(), hash(self.value)))

    @property
    def value(self):
        return self.__value

    def toXML(self):
        '''Funzione che serializza il parametro in XML

        Returns
        -------
        `etree._Element`
            Serializzazione in XML (oggetto etree._Element) dell'oggetto corrente, secondo lo schem a di BPSim
        '''
        className = str(self.__class__).split('.')[-1][:-2]
        attrDict = {key.split('__')[-1]: value for (key, value)
                    in self.__dict__.items() if value != None}
        constParam = etree.Element(BPSIM + className, nsmap=BPSIM_NSMAP)
        for item in attrDict.items():
            if isinstance(item[1], datetime):
                item = (item[0], item[1].isoformat()[0:19] + 'Z')
            elif isinstance(item[1], timedelta):
                item = (item[0], duration_isoformat(item[1]))
            elif isinstance(item[1], bool):
                item = (item[0], str(item[1]).lower())
            elif isinstance(item[1], TimeUnit):
                item = (item[0], item[1].value)
            constParam.set(item[0], str(item[1]))
        return constParam


class StringParameter(ConstantParameter):
    '''
    ConstantParameter con valore di tipo string.

    Parameters
    ----------
    validFor : `str`
        Come per ParameterValue
    value : `string`
        Valore del parametro
    '''

    def __init__(self, value, validFor=None):
        ########################################
        # Type checking
        ########################################
        if not isinstance(value, str):
            raise TypeError(
                'Attesa una stringa per il parametro value. Ricevuto: ' + str(type(value)))
        if validFor != None and not isinstance(validFor, str):
            raise TypeError(
                'Atteso una stringa, id di calendar. Ricevuto: ' + str(type(validFor)))
        ########################################
        # Inizializzazione
        ########################################
        super().__init__(value, validFor)


class NumericParameter(ConstantParameter):
    '''ConstantParameter con valore di tipo Long.

    Parameters
    ----------
    validFor : `str`
        Come per ParameterValue
    value : `int`
        Valore del parametro
    timeUnit : `TimeUnit`
        Utilizzato per sovrascrivere il valore di baseTimeUnit, definito a livello di ScenarioParameter
    '''

    def __init__(self, value, validFor: str = None, timeUnit: Union[str, TimeUnit] = None):
        ########################################
        # Type checking
        ########################################
        # issublcass(bool,int) = True
        if isinstance(value, bool):
            raise TypeError('Atteso, per value, un intero (Long)')
        if not isinstance(value, int):
            raise TypeError('Atteso un intero (Long)')

        if validFor != None and not isinstance(validFor, str):
            raise TypeError('L\'argomento validFor deve essere una stringa')

        if timeUnit is not None:
            if not isinstance(timeUnit, TimeUnit):
                if isinstance(timeUnit, str) and not TimeUnit.isTimeUnit(timeUnit):
                    raise ValueError('timeUnit dovrebbe essere una stringa')

        ########################################
        # Inizializzazione
        ########################################
        super().__init__(value, validFor)
        self.__timeUnit = TimeUnit(timeUnit) if timeUnit != None else None

    def __eq__(self, other: 'NumericParameter') -> bool:
        return super().__eq__(other) and self.__timeUnit == other.__timeUnit

    def __hash__(self):
        return hash((super().__hash__(), hash(self.__timeUnit)))

    @property
    def timeUnit(self):
        return self.__timeUnit


class FloatingParameter(ConstantParameter):
    '''
    ConstantParameter con valore di tipo float.

    Parameters
    ----------
    validFor : `str`
        Come per ParameterValue
    value : `string`
        Valore del parametro
    timeUnit : `TimeUnit`
        Utilizzato per sovrascrivere il valore di `baseTimeUnit`, definito a livello di `ScenarioParameter`
    '''

    def __init__(self, value, validFor=None, timeUnit=None):
        ########################################
        # Type checking
        ########################################
        if not isinstance(value, float):
            raise TypeError()
        if timeUnit != None:
            if not isinstance(timeUnit, str):
                raise TypeError()
            if not TimeUnit.isTimeUnit(timeUnit):
                raise ValueError()
        ########################################
        # Inizializzazione
        ########################################
        super().__init__(value, validFor)
        self.__timeUnit = TimeUnit(timeUnit) if timeUnit != None else None

    def __eq__(self, other: 'FloatingParameter') -> bool:
        return super().__eq__(other) and self.__timeUnit == other.__timeUnit

    def __hash__(self):
        return hash(((super().__hash__(), hash(self.__timeUnit))))

    @property
    def timeUnit(self):
        return self.__timeUnit


class BooleanParameter(ConstantParameter):
    '''ConstantParameter con valore di tipo boolean.

    Parameters
    ----------
    validFor : `str`
        Come per ParameterValue
    value : `bool`
        Valore del parametro
    '''

    def __init__(self, value, validFor=None):
        if not isinstance(value, bool):
            raise TypeError()
        super().__init__(value, validFor)


class DurationParameter(ConstantParameter):
    '''ConstantParameter con valore di tipo timedelta.

    Parameters
    ----------
    validFor : `str`
        Come per ParameterValue
    value : `timedelta`
        Valore del parametro
    '''

    def __init__(self, value, validFor=None):
        if not isinstance(value, timedelta):
            raise TypeError()
        super().__init__(value, validFor=validFor)


class DateTimeParameter(ConstantParameter):
    '''ConstantParameter con valore di tipo datetime.

    Parameters
    ----------
    validFor : `Calendar`
        Come per ParameterValue
    value : `string`
        Valore del parametro
    '''

    def __init__(self, value, validFor=None):
        if not isinstance(value, datetime):
            raise TypeError(
                'Atteso un parametro di tipo datetime per value. Ricevuto: ' + str(type(value)))
        super().__init__(value, validFor)

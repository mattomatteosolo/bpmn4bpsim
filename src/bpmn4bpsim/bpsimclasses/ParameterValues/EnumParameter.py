from collections import Counter
from lxml import etree
from typing import List
from bpmn4bpsim.bpsimclasses import BPSIM, BPSIM_NSMAP
from bpmn4bpsim.bpsimclasses.ParameterValues.ParameterValue import ParameterValue
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import ConstantParameter


class EnumParameter(ParameterValue):
    '''
    Classe che realizza la classe EnumParameter di BPSim, il cui obbiettivo base è fornire una sorta di array di ConstantParameter

    Parameters
    ----------
    validFor : `str`
        Come per ParameterValue
    paramList : `list<ConstantParameter>`
        Lista di ConstantParameter. Ovviamente lista delle classi che estendono ConstantParameter
    '''

    def __init__(self, paramList: List[ConstantParameter], validFor: str = None):
        if not isinstance(paramList, list):
            raise TypeError(
                'Attesa una lista di ConstantParameter come come valore per paramList. Ricevuto: ' + str(type(paramList)))
        # minOccurs=1 di default
        if len(paramList) < 1:
            raise ValueError('Ricevuta una lista vuota')
        for param in paramList:
            if not isinstance(param, ConstantParameter):
                raise ValueError(
                    'Attesa una lista di ConstantParameter per paramList. Ricevuto: ' + str(type(paramList)))

        super().__init__(validFor=validFor)
        self.__paramList = paramList

    def __eq__(self, other: 'EnumParameter') -> bool:
        return super().__eq__(other) and Counter(self.values) == Counter(other.values)

    def __hash__(self):
        h = ''
        for const_value in self.values:
            h = hash(const_value)
        return hash((super().__hash__(), h))

    @property
    def values(self) -> List[ConstantParameter]:
        '''
        Lista dei parametri ParameterValue
        '''
        return self.__paramList

    def toXML(self):
        '''
        Metodo per la serializzazione in XML

        Returns
        -------
        etree._Element
            Oggetto XML serializzazione dell'oggetto EnumParameter corrente, secondo lo schema BPSim
        '''
        attrDict = {key.split('__')[-1]: value for (key, value)
                    in self.__dict__.items() if value != None}
        attrDict.pop('paramList')

        enParam = etree.Element(BPSIM + 'EnumParameter', nsmap=BPSIM_NSMAP)
        if len(attrDict.keys()) > 0:
            for item in attrDict.items():
                enParam.set(item[0], str(item[1]))

        for param in self.__paramList:
            enParam.append(param.toXML())

        return enParam

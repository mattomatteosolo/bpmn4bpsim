from abc import ABC, abstractmethod

from lxml import etree


class ParameterValue(ABC):
    '''Classe che realizza la classe omonima definita in BPSim

    Gli altri parametri sono generati ed utilizzati soltanto dal simulatore come output, pertanto, non ha senso deserializzarli qui

    Parameters
    ----------
    validFor : `Calendar`
        Calendario di applicabilità del parametro
    '''

    def __init__(self, validFor: str = None):
        if validFor != None and not isinstance(validFor, str):
            raise TypeError(
                'Atteso un parametro di tipo str per validFor. Ricevuto: ' + str(type(validFor)))
        self.__validFor = validFor

    def __eq__(self, other: 'ParameterValue') -> bool:
        return self.validFor == other.validFor

    def __hash__(self):
        return hash(self.__validFor)

    @property
    def validFor(self):
        '''
        Id del bpsim:Calendar in cui è valido il parametro selzionato. Utilizzabile per effettuare ricerche sull'albero xml.
        '''
        return self.__validFor

    @validFor.setter
    def validFor(self, validFor):
        '''
        Setter per validFor. Sarà compito dell'utente fare un check sui calendar disponibili nello scenario ed invocare questo metodo.

        Parameters
        ----------
        validFor : `str`
            Id del calendar per cui questo valore di parametro è valido :type `string`
        '''
        self.__validFor = validFor

    @abstractmethod
    def toXML(self) -> etree._Element:
        pass

from collections import Counter
from lxml import etree
from typing import List
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.DistributionParameter import DistributionParameter
from bpmn4bpsim.bpsimclasses import ParamNonValido, BPSIM, BPSIM_NSMAP
from bpmn4bpsim.bpsimclasses.ParameterValues import EnumParameter, ConstantParameter, ExpressionParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ParameterValue import ParameterValue


class UserDistributionDataPoint(object):
    '''
    Classe utilizzata per realizzare l'omonima classe di BPSim. L'obiettivo è di fornire un range di valori definendo la probabilità di ciascuno di verificarsi. Ogni valore ha quindi una probabilità e un valore, espresso ancora una volta secondo i canoni di BPSim con un Parameter.

    Parameters
    ----------
    probability : `float`
        Probabilità che questo valore si verifichi, compreso tra 0 e 1. Per ogni UserDistribution, la somma di tutti questi valori deve essere pari a 1.0
    value : `ParameterValue`
        Valore effettivo da considerare
    '''

    def __init__(self, probability: float, value: ParameterValue):
        if not isinstance(probability, float):
            raise TypeError(
                'Atteso un float per probability. Ricevuto: ' + str(type(probability)))
        if not isinstance(value, ParameterValue):
            raise TypeError(
                'Atteso un oggetto di tipo ParameterValue per value. Ricevuto: ' + str(type(value)))
        if probability > 1.0 or probability <= 0.0:
            raise ValueError(
                'Atteso un valore nell\'intorno (0.0,1.0]. Ricevuto: ' + str(probability))
        self.__probability = probability
        self.__value = value

    def __eq__(self, other: 'UserDistributionDataPoint') -> bool:
        return self.probability == other.probability and self.value == other.value

    def __hash__(self) -> int:
        return hash((self.probability, self.value))

    @staticmethod
    def __valueType(valueType):
        '''
        Funzione helper che data una stringa, restituisce una sotto classe di `ParameterValue`. Se la stringa non combacia con nessuna di queste classi, invoca un'eccezione

        Parameters
        ----------
        valueType : `string`
            Stringa che contiene il nome della classe da utilizzare
        pvalueClass : `class`
            Sottoclasse di ParameterValue
        '''
        # recupera il tipo di param value
        # se si trova in questo modulo
        # if valueType in dir(modules[__name__]):
        #     valueType = getattr(modules[__name__], valueType)
        # se si trova in ConstantParameter
        if valueType in dir(ConstantParameter):
            valueType = getattr(ConstantParameter, valueType)
        elif valueType in dir(EnumParameter):
            valueType = getattr(EnumParameter, valueType)
        elif valueType in dir(ExpressionParameter):
            valueType = getattr(ExpressionParameter, valueType)
        else:
            raise ParamNonValido(
                'Non esiste una classe BPSIM con questo nome ' + valueType + 'che specifichi un ParamValue')

        return valueType

    @property
    def probability(self) -> float:
        return self.__probability

    @property
    def value(self) -> ParameterValue:
        return self.__value

    def toXML(self):
        '''
        Metodo per la serializzazione in XML

        Returns
        -------
        etree._Element
            Serializzazione in XML (con lxml) dell'oggetto corrent
        '''
        udDataPoint = etree.Element(
            BPSIM + 'UserDistributionDataPoint', nsmap=BPSIM_NSMAP, probability=str(self.__probability))
        udDataPoint.append(self.__value.toXML())
        return udDataPoint


class UserDistribution(DistributionParameter):
    '''
    Classe che realiza la classe UserDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    points : `UserDistributionDataPoint`
        Lista di di punti definita dall'utente
    discrete : `float`
        Se True la distribuzione è descreta, se False continua. Default False
    '''

    def __init__(self, points: List[UserDistributionDataPoint], discrete=False, timeUnit=None):
        ####################
        # Type check
        ####################
        if not isinstance(points, list):
            raise TypeError(
                'Attesa una lista di UserDistributionDataPoint per points. Ricevuto: ' + points.__class__.__name__)
        if not isinstance(discrete, bool):
            raise TypeError(
                'Atteso un valore booleano per discrete. Ricevuto: ' + str(type(discrete)))
        ####################
        # Value check
        ####################
        if len(points) < 1:
            raise ValueError('Attesa una lista NON vuota di punti.')
        probSum = 0.0
        for point in points:
            if not isinstance(point, UserDistributionDataPoint):
                raise ValueError(
                    'Attesa una lista di UserDistributionDataPoint per points. Ricevuto in points: ' + point.__class__.__name__)
            probSum += point.probability
        if (probSum < 1.0) or probSum > 1.0:
            raise ValueError(
                'Attesa una lista di UserDistributionDataPoint per points, in cui la somma delle diverse probability sia esattamente 1.0')
        ####################
        # Inizializzazione
        ####################
        super().__init__(timeUnit=timeUnit)
        self.__points = points
        self.__discrete = discrete

    def __eq__(self, other: 'UserDistribution') -> bool:
        return self.discrete == other.discrete and Counter(self.points) == Counter(other.points)

    @property
    def points(self) -> List[UserDistributionDataPoint]:
        return self.__points

    @property
    def discrete(self) -> bool:
        return self.__discrete

    def toXML(self):
        '''
        Metodo per la serializzazione in XML

        Returns
        -------
        etree._Element
            Oggetto corrente serializzato in XML (lxml)
        '''
        usrDistr = etree.Element(
            BPSIM + 'UserDistribution', nsmap=BPSIM_NSMAP, discrete=str(self.__discrete).lower())
        for point in self.__points:
            usrDistr.append(point.toXML())

        return usrDistr

"""
Modulo per i ParameterValue di tipo DistributionParameter

Contiene tutte le distribuzioni note supportate da BPSim
"""
from abc import ABC
from lxml import etree
from bpmn4bpsim.bpsimclasses.ParameterValues.ParameterValue import (
    ParameterValue,
)
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses import BPSIM, BPSIM_NSMAP


class DistributionParameter(ParameterValue, ABC):
    """
    Classe che realizza la classe astratta DistributionParameter di BPSim

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    """

    def __init__(self, validFor=None, timeUnit: TimeUnit | str = None):
        if timeUnit != None and isinstance(timeUnit,str):
            if  not TimeUnit.isTimeUnit(timeUnit):
                raise TypeError(
                    "Atteso un TimeUnit valido per timeUnit. Ricevuto: "
                    + str(type(timeUnit))
                )
            timeUnit = TimeUnit[timeUnit]
        super().__init__(validFor=validFor)
        self.__timeUnit = timeUnit

    def __eq__(self, other: "DistributionParameter") -> bool:
        return super().__eq__(other) and self.timeUnit == other.timeUnit

    def __hash__(self):
        return hash((super().__hash__(), self.timeUnit))

    @property
    def timeUnit(self) -> TimeUnit | None:
        return self.__timeUnit

    def toXML(self):
        attrDict = {
            key.split("__")[-1]: value
            for (key, value) in self.__dict__.items()
            if value != None
        }
        className = str(self.__class__).split(".")[-1][:-2]
        distParam = etree.Element(BPSIM + className, nsmap={**BPSIM_NSMAP})

        for item in attrDict.items():
            distParam.set(item[0], str(item[1]))

        return distParam


class BetaDistribution(DistributionParameter):
    """
    Classe che realiza la classe BetaDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    shape : `float`
        Valore della 'forma'
    scale : `float`
        Valore della 'scala'
    """

    def __init__(self, shape, scale, timeUnit=None, validFor=None):
        if not isinstance(shape, float):
            raise TypeError()
        if not isinstance(scale, float):
            raise TypeError()
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.shape = shape
        self.scale = scale


class BinomialDistribution(DistributionParameter):
    """
    Classe che realiza la classe BinomialDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    probability : `float`
        Probabilità di successo
    trials : `int`
        Numero di prove
    """

    def __init__(self, probability: float, trials: int, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.probability = probability
        self.trials = trials


class ErlangDistribution(DistributionParameter):
    """
    Classe che realiza la classe ErlangDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    mean : `float`
        Valore medio
    k : `int`
        Valore di 'K' tipico della distribuzione di Erlang
    """

    def __init__(self, mean: float, k: int, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mean = mean
        self.k = k


class GammaDistribution(DistributionParameter):
    """
    Classe che realiza la classe GammaDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    shape : `float`
        Valore della 'forma'
    scale : `float`
        Valore della 'scala'
    """

    def __init__(self, shape: float, scale: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.shape = shape
        self.scale = scale


class LogNormalDistribution(DistributionParameter):
    """
    Classe che realiza la classe LogNormalDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    mean : `float`
        Il valore medio
    standardDeviation : `float`
        Valore della deviazione standard
    """

    def __init__(self, mean: float, standardDeviation: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mean = mean
        self.standardDeviation = standardDeviation


class NegativeExponentialDistribution(DistributionParameter):
    """
    Classe che realiza la classe NegativeExponentialDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    mean : `float`
        Valore medio
    """

    def __init__(self, mean: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mean = mean


class NormalDistribution(DistributionParameter):
    """
    Classe che realiza la classe NormalDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    mean : `float`
        Valore della 'forma'
    standardDeviation : `float`
        Valore della 'scala'
    """

    def __init__(self, mean: float, standardDeviation: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mean = mean
        self.standardDeviation = standardDeviation


class PoissonDistribution(DistributionParameter):
    """
    Classe che realiza la classe PoissonDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    mean : `float`
        Valore medio
    """

    def __init__(self, mean: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mean = mean


class TriangularDistribution(DistributionParameter):
    """
    Classe che realiza la classe TriangularDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    mode : `float`
        Il valore più probabile
    min : `float`
        Il limite inferiore dei valori generati
    max : `float`
        Il limite superioire dei valori generati
    """

    def __init__(self, mode: float, minV: float, maxV: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mode = mode
        self.min = minV
        self.max = maxV


class TruncatedNormalDistribution(DistributionParameter):
    """
    Classe che realiza la classe TruncatedNormalDistribution

    Parameters
    ----------
    mean : float
        Valore medio
    standardDeviation : float
        Valore della deviazione standard
    minV : float
        Limite inferiore dei valori generati
    maxV : float
        Limite superiore dei valori generati
    timeUnit : TimeUnit
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    """

    def __init__(
        self,
        mean: float,
        standardDeviation: float,
        minV: float,
        maxV: float,
        timeUnit=None,
        validFor=None,
    ):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.mean = mean
        self.standardDeviation = standardDeviation
        self.min = minV
        self.max = maxV

    def __eq__(self, other: "TruncatedNormalDistribution") -> bool:
        return (
            super().__eq__(other)
            and self.min == other.min
            and self.max == other.max
            and self.standardDeviation == other.standardDeviation
            and self.mean == other.mean
        )

    def __hash__(self):
        return hash(
            (
                super().__hash__(),
                self.min,
                self.max,
                self.mean,
                self.standardDeviation,
            )
        )


class UniformDistribution(DistributionParameter):
    """
    Classe che realiza la classe UniformDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    min : `float`
        Limite inferiore dei valori generati
    max : `float`
        Limite superiore dei valori generati
    """

    def __init__(self, minV: float, maxV: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.min = minV
        self.max = maxV


class WeibullDistribution(DistributionParameter):
    """
    Classe che realiza la classe WeibullDistribution

    Parameters
    ----------
    timeUnit : `TimeUnit`
        Parametro che serve a sovrascrive il timeUnit dello scenario per questo parametro
    shape : `float`
        Valore della 'forma'
    scale : `float`
        Valore della 'scala'
    """

    def __init__(self, shape: float, scale: float, timeUnit=None, validFor=None):
        super().__init__(timeUnit=timeUnit, validFor=validFor)
        self.shape = shape
        self.scale = scale

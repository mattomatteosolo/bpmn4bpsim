# Per quanto riguarda questo tipo di parametri, siamo di fronte ad un argomento piuttosto ampio e non ritengo lo scopo della tesi la realizzazione di un parser di funzioni XPath. Quello che farò, sarà informare della presenza di questa possibilità, come delle aggiunti di BPSim. Ovviamente, fornirò un minimo di type checking sulle funzioni di BPSim, ma non andrò oltre

from lxml import etree
from .ParameterValue import ParameterValue
from .. import BPSIM, BPSIMNS, ParamNonValido, BPSIM_NSMAP


class ExpressionParameter(ParameterValue):
    '''
    NOTA PER LA SERIALIZZAZIONE: lxml non riconosce alcuni caratteri, tra cui '"', < e >, pertanto, si invitano gli utilizzatori a correggere questo comportamento con .replace('&lt;','<') o .replace('&gt;','>'). 

    NOTA 2: Sembra tutto regolare, e quindi non necessario correggere questi elementi.

    Classe che realizza la classe BPSim ExpressionParameter. In questa implementazione assumiamo che l'utente sia a conoscenza delle funzioni standard di XPath, quindi forniremo soltanto un rimando alla documentazione di W3S quando necessaio, e forniremo la definizione delle funzioni introdotte da BPSim senza implementazione. Sarà infatti compito del simulatore definirne il comportamento

    :param `value` Espressione XPath :type `string`

    :param `validFor` Id del calendar per cui questo valore è valido :type `string`
    '''

    __NS = etree.FunctionNamespace(BPSIMNS)
    __NSMAP = {
        'bpsim': BPSIM
    }

    def __init__(self, value, validFor=None):
        # try:
        #     # value è una stringa che rappresenta una funzione xpath, quindi nel namespace fs
        #     if 'bpsim' in value:
        #         etree.XPath(value, namespaces=self.__NSMAP)
        #     else:
        #         etree.XPath(value)
        #     # si tratta solo di una valutazione di una valutazione delle sintasi
        # except:
        if not isinstance(value, str):
            raise ParamNonValido(
                'L\'oggetto passata non è una stringa e non possiamo pertanto instanziarla come valore per un\'espressione XPath')
        super().__init__(validFor=validFor)
        self.__value = value

    @property
    def value(self):
        return self.__value

    @value.setter
    def setValue(self, value):
        try:
            if 'bpsim' in value:
                etree.XPath(value, namespaces=self.__NSMAP)
            else:
                etree.XPath(value)
        except:
            raise ParamNonValido('Inserita espressione non valida: ' + value)
        self.__value = value

    @staticmethod
    @__NS('getProperty')
    def getProperty(name):
        '''
        '''
        pass

    @staticmethod
    @__NS('getResource')
    def getResource(name, qty):
        '''
        '''
        pass

    @staticmethod
    @__NS('getResourceByRoles')
    def getResourceByRoles(roles, qty):
        pass

    @staticmethod
    @__NS('orResource')
    def orResource(*resources):
        pass

    def toXML(self):
        '''
        Metodo per la serializzazione XML (lxml)

        Returns
        -------
        etree._Element
            Oggetto XML (lxml) ottenuto serializzando l'oggetto corrente
        '''
        return etree.Element(BPSIM + 'ExpressionParameter', nsmap=BPSIM_NSMAP, value=self.__value)

from icalendar import Calendar as iCalendar, Event, vRecur
from dateutil.rrule import rrulestr
from isodate import parse_datetime, parse_duration
import uuid
from lxml import etree
import json
from typing import Union
from bpmn4bpsim.bpsimclasses import BPSIM, BPSIM_NSMAP


class Calendar(object):
    """Classe che realizza la classe omonima di BPSim. Come da specifiche, ogni
    oggetto calendar conterrà informazioni di ditpo VEVENT

    Parameters
    ----------
    id : `string`
        Identificativo univoco dell'oggetto. Default None, significa che viene
        creato come un uuid
    name : `string`
        Nome descrittivo per l'oggetto calendar corrente
    calendar : `icalendar`
        Oggetto di tipo iCalendar, serializzato come testo dell'oggetto
        calendar
    """

    def __init__(
        self,
        name: str,
        calendar: Union[iCalendar, dict, bytes],
        id: str = None,
    ):
        ########################################
        # Type checking + conversione calendar
        ########################################
        # Calendar estende dict
        if isinstance(calendar, dict):
            if not isinstance(calendar, iCalendar):
                calendar = Calendar.calendarFromDict(calendar)
        elif isinstance(calendar, str) or isinstance(calendar, bytes):
            calendar = iCalendar.from_ical(calendar)

        ####################
        # Inizializzazione
        ####################
        self.__id = id if id is not None else str(uuid.uuid1())
        self.__name = name
        self.__calendar = calendar

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Calendar):
            return False
        return (
            self.__name == other.__name
            and self.__id == other.__id
            and self.__calendar.to_ical().decode("utf-8")
            == other.__calendar.to_ical().decode("utf-8")
        )

    @property
    def id(self) -> str:
        return self.__id

    @property
    def name(self) -> str:
        return self.__name

    @property
    def calendar(self) -> iCalendar:
        return self.__calendar

    @staticmethod
    def calendarFromDict(calendar: dict):
        """
        NOTA PER LA SERIALIZZAZIONE: XML non riconosce la codifica per CR+LF e
        lascia un brutto valore, pertanto, quando si salverà il file, sarà
        premura di chi lo fa, di risolvere il problema sostituendo "\\r" con
        "" con replace('\\r','').

        Funzione helper che prende in input un dizionario o una stringa
        contenente un json, e converte tale oggetto in oggetto di tipo
        iCalendar utilizzabile in BPSim. Detto questo, tale oggetto dovrà
        contenre un elemento VEVENT ("event").

        Parameters
        ----------
        calendar : dict
            Oggetto iCalendar da formattare
        """
        if isinstance(calendar, str):
            calendar = json.loads(calendar)

        ical = iCalendar()
        if calendar.get("prodid") is None:
            ical["prodid"] = "BPSimaaS"

        if calendar.get("version") is None:
            ical["version"] = "2.0"
        for itemName, itemValue in calendar.items():
            if itemName == "event":
                for event in itemValue:
                    e = Event()
                    for eventProperty, eventPropertyValue in event.items():
                        if (
                            eventProperty == "dtstart"
                            or eventProperty == "dtend"
                            or eventProperty == "dtstamp"
                        ):
                            e.add(
                                eventProperty,
                                parse_datetime(eventPropertyValue),
                            )
                        elif eventProperty == "duration":
                            e.add(
                                eventProperty,
                                parse_duration(eventPropertyValue),
                            )
                        else:
                            e.add(eventProperty, eventPropertyValue)
                    ical.add_component(e)
            else:
                ical.add(itemName, itemValue)
        return ical

    def toXML(self):
        """
        Metodo per la serializzazione in XML del Calendar

        Returns
        -------
        etree._Element
            Serializzazione Calendar in formato XML secondo BPSim
        """
        calendar = etree.Element(
            BPSIM + "Calendar",
            nsmap=BPSIM_NSMAP,
            id=self.__id,
            name=self.__name,
        )

        calendar.text = (
            self.__calendar.to_ical()
            .decode("utf-8")
            .replace("\\", "")
            .replace("\r", "")
        )
        return calendar

    @staticmethod
    def parse_rrule(rrule: str) -> dict:
        """
        Metodo che prende in input una stringa e prova a parsarla come una
        rrule

        Parameters
        ----------
        rrule : str
            Stringa da parsare

        Returns
        -------
        dict
            Dizionario che contiene tutti i dati della rrule

        Raises
        ------
        TypeError
            Se rrule non è stringa

        ValueError
            Se la stringa non risulta parsabile
        """
        try:
            rrulestr(rrule)
        except Exception:
            raise ValueError("Non una valida rrule")
        if "RRULE:" in rrule:
            rrule = rrule.replace("RRULE:", "")
        return vRecur.from_ical(rrule)

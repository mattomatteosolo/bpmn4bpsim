from collections import Counter
from lxml import etree
from enum import Enum
from typing import List
from bpmn4bpsim.bpsimclasses import BPSIM, BPSIM_NSMAP
from bpmn4bpsim.bpsimclasses.ParameterValues.ParameterValue import (
    ParameterValue,
)
from bpmn4bpsim.bpsimclasses.ResultType import ResultType


class Parameter(object):
    """
    Classe che realizza la classe Parameter di BPSim. In questa classe specifichiamo il metodo toXML con un parametro aggiuntivo che rappresenta il nome del parametro che intendiamo rappresentare. Sarà onere delle classi chiamanti fornire un nome nel namespace dei parametri BPSim.

    Parameters
    ----------
    value : `list`
        Questo parametro è utilizzato per contenera la lista di `ParameterValue` associati al parametro
    resultRequest : `list`
        Parametro di solo input, utilizzato per indicare i tipi di risultato che ci aspettiamo in output, cioè nello scenario di output.
    """

    def __init__(
        self,
        parameterName: str,
        value: List[ParameterValue] = [],
        resultRequest: List[ResultType] = [],
    ):
        if len(value) == 0 and len(resultRequest) == 0:
            raise ValueError(
                "At least one from values or result requests must be not an "
                "empty list."
            )
        if len(value) > 0:
            value = list(set(value))
        self.__value = value
        if len(resultRequest) > 0:
            resultRequest = list(set(resultRequest))
        self.__resultRequest = resultRequest
        self.__name = parameterName

    def __hash__(self):
        h_val = h_res = ""
        if self.value:
            for val in self.value:
                h_val = hash((h_val, val))
        if self.value:
            for val in self.value:
                h_res = hash((h_res, val))
        return hash((self.parameterName, h_val, h_res))

    def __eq__(self, other: "Parameter") -> bool:
        if other == None:
            return False

        return (
            self.parameterName == other.parameterName
            and Counter(self.value) == Counter(other.value)
            and Counter(self.resultRequest) == Counter(other.resultRequest)
        )

    @classmethod
    def isValidValue(cls, values: list):
        """
        Metodo che prende una lista e controlla se questa è una lista valida di ParameterValue

        Parameters
        ----------
        values : list
            Lista di paramValues da controllare

        Returns
        -------
        bool
            True se tra tutti gli elementi della lista uno soltanto è privo del campo vaildFor, False altrimenti o se il parametro non è una lista, o se uno degli elementi non è un ParameterValue
        """
        if not isinstance(values, list):
            return False
        foundDefault = False
        for paramValue in values:
            if not isinstance(paramValue, ParameterValue):
                return False
            if foundDefault and not paramValue.validFor:
                return False
            elif not foundDefault and not paramValue.validFor:
                foundDefault = True
        return True

    @classmethod
    def isValidResult(cls, resultRequests: list):
        """
        Metodo che prende in input una lista e controlla se questa è una lista valida di ResultType

        Parameters
        ----------
        resultRequests : list
            Lista da controllare

        Returns
        -------
        bool
            True se resultRequests è una lista di ResultType
        """
        for resultRequest in resultRequests:
            if not isinstance(resultRequest, ResultType):
                if not isinstance(resultRequest, str):
                    return False
                if resultRequest.upper() not in ResultType.__members__:
                    return False
                resultRequests[
                    resultRequests.index(resultRequest)
                ] = ResultType(resultRequest)
        return True

    def isResultOnly(self):
        """
        Metodo che restituisce vero se il `Parameter` ha solo elementi per `resultType`

        :output bool: True se self.resulType != [] or None and self.values == [] or None, False altrimenti :type `bool`
        """
        if self.__value == None or self.__value == []:
            if self.__resultRequest != None and self.__resultRequest != []:
                return True
        return False

    @property
    def paramName(self):
        return self.__name

    @property
    def parameterName(self):
        return self.__name

    @property
    def resultRequest(self):
        return self.__resultRequest

    @property
    def value(self) -> List[ParameterValue]:
        return self.__value

    def toXML(self):
        """
        Funzione per la serializzazione. Data la necessità dei group parameter di serializzare i `Parameter`, senza dover aggiungere un attributo di classe non definito nelle specifiche, preferisco far passare a ciascuno dei parametri, il proprio nome
        """
        param = etree.Element(BPSIM + self.__name, nsmap={**BPSIM_NSMAP})

        # resultRequeste è una sequence min=0, max=unbound
        if self.__resultRequest:
            for resultType in self.__resultRequest:
                resultRequest = etree.Element(
                    BPSIM + "ResultRequest", nsmap={**BPSIM_NSMAP}
                )
                resultRequest.text = resultType.value
                param.append(resultRequest)

        # ci assicuriamo già al livello di istanziazione di avere almeno un valore per il parametro, poiché, anche se lo standard da card [0,unbounded], non ha senso istanziare parametri senza valori
        if self.__value:
            for paramValue in self.__value:
                value = paramValue.toXML()
                param.append(value)

        return param


class PropertyType(Enum):
    STRING = "string"
    BOOLEAN = "boolean"
    LONG = "long"
    DOUBLE = "double"
    DURATION = "duration"
    DATETIME = "dateTime"


class Property(Parameter):
    """
    Classe che realizza la classe property di BPSim.

    Parameters
    ----------
    propertyName : string
        Nome della proprietà OBBLIGATORIO
    value : `ParameterValue`
        Come per `Parameter`, rappresenta il valore del parametro in questione espresso in termini di BPSim:ParameterValue
    resultRequest : `ResultType`
        Come per `Parameter`, di solo input, utilizzato per determinare quale valore ci si aspetta in output
    type : `PropertyType`
        Tipo della proprietà. Opzionale, può essere dedotto dal valore
    """

    def __init__(
        self, propertyName: str, value, propertyType: PropertyType = None
    ):
        if propertyType != None:
            propertyType = PropertyType(propertyType)

        super().__init__(parameterName="Property", value=value)
        self.__propertyName = propertyName
        self.__type = propertyType

    def __eq__(self, other: "Property") -> bool:
        return (
            super().__eq__(other)
            and self.__propertyName == other.__propertyName
        )

    def __hash__(self):
        return hash((super().__hash__(), self.propertyName, self.propertyType))

    @property
    def propertyName(self):
        return self.__propertyName

    @property
    def propertyType(self) -> PropertyType | None:
        return self.__type

    def toXML(self):
        """
        Metodo per la serializzazione in XML (lxml)

        Returns
        -------
        etree._Element
            Serializzazione del corrente oggetto property in elemento XML (lxml)
        """
        propertyXML = super().toXML()
        propertyXML.set("name", self.__propertyName)
        if self.__type:
            propertyXML.set("type", self.__type.value)
        return propertyXML

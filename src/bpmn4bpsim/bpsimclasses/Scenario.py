from bson import ObjectId
from collections import Counter
from datetime import datetime
from isodate import datetime_isoformat
from lxml import etree
from typing import List
from bpmn4bpsim.bpsimclasses import BPSIM, BPSIM_NSMAP
from bpmn4bpsim.bpsimclasses.Calendar import Calendar
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters
from bpmn4bpsim.bpsimclasses.VendorExtension import VendorExtension


class Scenario(object):
    """
    Classe che realizza la classe Scenario (gli attributi della classe),
    definita in BPSim, che verrà utilizzata nella creazione e gestione degli
    scenari, nell'ambito del servizio BPSimaaS.

    Fra le altre cose, una sescrizione dei parametri come oggetto JSON
    serialazible (per python json), che può essere restituita in caso di errore
    quando si prova ad istanziare un nuovo scenario.

    Parameters
    ----------

    name: str
        pass

    _id: str
        pass

    description: str
        pass
    author: str
        pass
    vendor: str
        pass
    version: str
        pass
    inherits: str
        pass
    created: datetime
        pass
    modified: datetime
        pass
    scenarioParameters: ScenarioParameters
        pass
    elementParameters: List[ElementParameters]
        pass
    calendars: List[Calendar]
        pass
    vendorExtension: List[VendorExtension
        pass
    """

    SEQ_ORDER = ("ScenarioParameters", "ElementParameters", "Calendar")

    def __init__(
        self,
        name: str,
        _id: str = "",
        description: str = None,
        author: str = None,
        vendor: str = None,
        version: str = None,
        inherits: str = None,
        created: datetime = None,
        modified: datetime = None,
        scenarioParameters: ScenarioParameters = None,
        elementParameters: List[ElementParameters] = None,
        calendars: List[Calendar] = None,
        vendorExtension: List[VendorExtension] = None,
    ):
        ##########################
        # Assegnazione dei valori
        ##########################
        self._id = _id if len(_id) > 0 else str(ObjectId())
        self.name = name
        self.description = description
        self.author = author
        self.vendor = vendor
        self.version = version
        self.inherits = inherits
        self.created = created
        self.modified = modified
        self.__attributes = {
            "_id": _id if len(_id) > 0 else str(ObjectId()),
            "name": name,
            "description": description,
            "author": author,
            "vendor": vendor,
            "version": version,
            "inherits": inherits,
            "created": created or datetime.utcnow(),
            "modified": modified or datetime.utcnow(),
        }
        self.__elementParameters = elementParameters or []
        self.__calendars: List[Calendar] = (
            calendars if calendars is not None else []
        )
        self.__scenarioParameter = scenarioParameters
        self.__vendorExtensions = (
            vendorExtension if vendorExtension is not None else []
        )

    def __eq__(self, other: object) -> bool:
        if other is None:
            return False
        return (
            isinstance(other, Scenario)
            and self.__attributes == other.__attributes
            and self.__scenarioParameter == other.__scenarioParameter
            and Counter(self.__elementParameters)
            == Counter(other.__elementParameters)
            and Counter(self.__calendars) == Counter(other.__calendars)
            and Counter(self.__vendorExtensions)
            == Counter(other.__vendorExtensions)
        )

    def __hash__(self):
        return hash(
            (
                tuple(self.__attributes.values()),
                self.__scenarioParameter,
                tuple()
                if not self.__elementParameters
                else tuple(self.__elementParameters),
                tuple() if not self.calendars else tuple(self.__calendars),
                tuple()
                if not self.__vendorExtensions
                else tuple(self.__vendorExtensions),
            )
        )

    ####################
    # Setters e Getters
    ####################

    @property
    def elementParameters(self) -> List[ElementParameters] | None:
        return self.__elementParameters

    def elementParameter(self, parameterId: str) -> ElementParameters | None:
        """
        Proprietà che restituisce uno specifico ElementParameters

        Parameters
        ----------
        parameterId : str
            Attributo id dell'oggetto ElementParameters da restituire

        Returns
        -------
        ElementParameters
            Oggetto ElementParameters con id = parameterId

        Raises
        ------
        TypeError
            Se parameterId non è di tipo str
        """
        if self.__elementParameters is None:
            return None
        elementParameters = [
            x
            for x in self.__elementParameters
            if x.parametersId == parameterId
        ]
        if len(elementParameters) > 0:
            return elementParameters[0]
        return None

    @property
    def scenarioParameters(self) -> ScenarioParameters | None:
        """
        Getter per lo ScenarioParameters dello scenario

        Returns
        -------
        ScenarioParameters
            Scenario parameter dello scenario. None se non presente
        """
        return self.__scenarioParameter

    @scenarioParameters.setter
    def scenarioParameters(self, scenarioParameters: ScenarioParameters):
        """
        Metodo per settare l'oggetto scenario parameter del corrente scenario

        Parameters
        ----------
        scenarioParameters : ScenarioParameters
            Oggetto di tipo ScenarioParameters da impostare per questo scenario

        Raises
        ------
        TypeError
            Se l'oggetto passato non è di tipo ScenarioParameters
        """
        if not isinstance(scenarioParameters, ScenarioParameters):
            raise TypeError(
                f"Atteso un oggetto di tipo ScenarioParameters. Ricevuto: \
                    {scenarioParameters.__class__.__name__}"
            )
        self.__scenarioParameter = scenarioParameters

    @property
    def calendars(self) -> List[Calendar]:
        return self.__calendars

    @property
    def vendorExtensions(self) -> List[VendorExtension]:
        return self.__vendorExtensions

    def addElementParameter(self, elementParameter: ElementParameters):
        """
        Metodo utilizzato per aggiungere un oggetto di tipo ElementParameters
        alla lista degli ElementParameters dello scenario corrente

        Parameters
        ----------
        elementParameter : ElementParameters
            Oggetto ElementParameters da aggiungere alla lista
            self.__elementParameters

        Raises
        ------
        TypeError
            Se viene passato un oggetto che non è del tipo ElementParameters
        ValueError
            Se viene passato un oggetto di tipo ElementParameters con un id che
            appartiene ad uno degli ElementParameters già presenti nello
            scenario
        """
        if not isinstance(elementParameter, ElementParameters):
            raise TypeError(
                f"Atteso un oggetto di tipo ElementParameters. Ricevuto: \
                    {elementParameter.__class__.__name__}"
            )
        if self.__elementParameters is None:
            self.__elementParameters = []
        if any(
            ep.parametersId == elementParameter.parametersId
            for ep in self.__elementParameters
        ):
            raise ValueError(
                f"Esiste già un oggetto ElementParameters con id \
                    {elementParameter.parametersId}"
            )
        self.__elementParameters.append(elementParameter)

    def removeElementparameter(self, elem_paramemeters_id: str):
        """
        Metodo che permette di cancellare un ElementParameters dello scenario
        corrente

        Parameters
        ----------
        elem_paramemeters_id : str
            Id dell'element parameter da rimuovere

        Raises
        ------
        ValueError
            Se non è presente alcun ElementParameter con id
            elem_paramemeters_id nello scenario corrente
        """
        elem_parameters = self.elementParameter(elem_paramemeters_id)
        if not elem_parameters:
            raise ValueError(
                f"Non esiste ElementParameters con id {elem_paramemeters_id}"
            )
        self.__elementParameters.remove(elem_parameters)

    def updateElementParameter(self, element_parameters: ElementParameters):
        """
        Metodo che permette di aggiornare un ElementParameter

        Parameters
        ----------
        element_parameters : ElementParameters
            ElementParameter da sostituire a quello con lo stesso id
        """
        # remove then add
        self.removeElementparameter(element_parameters.parametersId)
        self.addElementParameter(element_parameters)

    def addCalendar(self, calendar: Calendar):
        """
        Metodo che permette di aggiungere un calendar allo scenario

        Parameters
        ----------
        calendar: `Calendar`
            Calendar da aggiungere

        Raises
        ------
        TypeError
            Se calendar non è di tipo `Calendar`
        ValueError
            Se è già presente un Calendar con lo stesso id di calendar nello
            scenario
        """

        if not isinstance(calendar, Calendar):
            raise TypeError(
                "Atteso un oggetto di tipo Calendar, ricevuto : "
                f"{calendar.__class__.__name__}"
            )
        if self.__calendars is None:
            self.__calendars = []
        if any(calendar.id == c.id for c in self.__calendars):
            raise ValueError(
                f"Un calendar con id {calendar.id} è già presente per questo "
                "scenario."
            )
        self.__calendars.append(calendar)

    def deleteCalendar(self, idCalendar: str):
        """Metodo per cancellare un calendar dallo scenario

        Parameters
        ----------
        idCalendar: `str`
            Calendar da aggiungere allo scenario

        Raises
        ------
        `ValueError`
            Se non esiste alcun calendar nello scenario con id idCalendar
        """

        calendars = [c for c in self.__calendars if c.id == idCalendar]
        if not calendars:
            raise ValueError(f"Nessun calendar con id {idCalendar} trovato")
        self.__calendars.remove(calendars[0])

    def updateCalendar(self, calendar: Calendar, idCalendar: str = None):
        """Metodo per aggiornare un calendar dello scenario.

        Parameters
        ----------
        calendar: `Calendar`
            Calendar usato per aggiornare il calendar dello scenario
        idCalendar: `str`
            Se None -> idCalendar = calendar.id, id del calendar da aggiornare
        """

        if not isinstance(calendar, Calendar):
            raise TypeError(
                "Atteso oggetto di tipo calendar, ricevuto: "
                f"{calendar.__class__.__name__}"
            )
        idCalendar = idCalendar if idCalendar else calendar.id
        self.deleteCalendar(idCalendar)
        self.addCalendar(calendar)

    ########################################
    # Metodi per la de-/serializzazione
    ########################################

    # TODO
    @classmethod
    def fromXML(cls, scenarioXML):
        # controlla se è stringa o oggetto lxml
        # deserializza e crea un oggetto di tipo scenario, che restituisce
        # return cls(....)
        pass

    def toXML(self):
        """
        Funzione per la serializzazione dello scenario utilizzando lxml, con
        namespace bpsim
        """
        scenarioXML = etree.Element(
            BPSIM + "Scenario", nsmap={**BPSIM_NSMAP}, id=self._id
        )
        for attributeName, attributeValue in self.__attributes.items():
            if attributeValue is not None:
                if (
                    attributeName.lower() == "created"
                    or attributeName.lower() == "modified"
                ):
                    attributeValue = datetime_isoformat(attributeValue)[:19]
                if attributeName != "_id":
                    scenarioXML.set(attributeName, attributeValue)
        for childName in self.SEQ_ORDER:
            if childName == "ElementParameters":
                for elemParameter in self.__elementParameters:
                    scenarioXML.append(elemParameter.toXML())
            elif childName == "Calendar" and self.__calendars:
                for calendar in self.__calendars:
                    scenarioXML.append(calendar.toXML())
            elif (
                childName == "ScenarioParameters" and self.__scenarioParameter
            ):
                scenarioXML.append(self.__scenarioParameter.toXML())
        return scenarioXML

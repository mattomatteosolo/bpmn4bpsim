from collections import Counter
from datetime import timedelta
from iso4217 import Currency
from isodate import  duration_isoformat
from lxml import etree
from bpmn4bpsim.bpsimclasses import BPSIM_NSMAP, BPSIM
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterGroups.PropertyParameters import PropertyParameters


class ScenarioParameters(object):
    '''
    Classe che realizza la classe BPSim ScenarioParameters

    Parameters
    ----------
    start: Parameter
        pass

    duration: Parameter
        pass

    warmup: Parameter
        pass

    propertyParameters: PropertyParameters
        pass

    seed: int
        pass

    replication: int
        pass

    baseTimeUnit: str
        pass

    baseCurrencyUnit: str
        pass

    baseResultFrequency: timedelta
        pass

    baseResultFrequencyCumul: bool
        pass

    traceOutput: bool
        pass

    traceFormat: str
        pass
    '''
    SEQ_ORDER = ('Start', 'Duration', 'Warmup', 'PropertyParameters')
    # Assumiamo per ora che XES isa l'unica opzione per il campo traceFormat e quindi traceOutput sempre False

    def __init__(
        self,
        start: Parameter = None,
        duration: Parameter = None,
        warmup: Parameter = None,
        propertyParameters: PropertyParameters = None,
        seed: int = None,
        replication: int = None,
        baseTimeUnit: TimeUnit = None,
        baseCurrencyUnit: Currency = None,
        baseResultFrequency: timedelta = None,
        baseResultFrequencyCumul: bool = None,
        traceOutput: bool = None,
        traceFormat: str = None,
    ):
        ####################
        # Value Checking
        ####################
        if seed is not None and seed < 0:
            raise ValueError()
        if replication is not None and replication < 1:
            raise ValueError()
        ###############
        # Assegnazione
        ###############
        self.__attributes = {
            'seed': seed,
            'replication': replication,
            'baseTimeUnit': baseTimeUnit,
            'baseCurrencyUnit': baseCurrencyUnit,
            'baseResultFrequency': baseResultFrequency,
            'baseResultFrequencyCumul': baseResultFrequencyCumul,
            'traceOutput': traceOutput,
            'traceFormat': traceFormat
        }
        self.__sequenceParameters = {
            'start': start,
            'duration': duration,
            'warmup': warmup,
            'propertyParameters': propertyParameters
        }

    def __eq__(self, other: 'ScenarioParameters') -> bool:
        return Counter(self.__attributes) == Counter(other.__attributes) \
            and Counter(self.__sequenceParameters) == Counter(other.__sequenceParameters)

    def __hash__(self):
        return hash(
            (
                tuple(self.__attributes.values()),
                tuple(self.__sequenceParameters.values())
            )
        )
    ###############
    # Properties
    ###############
    @property
    def start(self) -> Parameter:
        return self.__sequenceParameters.get('start')

    @property
    def duration(self) -> Parameter:
        return self.__sequenceParameters.get('duration')

    @property
    def warmup(self) -> Parameter:
        return self.__sequenceParameters.get('warmup')

    @property
    def propertyParameters(self) -> PropertyParameters:
        return self.__sequenceParameters.get('propertyParameters')

    @property
    def seed(self) -> int:
        return self.__attributes.get('seed')

    @property
    def replication(self) -> int:
        return self.__attributes.get('replication')

    @property
    def baseTimeUnit(self) -> str:
        return self.__attributes.get('baseTimeUnit')

    @property
    def baseCurrencyUnit(self) -> str:
        return self.__attributes.get('baseCurrencyUnit')

    @property
    def baseResultFrequency(self) -> timedelta:
        return self.__attributes.get('baseResultFrequency')

    @property
    def baseResultFrequencyCumul(self) -> bool:
        return self.__attributes.get('baseResultFrequencyCumul')

    @property
    def traceOutput(self) -> bool:
        return self.__attributes.get('traceOutput')

    @property
    def traceFormat(self) -> str:
        return self.__attributes.get('traceFormat')

    def toXML(self):
        '''
        Metodo per la serializzazione XML (lxml)

        Returns
        -------
        etree._Element
            Oggetto lxml.etree._Element serializzazion XML dell'oggetto corrente
        '''
        scenarioParameters = etree.Element(
            BPSIM + 'ScenarioParameters', nsmap=BPSIM_NSMAP)
        for attributeName, attributeValue in self.__attributes.items():
            if attributeValue is not None:
                if attributeName == 'baseTimeUnit' \
                        or attributeName == 'baseCurrencyUnit':
                    attributeValue = attributeValue.value
                elif attributeName == 'baseResultFrequency':
                    attributeValue = duration_isoformat(attributeValue)
                elif attributeName == 'baseResultFrequencyCumul' \
                        or attributeName == 'traceOutput':
                    attributeValue = str(attributeValue).lower()
                scenarioParameters.set(attributeName, str(attributeValue))
        for itemName in {item.lower() for item in self.SEQ_ORDER}:
            if itemName.lower() in self.__sequenceParameters:
                if self.__sequenceParameters[itemName] is not None:
                    scenarioParameters.append(
                        self.__sequenceParameters[itemName].toXML())
        return scenarioParameters

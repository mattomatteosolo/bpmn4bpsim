from lxml import etree
from bpmn4bpsim.bpsimclasses import BPSIM_NSMAP, BPSIM


class VendorExtension(object):
    '''
    Classe che mappa VendorExtension di BPSim.

    Trattandosi di elementi di cui non conosciamo a priori la definizione, possiamo soltanti limitarci all'inserire questi elementi nel documento finale

    Parameters
    ----------
    name : str
        Nome dell'oggetto
    value : etree._Element
        Contenuto dell'oggetto (inteso come in serializzazione XML)
    attributi : dict
        Serie di attributi opzionali (come definito nello schema)
    '''

    def __init__(self, name: str, value: etree._Element = None):
        if not isinstance(name, str):
            raise TypeError(
                f'Atteso un oggetto di tipo str per name. Ricevuto: {name.__class__.__name__}')
        self.__name = name
        if value != None and not etree.iselement(value):
            if not isinstance(value, str):
                raise TypeError(
                    f'Atteso un oggetto XML per value. Ricevuto: {value.__class__.__name__}')
            value = etree.parse(value).getroot()
        self.__value = value

    def toXML(self) -> etree._Element:
        '''
        Metodo per la serializzazione in XML (LXML)

        Returns
        -------
        etree._Element
            Oggetto XML (lxml) ottenuto deserializzando il corrente VendorExtension
        '''
        vendorExtension = etree.Element(
            BPSIM + 'VendorExtension', nsmap=BPSIM_NSMAP, name=self.__name)
        if self.__value != None:
            vendorExtension.append(self.__value)
        return vendorExtension

from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpmnfacilities.bpmn_types import (
    isActivity,
    isResource,
    isResourceRole,
    isProcess,
)
from bpmn4bpsim.bpmnfacilities.bpmn_conditional_types import isDecomposable
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroups,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    DurationParameter,
    FloatingParameter,
)


class TimeParameters(ParameterGroups):
    """
    Classe "scaffale" per i parametri legati al tempo per un elemento di un processo di business. Nella nostra implementazione, ci aspettiamo un parametro *args costituito da una serie di coppie ('nomeparam',`Parameter`) come input, oppure una ed una sola lista, che provvederemo a 'spacchettare' dallo *args

    Per maggiori info sulla descrizione si rimanda alle specifiche di BPSim. Tutti i parametri che seguono hanno cardinalità 0...1, ma per essere serializzao, almeno uno dei seguenti parametri deve essere istanziato correttamente.

    Parameters
    ----------
    bpmElement : `etree._Element`
        Elemento BPMN a cui applicare TimeParameters
    params : `list`
        Lista di tuple(str,Parameter), il cui elemento 0 è una stringa contenente il nome del TimeParameter da applicare, mentre, l'elemento 1 è il Parameter vero e proprio. La lista dei nomi di parametri possibile a seguire

    Params:
    -------
    TransferTime
        Tempo impiegato a passare dal precedente al segente passo di simulazione
    QueueTime
        Ritardo dalla "presentazione" del nuovo elemento al suo allocamento in simulazione
    WaitTime
        Tempo dall'allocamento del nuovo elemento di simulazione ed il suo effettivo inizio
    LagTime
        timedelta dal completamento del precedente elemento (i-esimo) in simulazione e l'inizio del successivo (i+1-esimo), utilizzabile solo come `ResultRequest` (senza value), definito come `TransferTime + QueueTime + WaitTime`
    SetupTime
        Tempo di pre-processing, speso prima di effettuare il vero lavoro
    ProcessingTime
        Tempo effettivamento speso nella simulazione dell'elemento
    ValidationTime
        Tempo speso in verifche ed ispezioni del lavoro fatto
    ReworkTime
        Tempo speso nella correzione o nel rifacimento del lavoro
    DurationTime
        Tempo totale speso nella simulazione, può essere usato solo come ResultRequests ed è definito come `SetupTime + ProcessingTime + ValidationTime + ReworkTime`
    ElapsedTime
        Tempo passato dal completamento del precedente lavoro al completmento del lavoro corrente, definito come `LagTime + ElapsedTime`
    """

    #################################################################
    # Costanti utilizzate nei vari metodi
    # Si tratta di elementi specifici per ciascun ParametersGroup
    #################################################################
    SEQ_ORDER = (
        "TransferTime",
        "QueueTime",
        "WaitTime",
        "SetUpTime",
        "ProcessingTime",
        "ValidationTime",
        "ReworkTime",
        "LagTime",
        "Duration",
        "ElapsedTime",
    )
    __IN_PARAM = (
        "TransferTime",
        "QueueTime",
        "WaitTime",
        "SetUpTime",
        "ProcessingTime",
        "ValidationTime",
        "ReworkTime",
    )
    __OUT_PARAM = ("LagTime", "Duration", "ElapsedTime")
    RESOLVES = {
        key: [
            NumericParameter,
            FloatingParameter,
            DurationParameter,
            ExpressionParameter,
        ]
        for key in __IN_PARAM
    }
    # TODO, supporto per DistribuitonParameters
    # RESOLVES.extend([getattr(DistributionParameter, distribution) for distribution in dir(DistributionParameter)
    #                  if 'Distribution' in distribution and 'Parameter' not in distribution and 'User' not in distribution])

    def __init__(self, bpmElement: etree._Element, parameters: list):
        super().__init__(bpmElement, parameters)

    #################################################################
    # Metodi che controllano la validità dei parametri
    # e la loro applicabilità rispetto a determinati elementi BPMN
    #################################################################

    @classmethod
    def isApplicable(cls, bpmElement: etree._Element, parameter: Parameter):
        """
        Metodo che prende in input un elemento BPMN come elemento lxml o come serializzazione XML, ed esegue su di esso un controllo di applicabilità del parametro passato.

        Questa funzione non prende altri parametri perché tutti i parametri hanno lo stesso campo di applicazione.

        Parameters
        ----------
        bpmElement: `etree._Element`
            Elemento bpm al quale si intende applicare il gruppo di parametri :type `etree._Element` o `string`

        param: `Parameter`
            Parametro che si vuole aggiungere a bpmElement

        Returns
        -------
        bool: `bool`
            Vero se il gruppo di parametri è applicabile all'elemento passato, falso altrimenti
        """
        ####################
        # Type Checking
        ####################
        if not etree.iselement(bpmElement):
            raise TypeError(
                "Atteso un elemento BPMN in formato xml (lxml). Ricevuto: "
                + str(type(bpmElement))
            )
        ##############################
        # Controllo dei valori di I/O
        ##############################
        if not cls.parameterHasValidValues(parameter):
            return False
        ############################################################
        # Controllo dell'applicabilità rispetto ad un elemento BPM
        ############################################################
        if (
            isResource(bpmElement)
            or isResourceRole(bpmElement)
            or parameter.paramName in cls.__OUT_PARAM
        ):
            if parameter.isResultOnly():
                return True
            else:
                return False
        if isActivity(bpmElement):
            if parameter.isResultOnly():
                return True
            if not isDecomposable(bpmElement):
                return True
        if isProcess(bpmElement) and parameter.isResultOnly():
            return True

        return False

    @classmethod
    def isValidResult(cls, parameter: Parameter):
        if not cls.checkParameter(parameter):
            raise ParamNonValido()
        return True

    # @classmethod
    # def isValidDistribution(cls, parameterName: str, distributionParameter: DistributionParameter):
    #     pass

    ####################
    # Setters e Getters
    ####################

    # def setParam(self):
    #     pass

    # def getParam(self):
    #     pass

    # def updateParam(self):
    #     pass

    # def deleteParam(self):
    #     pass

    ###################################
    # Metodi per la de-/serializzazione
    ###################################

    def toXML(self):
        """
        Metodo per la serializzazione in XML
        """
        return super().toXML(self.parameters)

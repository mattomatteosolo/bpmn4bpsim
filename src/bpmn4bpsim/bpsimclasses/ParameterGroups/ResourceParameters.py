from lxml import etree
from typing import List, Mapping
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpmnfacilities.bpmn_types import isResource, isResourceRole
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import ParameterGroups
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import BooleanParameter, NumericParameter, StringParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import ExpressionParameter


class ResourceParameters(ParameterGroups):
    '''
    Classe wrapper per i parametri legati alle risorse BPM

    Parameters
    ----------
    Availability : Parameter
        Determina quando una risorsa è disponibile. Spesso associato ad un Calendar per determinare quando la risorsa è effettivamente disponibile. Risolve ad un BooleanParameter. Il default è True, la risorsa è disponibile sempre
    Quantity : Parameter
        Quantità della stessa risorsa, deve risolvere in un NumeriParameter
    Selection
        Criterio per scegliere la risorsa, si tratta di una sovrascrittura del comportamento di un elemento BPMN ResourceRole
    Role : list
        I ruoli, eventualemente più di uno, che può assumere una risorsa, possono essere riusati da ResourceParameters.Selection. Risolve in StringParameter
    '''
    #################################################################
    # Costanti utilizzate nei vari metodi
    # Si tratta di elementi specifici per ciascun ParametersGroup
    #################################################################
    SEQ_ORDER = ('Selection', 'Availability', 'Quantity', 'Role')
    RESOLVES = {
        'Availability': [BooleanParameter, ExpressionParameter],
        'Quantity': [NumericParameter, ExpressionParameter],
        'Selection': [StringParameter, NumericParameter, ExpressionParameter],
        'Role': [StringParameter, ExpressionParameter]
    }

    def __init__(self, bpmElement: etree._Element, parameters: List[Parameter]):
        if len(parameters) < 1:
            raise ValueError()

        self.__parameters: Mapping[str, Parameter | List[Parameter]] = dict()
        self.__parameters['Role'] = list()
        for parameter in parameters:
            if not self.isApplicable(bpmElement, parameter):
                raise ParamNonValido()

            if parameter.paramName == 'Role':
                self.__parameters['Role'].append(parameter)
            else:
                self.__parameters[parameter.paramName] =  parameter

    #################################################################
    # Metodi che controllano la validità dei parametri
    # e la loro applicabilità rispetto a determinati elementi BPMN
    #################################################################

    @classmethod
    def isApplicable(cls, bpmElement: etree._Element, parameter: Parameter):
        '''
        Metodo per controllare se il parametro passato è applicabile alll'elemento BPM passato.

        Parameters
        ----------
        bpmElement : etree._Element oppure string
            Elemento del modello sul quale controllare l'applicabilità
        parameter : string
            Nome del parametro da applicare a bpmElement

        Returns
        -------
        bool
            True se parameter è un parametro valido per il gruppo e può essere applicato all'elemento BPM bpmElement. False altrimenti
        '''
        if not etree.iselement(bpmElement):
            raise TypeError()
        ##############################
        # Controllo dei valori di I/O
        ##############################
        if not cls.isValidResult(parameter):
            return False
        if not cls.parameterHasValidValues(parameter):
            return False
        ##############################
        # Controllo di applicabilità
        ##############################
        if isResourceRole(bpmElement):
            if parameter.paramName == 'Selection':
                return True
            pass
        elif isResource(bpmElement):
            if parameter.paramName in {'Availability', 'Quantity', 'Role'}:
                return True
        return False

    @classmethod
    def isValidResult(cls, parameter: Parameter):
        if not cls.checkParameter(parameter):
            raise ParamNonValido()
        if not parameter.resultRequest:
            return True
        if parameter.paramName != 'Selection':
            return False
        for resultType in parameter.resultRequest:
            if resultType.value not in {'min', 'max'}:
                return False
        return True

    ####################
    # Setters e Getters
    ####################
    @property
    def parameters(self) -> List[Parameter]:
        roles: List[Parameter] = self.__parameters['Role']
        not_roles: List[Parameter] = [v for k, v in self.__parameters.items() if k.lower() != 'role']
        return not_roles + roles

    ###################################
    # Metodi per la de-/serializzazione
    ###################################

    def toXML(self):
        return super().toXML(self.parameters)

from abc import ABC, abstractmethod
from collections import Counter
from typing import Mapping, List
from lxml import etree
from enum import Enum
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.EnumParameter import EnumParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.DistributionParameter import DistributionParameter
from bpmn4bpsim.bpsimclasses import ParamNonValido, BPSIM, BPSIM_NSMAP


class ParameterGroupsName(Enum):
    TimeParameters = "TimeParameters"
    ControlParameters = "ControlParameters"
    ResourceParameters = "ResourceParameters"
    CostParameters = "CostParameters"
    PropertyParameters = "PropertyParameters"
    PriorityParameters = "PriorityParameters"


class ParameterGroups(ABC):
    '''
    Classe scheletro per gli altri GroupParam

    self.__params = {
        <str: paramName>: <Parameter: parameter>
        ...
    }
    '''
    #################################################################
    # Costanti utilizzate nei vari metodi
    # Si tratta di elementi specifici per ciascun ParametersGroup,
    # pertanto i loro valori verrano modificati in ciascuna classe
    # che utilizza ParametersGroup come "Interfaccia"
    #################################################################
    SEQ_ORDER = ()
    RESOLVES = dict()

    def __init__(self, bpmElement: etree._Element, parameters: List[Parameter]):
        if len(parameters) < 1:
            raise ValueError('Attesa una lista di Parameter, NON vuota.')
        ##############################################
        # Inizializzazione dell'oggetto con controlli
        ##############################################
        self.__parameters: Mapping[str, Parameter] = dict()
        # se non, lancia Type o value error
        for parameter in parameters:
            #####################
            # Applicability check
            #####################
            if not self.isApplicable(bpmElement, parameter):
                raise ParamNonValido(
                    'Fornito un parametro con valori di input o output non applicabili all\'elemento BPM fornito.')

            self.__parameters.update({parameter.paramName: parameter})

    def __eq__(self, other: 'ParameterGroups') -> bool:
        return Counter(self.parameters) == Counter(other.parameters)

    def __hash__(self):
        return hash(tuple(self.parameters))

    #################################################################
    # Metodi che controllano la validità dei parametri
    # e la loro applicabilità rispetto a determinati elementi BPMN
    #################################################################

    @classmethod
    @abstractmethod
    def isApplicable(cls, bpmElement: etree._Element, parameter: Parameter):
        pass

    @classmethod
    @abstractmethod
    def isValidResult(cls, parameter: Parameter):
        pass

    @classmethod
    def parameterHasValidValues(cls, parameter: Parameter):
        '''
        Metodo che controlla se il parametro passato ha valori validi per il gruppo che lo lancia

        Parameters
        ----------
        parameter : Parameter
            Oggetto Parameter da controllare

        Returns
        -------
        bool
            True se parameter.value è vuota o None, o se tutti i gli oggetti che contiene sono ParameterValue di un tipo valido per RESOLVES[parameter.paramName]. False altrimenti

        Raises
        ------
        TypeError
            Se parameter non è di tipo Parameter
        ValueError
            Se parameter.paramName non è in RESOLVE.keys()
        '''
        if not cls.checkParameter(parameter):
            raise ParamNonValido()

        if not parameter.value:
            return True
        if parameter.paramName not in cls.RESOLVES.keys():
            return False
        for value in parameter.value:
            ##################################################
            # Il supporto per DistributionParameter va qui
            ##################################################
            if isinstance(value, EnumParameter):
                return cls.isValidEnum(parameter.paramName, value)
            resolveFound = False
            for resType in cls.RESOLVES[parameter.paramName]:
                if isinstance(value, resType):
                    # aggiungi e break
                    resolveFound = True
                    break
            if not resolveFound:
                return False
        return True

    @classmethod
    def isValidDistribution(cls, parameterName: str, distributionParameter: DistributionParameter):
        # Se ha dei valori per l'input, effettua determinati controlli
                # se il valore è un UserDistribution devo controllare che risolva a punti Numeric o Floating
                # if isinstance(value, DistributionParameter.UserDistribution):

                #     # controllo tutti i punti della UserDistribution
                #     for point in value.points:
                #         validPoint = False
                #         # se point non è istanza di classi in RESOLVES raise exception
                #         for resType in self.RESOLVES:
                #             if isinstance(point.value, resType):
                #                 validPoint = True
                #         if not validPoint:
                #             raise ParamNonValido(
                #                 'I punti della UserDistribution devono essere Numeric o Floating Parameter per i TimeParameters')
                #         # istanzia la UserDistribution
                #         # self.__params.update({param[0]:param[1]})
        pass

    @classmethod
    def isValidEnum(cls, parameterName: str, enumParameter: EnumParameter):
        '''
        Metodo che controlla che restituisce Vero se enumParameter ha solo valori ammissibili per parameterName

        Parameters
        ----------
        parameterName : `str`
            Nome del parametro di cui controllare i ParameterValue ammissibili
        enumParameter : `EnumParameter`
            EnumParameter di cui controllare i values

        Raises
        ------
        TypeError
            sdad
        ValueError
            sdasds
        '''
        if not isinstance(enumParameter, EnumParameter):
            raise TypeError(
                'Atteso un EnumParameter per enumParameter. Ricevuto: ' + str(type(enumParameter)))
        if not isinstance(parameterName, str):
            raise TypeError(
                'Atteso un oggetto str per parameterName. Ricevuto: ' + str(type(parameterName)))
        if parameterName not in cls.RESOLVES.keys():
            raise ValueError(
                'parameterName non ha un nome valido per il gruppo chiamante')

        for paramValue in enumParameter.values:
            typeFounded = False
            for resolveType in cls.RESOLVES[parameterName]:
                if isinstance(paramValue, resolveType):
                    typeFounded = True
                    break
            if not typeFounded:
                return False

        return True

    @classmethod
    def checkParameter(cls, parameter: Parameter):
        '''
        Metodo che controlla se l'oggetto passato è di tipo Parameter e ha un nome valido per il gruppo che lo invoca

        Parameters
        ----------
        parameter : Parameter
            Oggetto da controllare

        Returns
        -------
        bool
            True se parameter è di tipo Parameter e parameter.paramName è in RESOLVE.keys()
        '''
        if not isinstance(parameter, Parameter):
            return False
        if parameter.paramName not in cls.SEQ_ORDER:
            return False
        return True

    ####################
    # Setters e Getters
    ####################

    @property
    def parameters(self) -> list:
        return [parameter for parameter in self.__parameters.values()]

    def parameter(self, parameterName: str) -> Parameter | None:
        '''
        Metodo che restituisce un Parametera ppartenente al corrente gruppo di parametri, dato il suo nome

        Se il nome passato non è valido, lancia un errore. Se, invece, il parametro non è trovato, restituisce None

        Parameters
        ----------
        parameterName : str
            Nome del parametro da restituire

        Returns
        -------
        Parameter
            Oggetto Parameter il cui nome è parameterName

        Raises
        ------
        TypeError
            Se parameterName non è una str
        ValueError
            Se il nome non è quello di un parametro del gruppo corrente
        '''
        if not isinstance(parameterName, str):
            raise ValueError(
                f'Atteso un oggetto di tipo str per parameterName. Ricevuto: {parameterName.__class__.__name__}')
        if parameterName not in self.SEQ_ORDER:
            raise ValueError(
                f'{parameterName} non è un nome di parametro valido per il gruppo {self.__class__.__name__}')
        return self.__parameters.get(parameterName)

    # @abstractmethod
    # def setParam(self, jsonSpec):
    #     # come per fromJson, devo fare controlli sul tipo
    #     pass

    # @abstractmethod
    # def updateParam(self, jsonSpec):
    #     pass

    # @abstractmethod
    # def deleteParam(self, jsonSpec):
    #     pass

    # @abstractmethod
    # def getParam(self, paramName):
    #     pass

    ########################################
    # Metodi per la de-/serializzazione
    ########################################

    def toXML(self, params: list):
        '''
        Metodo per la serializzazione in XML

        Parameters
        ----------
        params : `list`
            Dizionario dei parametri del gruppo specifico
        '''
        if not isinstance(params, list):
            raise TypeError()
        className = self.__class__.__name__
        groupParam = etree.Element(BPSIM + className, nsmap=BPSIM_NSMAP)
        for parameter_name in self.SEQ_ORDER:
            parameters = [
                x for x in params if x.parameterName == parameter_name]
            if len(parameters) == 1:
                paramXML = parameters[0].toXML()
                groupParam.append(paramXML)
            elif len(parameters) > 1:
                for parameter in parameters:
                    paramXML = parameter.toXML()
                    groupParam.append(paramXML)
        return groupParam

from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import ParameterGroups
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import ExpressionParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import BooleanParameter, NumericParameter, FloatingParameter
from bpmn4bpsim.bpmnfacilities.bpmn_conditional_types import isDecomposable
from bpmn4bpsim.bpmnfacilities.bpmn_types import isTask, isSubProcess, isTransaction


class PriorityParameters(ParameterGroups):
    '''
    Classe che raggruppa i parametri che specificano le "priorità" di un elemento del processo di business

    Parameters
    ----------
    bpmElement : etree._Element
        Elemento a cui applicare questo gruppo di parametri
    parameters : list
        Una lista di tuple (str, Parameter), in cui il primo elemento è il nome del parametro, mentre il secondo è il parametro vero e proprio. Di seguito una lista di possibili nomi di parametri per questo gruppo

    Param names
    -----------
    Interruptible : Parameter
        Determina se l'esecuzione dell'elemento a cui è associato, è interrompibile oppure no. Deve risolvere ad un BooleanParameter ed il default è False (l'elemento non può essere interrotto)
    Priority : Parameter
        Definisce la priorità di un elemento del modello ed è usato per influenzare l'ordine con il quale risorse di cui se ne possiedono poche sono allocate. Più alto il valore di questo parametro, più alta la possibilità che la risorsa venga allocata per questo elemento. Deve risolvere ad un NumericParameter o un FloatingParameter
    '''
    SEQ_ORDER = ('Interruptible', 'Priority')
    # TODO supporto per distribution parameters
    RESOLVES = {
        'Interruptible': [BooleanParameter, ExpressionParameter],
        'Priority': [NumericParameter, FloatingParameter, ExpressionParameter]
    }
    # RESOLVES['Priority'].extend([getattr(DistributionParameter,distr) for distr in dir(DistributionParameter) if 'Parameter' not in distr and 'User' not in distr])

    def __init__(self, bpmElement: etree._Element, parameters: list):
        super().__init__(bpmElement, parameters)

    @classmethod
    def isApplicable(cls, bpmElement: etree._Element, parameter: Parameter):
        ####################
        # Type checking
        ####################
        if not etree.iselement(bpmElement):
            raise TypeError()
        ##############################
        # Controllo dei valori in I/O
        ##############################
        if not cls.isValidResult(parameter):
            return False
        if not cls.parameterHasValidValues(parameter):
            return False
        ############################################################
        # Controllo dell'applicabilità rispetto ad un elemento BPM
        ############################################################
        if isTask(bpmElement):
            return True
        if isSubProcess(bpmElement) or isTransaction(bpmElement):
            if not isDecomposable(bpmElement):
                return True
        return False

    @classmethod
    def isValidResult(cls, parameter: Parameter):
        if not cls.checkParameter(parameter):
            raise ParamNonValido()
        if not parameter.resultRequest:
            return True
        return False

    def toXML(self):
        '''
        Metodo per la serializzazione in XML

        Returns
        -------
        etree._Element
            Elemento XML con namespace BPSIM e contenente le informazioni necessarie per la simulazione
        '''
        return super().toXML(self.parameters)

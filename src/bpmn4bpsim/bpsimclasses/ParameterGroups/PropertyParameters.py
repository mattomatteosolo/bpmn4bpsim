from lxml import etree
from typing import List, Mapping
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpmnfacilities.bpmn_types import (
    isStartEvent,
    isIntermediateEvent,
    isEndEvent,
    isTask,
    isSequenceFlow,
    isMessageFlow,
    isActivity,
)
from bpmn4bpsim.bpmnfacilities.bpmn_conditional_types import (
    isBoundaryEvent,
    isDecomposable,
)
from bpmn4bpsim.bpsimclasses.Parameter import (
    Property,
    Parameter,
    ParameterValue,
)
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroups,
)


class PropertyParameters(ParameterGroups):
    """
    Classe che raggruppa i parametri che specificano proprietà di un elemento del modello

    Parameters
    ----------
    Property : `list`
        Una lista di Property, cioè proprietà addizionali assegnati ad un elemento BPMN. Queste proprietà possono essere accedute dagli ExpressionParameter, sono valutate e settate non appena un token di simulazione entra nell'elemento a cui si riferiscono. Possono risolversi in un qualsiasi tipo di parametro e non ha un valore di default
    QueueLength : `Parameter`
        Pseudo-Parametro utilizzabile in input soltanto per richiedere un ResultRequest.
    """

    SEQ_ORDER = ("Property", "QueueLength")
    RESOLVES = {"Property": [ParameterValue]}

    def __init__(
        self,
        bpmElement: etree._Element | None,
        parameters: list,
        scenario_parameters: bool = False,
    ):
        if not isinstance(parameters, list):
            raise TypeError()
        if not etree.iselement(bpmElement) and not scenario_parameters:
            if not isinstance(bpmElement, str):
                raise TypeError()
        if len(parameters) < 1:
            raise ValueError()

        self.__parameters: Mapping[str, Parameter | List[Parameter]] = dict()
        self.__parameters["Property"] = list()
        for parameter in parameters:
            if bpmElement != None and not self.isApplicable(
                bpmElement, parameter, scenario_parameters
            ):
                raise ParamNonValido()

            if parameter.paramName == "Property":
                if not isinstance(parameter, Property):
                    raise ParamNonValido()
                self.__parameters["Property"].append(parameter)
            else:
                self.__parameters.update({parameter.paramName: parameter})

    @classmethod
    def isApplicable(
        cls,
        bpmElement: etree._Element | None,
        parameter: Parameter,
        scenario_parameters=False,
    ):
        if bpmElement != None and not scenario_parameters:
            if not etree.iselement(bpmElement):
                if (
                    not isinstance(bpmElement, str)
                    and bpmElement != "ScenarioParameters"
                ):
                    raise TypeError()

            if not cls.isValidResult(parameter):
                return False
            if not cls.parameterHasValidValues(parameter):
                return False
            if bpmElement == "ScenarioParameters":
                return True

            if (
                isStartEvent(bpmElement)
                or isIntermediateEvent(bpmElement)
                or isEndEvent(bpmElement)
                or isBoundaryEvent(bpmElement)
                or isSequenceFlow(bpmElement)
                or isMessageFlow(bpmElement)
            ):
                if parameter.paramName == "Property":
                    return True
            if isTask(bpmElement):
                return True
            if isActivity(bpmElement):
                if parameter.paramName == "Property":
                    return True
                if (
                    not isDecomposable(bpmElement)
                    and parameter.paramName == "QueueLength"
                ):
                    return True
            return False
        return True

    @classmethod
    def isValidResult(cls, parameter: Parameter):
        if not cls.checkParameter(parameter):
            raise ParamNonValido()

        if not parameter.resultRequest:
            return True
        if parameter.paramName == "QueueLength":
            if not parameter.isResultOnly():
                return False
            for resultRequest in parameter.resultRequest:
                if resultRequest.value in {"min", "max", "mean"}:
                    return True
        return False

    @property
    def parameters(self) -> List[Parameter]:
        l: List[Parameter] = (
            []
            if self.__parameters.get("Property") is None
            else self.__parameters["Property"]
        )
        ql: Parameter | None = self.__parameters.get("QueueLength")
        if ql != None:
            l.append(ql)
        return l

    def toXML(self):
        return super().toXML(self.parameters)

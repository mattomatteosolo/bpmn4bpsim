from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpmnfacilities import bpmn_types
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import ParameterGroups
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import NumericParameter, FloatingParameter
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import ExpressionParameter
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType


class CostParameters(ParameterGroups):
    '''
    Classe "scaffold" per i parametri che specificano il costo di un elemento di un processo di business

    Parameters
    ----------
    bpmElement : `etree._Element`
        Elemento BPM sul quale applicare i parametri passati
    params : `list`
        Lista di tuple (str, Parameter), in cui il primo elemento di ciascuna tupla è uno dei parametri del gruppo

    Params
    ------
    FixedCost : `NumericParameter` o `FloatingParameter`
        Costo fisso da pagare per ogni "occorrenza", espresso in funzione di baseCurrencyUnit definito in ScenarioParameters
    UnitCost : `NumericParameter` o `FloatingParameter`
        Costo per unità do tempo, espresso come baseCurrencyUnit * baseTimeUnit, entrambi definiti in ScenarioParameter
    '''
    #################################################################
    # Costanti utilizzate nei vari metodi
    # Si tratta di elementi specifici per ciascun ParametersGroup
    #################################################################
    SEQ_ORDER = ('FixedCost', 'UnitCost')
    # TODO aggiungere supporto per DistributionParameter
    RESOLVES = {key:[NumericParameter, FloatingParameter, ExpressionParameter] for key in SEQ_ORDER}
    # RESOLVES.extend([getattr(DistributionParameter,distr) for distr in dir(DistributionParameter) if 'User' not in distr and 'Distribution' not in distr])

    def __init__(self, bpmElement: etree._Element, parameters: list):
        super().__init__(bpmElement, parameters)

    #################################################################
    # Metodi che controllano la validità dei parametri
    # e la loro applicabilità rispetto a determinati elementi BPMN
    #################################################################

    @classmethod
    def isApplicable(cls, bpmElement: etree._Element, parameter: Parameter):
        ####################
        # Type checking
        ####################
        if not etree.iselement(bpmElement):
            raise TypeError()
        ##############################
        # Controllo dei valori di I/O
        ##############################
        if not cls.isValidResult(parameter):
            return False
        if not cls.parameterHasValidValues(parameter):
            return False
        ##################################################
        # Controllo di applicabilità
        ##################################################
        if bpmn_types.isActivity(bpmElement) or bpmn_types.isProcess(bpmElement) or bpmn_types.isResource(bpmElement):
            return True
        ##########
        # Default
        ##########
        return False

    @classmethod
    def isValidResult(cls, parameter: Parameter):
        if not cls.checkParameter(parameter):
            raise ParamNonValido()
        if not parameter.resultRequest:
            return True
        if parameter.resultRequest == [ResultType.SUM]:
            return True
        return False

    ####################
    # Setters e Getters
    ####################
    # TODO

    ###################################
    # Metodi per la de-/serializzazione
    ###################################

    def toXML(self):
        return super().toXML(self.parameters)

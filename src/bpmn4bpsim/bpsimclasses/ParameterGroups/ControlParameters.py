from lxml import etree
from bpmn4bpsim.bpsimclasses import ParamNonValido
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroups,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    NumericParameter,
    DurationParameter,
    FloatingParameter,
    BooleanParameter,
)
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)
from bpmn4bpsim.bpmnfacilities import bpmn_conditional_types, bpmn_types


class ControlParameters(ParameterGroups):
    """
    Classe che raggruppa i parametri che specificano controlli per il flusso della simulazione

    Parameters
    ----------
    bpmElement : `etree._Element`
        Elemento al quale vanno applicati i parametri. Questo viene utilizzato soltanto per effettuare i controlli in fase di inizializzazione, ed il suo riferimento non viene salvato
    parameters : `list`
        Lista di parametri da utilizzare. A seguire la lista dei nomi di parametro validi per ControlParameters

    Parameters' names
    -----------------
    InterTriggerTimer
        Intervallo di tempo tra le occorenze di un certo evento. Tale evento occore ogni volta che "scade" il timer fino ad un massimo stabilito nel parametro `TriggerCount` :resolve `NumericParameter`, `FloatingParameter`
    TriggerCount
        Massimo numero di volte che l'evento viene triggerato
    Probability
        Probabilità che il controllo sia passato all'elemento. Il parametro deve risolvere ad un Numeric o Floating Parameter. Il valore di default dipende dall'elemento referenziato: per i sequenceFlow è distribuita in egual maniera (e.g. 4 sequence senza che il parametro sia definito, tutti hanno il 25% di probabilità), per gli eventi è 0
    Condition
        Condizione che deve essere riscontrata prima di passare il controllo a questo elemento
    """

    #################################################################
    # Costanti utilizzate nei vari metodi
    # Si tratta di elementi specifici per ciascun ParametersGroup
    #################################################################
    SEQ_ORDER = (
        "Probability",
        "Condition",
        "InterTriggerTimer",
        "TriggerCount",
    )
    RESOLVES = {
        "InterTriggerTimer": [
            NumericParameter,
            FloatingParameter,
            DurationParameter,
            ExpressionParameter,
        ],
        "TriggerCount": [NumericParameter, ExpressionParameter],
        "Probability": [
            NumericParameter,
            FloatingParameter,
            ExpressionParameter,
        ],
        "Condition": [BooleanParameter, ExpressionParameter],
    }
    # RESOLVE['InterTriggerTimer'].extend([getattr(DistributionParameter, distribution) for distribution in dir(DistributionParameter)
    #                                      if 'Distribution' in distribution and 'Parameter' not in distribution and 'User' not in distribution])

    def __init__(self, bpmElement: etree._Element, parameters: list):
        super().__init__(bpmElement, parameters)
        if (
            len(
                list(
                    filter(
                        lambda x: x.parameterName == "Probability",
                        self.parameters,
                    )
                )
            )
            > 0
            and len(
                list(
                    filter(
                        lambda x: x.parameterName == "Condition",
                        self.parameters,
                    )
                )
            )
            > 0
        ):
            raise ParamNonValido(
                "Uno solo tra Probability e Condition può essere specificato"
            )

    #################################################################
    # Metodi che controllano la validità dei parametri
    # e la loro applicabilità rispetto a determinati elementi BPMN
    #################################################################

    @classmethod
    def isApplicable(cls, bpmElement: etree._Element, parameter: Parameter):
        """
        Metodo che controlla se un parametro è applicabile per un certo elemento

        Metodo che restituisce vero se l'elemento `bpmElemen` è
        parametrizzabile dal parametro con nome `paramName`, sia in input, che
        in solo output, lasciando la responsabilità di controllare se il
        parametro è outputOnly al costruttore/setter

        Parameters
        ----------
        bpmElement : `etree._Element`
            Elemento BPMN per cui controllare l'applicabilità del parametro
        param : `Parameter`
            Coppia (paramName, Parameter) che contiene i dati necessari a
            verificare l'applicabilità del parametro.

        Returns
        -------
        `bool`
            True se il parametro è applicabile all'elemento sia se per I/O, sia
            se solo per output, False altrimenti

        Raises
        ------
        TypeError
            Se bpmElement non è un oggetto etree._Element, oppure se param non
            è una tuple
        ValueError
            Se gli elementi di param non sono una str e un Parameter
        """
        ##########################
        # Type checking
        ##########################
        if not etree.iselement(bpmElement):
            raise TypeError(
                "Atteso un elemento XML. Ricevuto " + str(type(bpmElement))
            )
        ##############################
        # Controllo dei valori di I/O
        ##############################
        if not cls.isValidResult(parameter):
            return False
        if not cls.parameterHasValidValues(parameter):
            return False
        ##################################################
        # Controllo di applicabilità
        ##################################################
        if bpmn_types.isStartEvent(bpmElement):
            if bpmn_conditional_types.startInEventSubProcess(
                bpmElement
            ) and (
                parameter.paramName == "Probability"
                or parameter.paramName == "Condition"
            ):
                return True
            if (
                parameter.paramName == "InterTriggerTimer"
                or parameter.paramName == "TriggerCount"
            ):
                return True
        if bpmn_types.isIntermediateEvent(bpmElement):
            if (
                bpmn_conditional_types.isCatchEvent(bpmElement)
                and parameter.paramName == "InterTriggerTimer"
            ):
                return True
        if bpmn_conditional_types.isBoundaryEvent(bpmElement) and (
            parameter.paramName == "Probability"
            or parameter.paramName == "Condition"
        ):
            return True
        if (
            bpmn_types.isTask(bpmElement)
            or bpmn_types.isSubProcess(bpmElement)
            or bpmn_types.isTransaction(bpmElement)
            or bpmn_types.isCallActivity(bpmElement)
        ):
            if (
                parameter.paramName == "InterTriggerTimer"
                or parameter.paramName == "TriggerCount"
            ):
                if not bpmn_conditional_types.isDecomposable(
                    bpmElement
                ) and not bpmn_conditional_types.hasIncoming(
                    bpmElement
                ):
                    return True
        if bpmn_types.isEventSubProcess(
            bpmElement
        ) and not bpmn_conditional_types.isDecomposable(bpmElement):
            return True
        if bpmn_conditional_types.startInEventSubProcess(
            bpmElement
        ):
            return True
        if bpmn_types.isSequenceFlow(bpmElement):
            if (
                parameter.paramName == "Probability"
                or parameter.paramName == "Condition"
            ):
                return True
        # in caso di TriggerCount solo per output, COUNT è ammesso per tutte le activity (anche decomponibili) e per i process
        if bpmn_types.isActivity(bpmElement) or bpmn_types.isProcess(
            bpmElement
        ):
            if (
                parameter.isResultOnly()
                and parameter.paramName == "TriggerCount"
            ):
                return True
        ##########
        # Default
        ##########
        return False

    @classmethod
    def isValidResult(cls, parameter: Parameter):
        """
        Metodo che restituisce vero se param non contiene valori per ResultType, oppure se contiene valori in accordo con le specifiche.

        Se 'InterTriggerTimer', sono ammessi: MIN, MAX, MEAN, SUM. Se 'TriggerCount': COUNT.

        Parameters
        ----------
        parameter : `Parameter`
            Parametro di cui controllare la validità

        Returns
        -------
        `bool`
            True se param._resultRequest == [] or None, oppure se param._resultRequest è consistente con le specifiche BPSim. Falso altrimenti

        Raises
        ------
        TypeError
            Se paramName non è una str oppure se param non è un Parameter
        ValueError
            Se paramName non è tra i nomi validi
        """
        ##########################
        # Type checking
        ##########################
        if not cls.checkParameter(parameter):
            raise ParamNonValido()
        #########################################################################
        # Controllo di validità dei valori di resultType in param
        # Come anticipato, se un parametro non ne ha, allora questi sono "validi"
        #########################################################################
        if parameter.resultRequest == [] or parameter.resultRequest == None:
            return True
        #############################################
        # Controlli specifici per i ControlParameters
        #############################################
        for resultType in parameter.resultRequest:
            if parameter.paramName == "InterTriggerTimer":
                if resultType == ResultType.COUNT:
                    return False
            elif parameter.paramName == "TriggerCount":
                if resultType != ResultType.COUNT:
                    return False
            else:
                return False
        return True

    # TODO
    # @classmethod
    # def isValidUserDistribution(cls,paramName: str, userDistr: DistributionParameter.UserDistribution):
    #     '''
    #     Metodo che determina se la UserDistribution fornita ha tutti i punti validi per un certo parametro

    #     Parameters
    #     ----------
    #     paramName : str
    #         Nome del parametro di cui verificare l'applicabilità di userDistr
    #     userDistr : UserDistribution
    #         UserDistribution di cui controllare il tipo dei punti

    #     Returns
    #     -------
    #     `bool`
    #         True se tutti i punti di userDistr hanno valori validi, False altriment

    #     Raises
    #     ------
    #     TypeError
    #         [description]
    #     ValueError
    #         [description]
    #     '''
    #     if not isinstance(paramName,str):
    #         raise TypeError()
    #     if not isinstance(userDistr, DistributionParameter.UserDistribution):
    #         raise TypeError()
    #     if paramName not in cls.RESOLVE.keys():
    #         raise ValueError()

    #     for point in userDistr.points:
    #         validPoint = False
    #         for resolveType in cls.RESOLVE[paramName]:
    #             if isinstance(point,resolveType):
    #                 validPoint = True
    #                 break

    #         if not validPoint:
    #             return validPoint

    #     return True

    ####################
    # Setters e Getters
    ####################
    # TODO

    ###################################
    # Metodi per la de-/serializzazione
    ###################################

    def toXML(self):
        """Metodo per la serializzazione in XML

        Returns
        -------
        etree._Element
            Serializzazione dell'oggetto corrente secondo lo schema XML di BPSim
        """
        return super().toXML(self.parameters)

from bson import ObjectId
from lxml import etree
from typing import Dict, List
from . import BPSIM_NSMAP, BPSIM, ParamNonValido
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroups,
)
from bpmn4bpsim.bpsimclasses.VendorExtension import VendorExtension


class ElementParameters(object):
    """
    Classe concreta per la definizione di tutti i parametri legati ad un
    particolare elemento del modello. Se più oggetti di questa classe fanno
    riferimento allo stesso elemento BPM, allori si applicheranno nell'ordine
    dato.

    NOTA: Non ci interessa fornire alcuna estensione proprietaria

    Parameters
    ----------
    id : str
        Identificativo univoco di un oggetto `ElementParameter`. Per quanto
        riguarda la nostra implementazione, utilizzeremo gli `ObjectId` tipici
        di MongoDB in quanto forniscono una sorta di hash crittografico
    elementRef : etree._Element
        Riferimento ad un elemento del processo di business al quale applicare
        i parametri contenuti
    parametersGroup : List[ParametersGroup]
        Lista di ParametersGroup
    vendorExtensions : List[VendorExtension]
        Lista di VendorExtension
    """

    SEQ_ORDER = (
        "TimeParameters",
        "ControlParameters",
        "ResourceParameters",
        "PriorityParameters",
        "CostParameters",
        "PropertyParameters",
        "VendorExtension",
    )

    def __init__(
        self,
        elementRef: etree._Element,
        parametersId=None,
        parametersGroup: List[ParameterGroups] = [],
        vendorExtensions: List[VendorExtension] = [],
    ):
        if parametersId is None:
            parametersId = str(ObjectId())
        #########################
        # Assegnazione valori
        #########################
        self.__parameters: Dict[str, ParameterGroups] = {
            p.__class__.__name__: p for p in parametersGroup
        }
        self.__vendorExtension = vendorExtensions
        ##################################################
        # Controlla che tale elemento abbia senso
        ##################################################
        if not self.__parameters and not self.__vendorExtension:
            raise ParamNonValido(
                "Useless ElementParameters, since neither parameters nor "
                "vendor extensions were provided"
            )
        self.__id = parametersId
        elementId = elementRef.get("id")
        if elementId is None:
            raise ValueError(
                "Given element doesn't have an attribute called id"
            )
        self.__elementRef = elementId

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, ElementParameters):
            return False
        return (
            self.elementRef == other.elementRef
            and self.groupParameters == other.groupParameters
            and self.parametersId == other.parametersId
        )

    def __hash__(self):
        return hash((self.__id, self.elementRef, tuple(self.parametersGroup)))

    ##############################
    # Sezione per Setters/Getters
    ##############################

    @property
    def elementRef(self):
        return self.__elementRef

    @property
    def parametersId(self):
        return self.__id

    @parametersId.setter
    def parametersId(self, _id: str):
        if not isinstance(_id, str):
            raise TypeError()
        self.__id = _id

    @property
    def groupParameters(self):
        return self.__parameters

    @property
    def parametersGroup(self) -> List[ParameterGroups]:
        return [
            parameter_group for parameter_group in self.__parameters.values()
        ]

    def parameterGroup(self, groupParameter: str) -> ParameterGroups | None:
        """
        Metodo che restituisce un ParameterGroup appartenente al corrente
        oggetto ElementParameters, dato il suo nome

        Se il nome passato non è valido, lancia un errore. Se, invece, il
        gruppo non è trovato, restituisce None

        Parameters
        ----------
        groupParameter : str
            Nome del gruppo da restituire

        Returns
        -------
        ParametersGroup
            Gruppo di parametri il cui nome è groupParameter

        Raises
        ------
        TypeError
            Se groupParameter non è una str
        ValueError
            Se il nome non è quello di un gruppo di parametri BPSIM
        """
        if not isinstance(groupParameter, str):
            raise TypeError(
                "Attesso un oggetto di tipo str. Ricevuto: "
                f"{groupParameter.__class__.__name__}"
            )
        elif groupParameter not in self.SEQ_ORDER:
            raise ValueError(
                f"{groupParameter} non è il nome di un gruppo di parametri "
                "BPSim"
            )
        if not self.__parameters:
            return None
        return self.__parameters.get(groupParameter)

    def addGroupParameter(self, groupParameter: ParameterGroups):
        """
        Metodo per aggiungere un ParameterGroup all'ElementParameter corrente

        Parameters
        ----------
        groupParameter : ParametersGroup
            OgettoParameterGroup da aggiungere

        Raises
        ------
        TypeError
            Se groupParameter non è instanza di una sottoclasse di
            ParametersGroup
        ValueError
            Se esiste già un ParametersGroup dello stesso tipo nel corrente
            ElementParameter
        """
        if not isinstance(groupParameter, ParameterGroups):
            raise TypeError(
                "Atteso un oggetto di tipo Parametersroup, o una sua "
                f"sotto-classe. Ricevuto: {groupParameter.__class__.__name__}"
            )
        if self.__parameters is None:
            self.__parameters = {}
        group_name = groupParameter.__class__.__name__
        if any(group_name == pg for pg in self.__parameters):
            raise ValueError(
                f"L'oggetto ElementParameter con id {self.__id} possiede"
                f" già {group_name}"
            )
        self.__parameters.update(
            {groupParameter.__class__.__name__: groupParameter}
        )

    def removeGroupParameter(self, group_name: str):
        """
        Metodo per rimuovere un ParameterGroup all'ElementParameter corrente
        dato il suo nome

        Parameters
        ----------
        group_name : str
            Nome del ParameterGroup da eleminare

        Raises
        ------
        ValueError
            Se non esiste già un ParametersGroup dello stesso tipo nel corrente
            ElementParameter
        """
        if group_name not in self.__parameters.keys():
            raise ValueError(f"Questo ElementParameters non ha {group_name}")
        self.__parameters.pop(group_name)

    def updateGroupParameter(self, group_parameter: ParameterGroups):
        """
        Metodo che aggiorna un ParametersGroup dell'ElementParameters corrente

        Parameters
        ----------
        group_parameter : ParametersGroup
            ParrametersGroup che sostituirà quello presente
            nell'ElementParameters corrente
        """
        # Prima cancella poi aggiungi
        self.removeGroupParameter(group_parameter.__class__.__name__)
        self.addGroupParameter(group_parameter)

    def toXML(self):
        """
        Metodo per la serializzazione in XML (lxml)

        Returns
        -------
        etree._Element
            Oggetto XML (lxml), serializzazione dell'oggetto corrente
        """
        elementParameters = etree.Element(
            BPSIM + "ElementParameters",
            nsmap={**BPSIM_NSMAP},
            id=self.__id,
            elementRef=self.__elementRef,
        )
        for paramGroupName in self.SEQ_ORDER:
            if self.__parameters.get(paramGroupName):
                elementParameters.append(
                    self.__parameters[paramGroupName].toXML()
                )
        return elementParameters

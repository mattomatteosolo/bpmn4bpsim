from datetime import datetime


def GeneraTimeStamp(timestamp=None):
    '''
    Funzione che prende in input opzionale un timestamp, che, se valido,
    restituisce in iso8601 UTC/Zulu, altrimenti, genera il timestamp attuale

    Parameters
    ----------
    timestamp : string
        Stringa che contiene la data, che, da definizione deve essere in iso8601
    '''
    if timestamp:
        try:
            return datetime.fromisoformat(timestamp).isoformat()
        except BaseException:
            return datetime.utcnow().isoformat()[:-7]

    return datetime.utcnow().isoformat()[:-7]

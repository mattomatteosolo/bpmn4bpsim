from collections import Counter
from copy import deepcopy
from lxml import etree
from typing import List
from bpmn4bpsim.bpsimclasses import ParamNonValido, BPSIM_NSMAP, BPSIM
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroups,
)
from bpmn4bpsim.bpsimclasses.Scenario import Scenario
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpmnfacilities import SEMANTIC, BPMN_NAMESPACES


class Modello(object):
    """
    Classe per la gestione delle astrazioni del Modello BPMN

    Lo scopo principale di questa classe è semplificare le operazioni di parsing
     e de-/serializzazione

    Parameters
    ----------
    modello : etree._Element
        Modello (assunto essere "valido") BPMN
    nomeModello : str
        Nome del file che contiene il modello
    _id : str
        Campo _id del modello, utilizzato da Mongo
    scenari : list
        Lista di Scenario validi per modello

    Raises
    ------
    TypeError
        Se modello non è etree._Element o scenari non è list
    ParamNonValido
        Se esiste uno scenario in scenari che non è Scenario

    Methods
    -------
    addScenario(scenario: Scenario)
        Metodo usato per aggiungiere uno scenario alla lista di quelli già
        presenti
    removeScenario(idScenario: str)
        Metodo che serve ad eliminare uno scenario dalla lista
    updateScenario
        pass
    updateScenarioId
        pass
    addElementParameter
        pass
    removeElementParameter
        pass
    updateElementParameter
        pass
    addParameterGroup
        pass
    removeParameterGroup
        pass
    updateParameterGroup
        pass
    toXML()
        Metodo per la serializzazione in XML
    """

    def __init__(
        self,
        modello: etree._Element,
        nomeModello: str,
        _id: str,
        scenari: List[Scenario] = [],
    ):
        self.__nomeModello = nomeModello
        self.__modello = modello
        self.__scenari = scenari
        self.__id = _id

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Modello):
            return False
        return (
            self.__nomeModello == other.__nomeModello
            and self.__id == other.__id
            and Counter(self.__scenari) == Counter(other.__scenari)
        )

    def scenario(self, idScenario: str) -> Scenario | None:
        """
        Metodo che restituisce uno scenario associato al modello dato il suo id.

        Restituisce None se self.__scenari == None o se non esiste alcuno
        scenario corrispondente in self.__scenari

        Parameters
        ----------
        idScenario : str
            Id dello scenario, by default None

        Returns
        -------
        Scenario
            Scenario con idScenario == idScenario
        """
        if self.__scenari is None:
            return None
        for scenario in self.__scenari:
            if scenario._id == idScenario:
                return scenario
        return None

    # scenari = property(scenario)
    @property
    def scenari(self):
        return self.__scenari

    @property
    def modello(self):
        return self.__modello

    @property
    def nomeModello(self) -> str:
        return self.__nomeModello

    @property
    def _id(self) -> str:
        return self.__id

    def addScenario(self, scenario: Scenario) -> Scenario:
        """
        Metodo usato per aggiungiere uno scenario alla lista di quelli già
        presenti

        Per prima cosa, il metodo si assicura che sia già presente nella lista
        lo scenario con id = scenario.inherits, se  quello che stiamo cercando
        di inserire possiede l'attributo inherits.

        Inoltre, il metodo previene la presenza di scenari con lo stesso id,
        lanciando un'eccezione

        Parameters
        ----------
        scenario : Scenario
            Scenario che vogliamo inserire

        Raises
        ------
        ParamNonValido
            Se lo scenario che inseriamo ha un valore di inherits che non
            corrisponde a nessun id di scenario presenti nel modello
        ValueError
            Se eseiste uno scenario con lo stesso id di quello che stiamo
            inserendo
        """
        if self.__scenari is not None:
            if any(sc._id == scenario._id for sc in self.scenari):
                raise ValueError(
                    f"Uno scenario con id {scenario._id} esiste già. Si "
                    "consideri modificaScenario"
                )
        else:
            self.__scenari = []
        # Controllo dipendenze
        if scenario.inherits:
            if not any(sc._id == scenario.inherits for sc in self.scenari):
                raise ParamNonValido(
                    f"Non esiste alcuno scenario nel modello con id=scenario."
                    f"inherits (={scenario.inherits})"
                )
        self.__scenari.append(scenario)
        return scenario

    # O(len(self.__scenari))
    def removeScenario(self, idScenario: str):
        """
        Metodo che serve ad eliminare uno scenario dalla lista

        Il metodo, prima di effettuare la rimozione, si assicura che lo
        scenario con id idScenario esista, e che non esista alcun altro
        scenario che dipenda da questo. Ciò avviene perchè romperere le
        dipendenze renderebbe impossibile la simulazione

        Parameters
        ----------
        idScenario : str
            [description]

        Raises
        ------
        ParamNonValido
            Se non esiste scenario con id = idScenario o se un altro scenario
            ha questo come dipendenze
        """
        if not any(sc._id == idScenario for sc in self.__scenari):
            raise ParamNonValido(
                f"{idScenario} non è l'id di alcuno scenario creato per questo"
                " modello"
            )
        # controlla se nessun altro scenario dipende da quello fornito
        # cioè controllo dipendenze
        inherited = [
            scenario
            for scenario in self.__scenari
            if scenario.inherits == scenario
        ]
        if len(inherited) > 0:
            raise ParamNonValido(
                f"Lo scenario con id {idScenario} non è removibile in quanto "
                f"un altro scenario eredita da questo: {inherited[0]._id}"
            )
        self.__scenari.remove(
            [sc for sc in self.__scenari if sc._id == idScenario][0]
        )

    def updateIdScenario(self, vecchioId: str, nuovoId: str):
        """
        Metodo che si occupa di modificare l'id di uno scenario del modello

        Se non esistono scenari, o esiste già uno scenario con id nuovoId o non
        esiste uno scenario con id vecchioId lancia un'eccezone

        Parameters
        ----------
        vecchioId : str
            Id dello scenario di cui si vuole cambiare l'id
        nuovoId : str
            Id da sostituire a quello vecchio

        Raises
        ------
        TypeError
            Se vecchioId o nuovoId non sono oggetti di tipo str
        ValueError
            Se self.__scenari non è una lista con almeno un elemento, se esiste
            già uno scenario con id nuovoId o se non ne esiste uno con id
            vecchioId
        """
        if not isinstance(vecchioId, str):
            raise TypeError(
                "Atteso un oggetto di tipo str. Ricevuto: "
                f"{vecchioId.__class__.__name__}"
            )
        if not isinstance(nuovoId, str):
            raise TypeError(
                "Atteso un oggetto di tipo str. Ricevuto: "
                f"{nuovoId.__class__.__name__}"
            )
        if not self.__scenari:
            raise ValueError(
                "Si vuole modificare l'id di uno scenario, ma questo modello "
                "non ne possiede"
            )
        if any(sc._id == nuovoId for sc in self.__scenari):
            raise ValueError(f"Esiste già uno scenario con id {nuovoId}")
        if not any(sc._id == vecchioId for sc in self.__scenari):
            raise ValueError(f"Non esiste alcuno scenario con id {vecchioId}")
        for scenario in self.__scenari:
            # aggiorna tutte le occorenze di inherits
            if scenario.inherits == vecchioId:
                scenario.inherits = nuovoId
            # aggiorna l'id dello scenario
            elif scenario._id == vecchioId:
                scenario._id = nuovoId

    def updateScenario(self, scenario: Scenario):
        """
        Metodo che si occupa di modificare uno scenario

        Parameters
        ----------
        scenario : Scenario
            Scenario con id uguale ad uno già presente in self.__scenari da
            modificare

        Raises
        ------
        TypeError
            Se scenario non è un oggetto di tipo Sceario
        ValueError
            Se non esiste Scenario con id scenario.idScenario, se non esiste
            alcuno scenaio con id scenario.inherits in self.__scenari (vale
            anche se self.__scenari è una lista vuota o None)
        """
        if not isinstance(scenario, Scenario):
            raise TypeError(
                f"Atteso un oggetto di tipo Scenario. Ricevuto \
                    {scenario.__class__.__name__}"
            )
        if not self.__scenari:
            raise ValueError("Non sono presenti scenari nel modello.")
        to_modify = self.scenario(scenario._id)
        if not to_modify:
            raise ValueError(
                f"Nessuno scenario con id {scenario._id}. \
                    Si consideri addScenario"
            )
        if scenario.inherits and scenario.inherits != to_modify.inherits:
            # devo controllare che sia tutto in regola
            if not any(sc._id == scenario.inherits for sc in self.__scenari):
                raise ValueError(
                    f"Nessun scenario con id {scenario.inherits} è stato \
                        trovato nel modello"
                )
        self.__scenari.remove(to_modify)
        self.__scenari.append(scenario)

    def addElementParameter(
        self, idScenario: str, elementParameter: ElementParameters
    ):
        """
        Metodo che permette di aggiungere un oggetto di tipo ElementParameters
        allo scenario con id idScenario del corrente modello

        Parameters
        ----------
        idScenario : str
            Id dello scenari a cui aggiungere elementParameter
        elementParameter : dict
            Oggetto JSON che rappresenta un ElementParameters da aggiungere
            allo scenario con id idScenario

        Returns
        str
            Id dell'oggetto ElementParameter appena aggiunto

        Raises
        ------
        ValueError
            Se elementRef non è l'id di alcun elemento del modello, oppure se
            l'id con scenario idScenario non appartiene alla lista degli
            scenari del modello corrente
        """
        scenario = self.scenario(idScenario)
        if not scenario:
            raise ValueError(
                f"Nessuno scenario con id {idScenario} trovato nella lista "
                "degli scenari associati al modello"
            )
        scenario.addElementParameter(elementParameter)
        return elementParameter.parametersId

    def addGroupParameter(
        self,
        idScenario: str,
        idElementParameter: str,
        groupParameter: ParameterGroups,
    ):
        """
        Metodo usato per aggiungere un ParametersGroup ad uno specifico
        ElementParameter di uno specifico scenario del modello corrente

        Parameters
        ----------
        idScenario : str
            Id dello scenario a cui appartiene l'ElementParameter da aggiornare
        idElementParameter : str
            Id dell'ElementParameter da aggiornare
        groupParameterJSON : dict
            Oggetto JSON contenente i dati del ParameterGroup da inserire

        Raises
        ------
        ValueError
            - Se non esiste uno scenario con id idScenario negli scenari del
            modello
            - Se lo scenario selezionato non contiene ElementParameter con id
            idElementParameter
            - Se il modello non contiene un elemento con in id
            elementParameter.elemntRef
        TypeError
            se groupParameter non è di tipo ParametersGroup o non è derivabile
            come ParametersGroup
        """
        scenario = self.scenario(idScenario)
        if scenario is None:
            raise ValueError(
                f"Il modello corrente non ha alcuno scenario con id {idScenario}"
            )
        elementParameter = scenario.elementParameter(idElementParameter)
        if elementParameter is None:
            raise ValueError(
                f"Lo scenario con id {idScenario} non possiede alcun "
                f"ElementParameter con id {idElementParameter}"
            )
        # elementRef = self.__modello.xpath(
        #     f'//*[@id="{elementParameter.elementRef}"]'
        # )
        # if not elementRef:
        #     raise ValueError("Il modello è inconsistente con gli scenari")
        # if not isinstance(elementRef, list):
        #     raise Exception()
        # if not isinstance(elementRef[0], etree._Element):
        #     raise Exception()
        # elementRef = elementRef[0]
        elementParameter.addGroupParameter(groupParameter)

    def removeGroupParameter(
        self,
        idScenario: str,
        idElementParameter: str,
        groupParameter: ParameterGroups,
    ):
        """
        Metodo usato per rimuovere un ParametersGroup ad uno specifico
        ElementParameter di uno specifico scenario del modello corrente

        Parameters
        ----------
        idScenario : str
            Id dello scenario a cui appartiene l'ElementParameter da aggiornare
        idElementParameter : str
            Id dell'ElementParameter da aggiornare
        groupParameterJSON : dict
            Oggetto JSON contenente i dati del ParameterGroup da rimuovere

        Raises
        ------
        ValueError
            - Se non esiste uno scenario con id idScenario negli scenari del
            modello
            - Se lo scenario selezionato non contiene ElementParameter con id
            idElementParameter
            - Se il modello non contiene un elemento con in id
            elementParameter.elemntRef
        TypeError
            Se groupParameter non è di tipo ParametersGroup o non è derivabile
            come ParametersGroup
        """
        scenario = self.scenario(idScenario)
        if not scenario:
            raise ValueError(
                f"Il modello corrente non ha alcuno scenario con id {idScenario}"
            )
        elementParameter = scenario.elementParameter(idElementParameter)
        if not elementParameter:
            raise ValueError(
                f"Lo scenario con id {idScenario} non possiede alcun "
                f"ElementParameter con id {idElementParameter}"
            )
        # elementRef = self.__modello.xpath(
        #     f'//*[@id="{elementParameter.elementRef}"]'
        # )
        # if not elementRef:
        #     raise ValueError("Il modello è inconsistente con gli scenari")
        # elementRef = elementRef[0]
        elementParameter.removeGroupParameter(
            groupParameter.__class__.__name__
        )

    def updateGroupParameter(
        self,
        idScenario: str,
        idElementParameter: str,
        groupParameter: ParameterGroups,
    ):
        """
        Metodo usato per rimuovere un ParametersGroup ad uno specifico
        ElementParameter di uno specifico scenario del modello corrente

        Parameters
        ----------
        idScenario : str
            Id dello scenario a cui appartiene l'ElementParameter da aggiornare
        idElementParameter : str
            Id dell'ElementParameter da aggiornare
        groupParameterJSON : dict
            Oggetto JSON contenente i dati del ParameterGroup da rimuovere

        Raises
        ------
        ValueError
            - Se non esiste uno scenario con id idScenario negli scenari del
            modello
            - Se lo scenario selezionato non contiene ElementParameter con id
            idElementParameter
            - Se il modello non contiene un elemento con in id
            elementParameter.elemntRef
        TypeError
            Se groupParameter non è di tipo ParametersGroup o non è derivabile
            come ParametersGroup
        """
        scenario = self.scenario(idScenario)
        if not scenario:
            raise ValueError(
                f"Il modello corrente non ha alcuno scenario con id {idScenario}"
            )
        elementParameter = scenario.elementParameter(idElementParameter)
        if not elementParameter:
            raise ValueError(
                f"Lo scenario con id {idScenario} non possiede alcun "
                f"ElementParameter con id {idElementParameter}"
            )
        # elementRef = self.__modello.xpath(
        #     f'//*[@id="{elementParameter.elementRef}"]'
        # )
        # if not elementRef:
        #     raise ValueError("Il modello è inconsistente con gli scenari")
        # elementRef = elementRef[0]
        elementParameter.updateGroupParameter(groupParameter)

    def toXML(self, idScenari: List[str]) -> etree._Element:
        """
        Metodo per la serializzazione del modello co scenari bpsim

        Returns
        -------
        etree._Element
            Element XML (lxml) contenente il modello e gli scenari con gli id
            fornti

        Raises
        ------
        ParamNonValido
            Se viene richesto un id scenario non valido
        """
        if len(idScenari) == 0:
            return self.modello

        scenariIds = {sc._id for sc in self.__scenari}

        if not set(idScenari).issubset(scenariIds):
            raise ParamNonValido(
                f"Ricevuti id per gli scenari non validi: {repr(idScenari)}"
            )
        requestedScenari = [sc for sc in self.__scenari if sc._id in idScenari]

        # costruzione elementi di appoggio
        rel = etree.Element(
            SEMANTIC + "relationship",
            nsmap={**BPMN_NAMESPACES, **BPSIM_NSMAP},
            type="BPSimData",
        )
        extensionElements = etree.Element(
            SEMANTIC + "extensionElements", nsmap={**BPMN_NAMESPACES}
        )
        bpsimData = etree.Element(
            BPSIM + "BPSimData", nsmap={**BPMN_NAMESPACES}
        )
        # dependency check
        for sc in requestedScenari:
            if sc.inherits and sc.inherits not in idScenari:
                raise ParamNonValido(
                    f"Lo scenario con id {sc._id} eredita da "
                    f"{sc.inherits} che non è nella lista fornita."
                )
            bpsimData.append(sc.toXML())

        # associazione elementi di appaggio a copia modello
        extensionElements.append(bpsimData)
        rel.append(extensionElements)
        modello = deepcopy(self.__modello)
        definitions = modello.xpath(
            "//*[local-name()='definitions']"
        )
        # print(etree.tostring(modello, pretty_print=True, encoding="unicode"))
        if definitions is None:
            raise ValueError("No definitions found")
        definitions[0].append(rel)

        return modello

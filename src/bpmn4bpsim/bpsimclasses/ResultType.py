from enum import Enum


class ResultType(Enum):
    MIN = "min"
    MAX = "max"
    MEAN = "mean"
    COUNT = "count"
    SUM = "sum"

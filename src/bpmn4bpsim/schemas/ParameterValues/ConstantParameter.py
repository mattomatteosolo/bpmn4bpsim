from marshmallow import fields, post_load
from bpmn4bpsim.schemas.ParameterValues.ParameterValue import ParameterValueSC
from bpmn4bpsim.schemas.fields import Duration
from bpmn4bpsim.bpsimclasses.ParameterValues.ConstantParameter import (
    StringParameter,
    BooleanParameter,
    NumericParameter,
    TimeUnit,
    FloatingParameter,
    DateTimeParameter,
    DurationParameter,
)


class ConstantParameterSC(ParameterValueSC):
    value = fields.Raw(required=True)


class StringParameterSC(ConstantParameterSC):
    value = fields.String(required=True)

    @post_load
    def make_string_parameter(self, data, **kwargs) -> StringParameter:
        return StringParameter(**data)


class BooleanParameterSC(ConstantParameterSC):
    value = fields.Boolean(required=True)

    @post_load
    def make_boolean_parameter(self, data, **kwargs) -> BooleanParameter:
        return BooleanParameter(**data)


class NumericParameterSC(ConstantParameterSC):
    value = fields.Integer(required=True)
    timeUnit = fields.String(validate=TimeUnit.isTimeUnit)

    @post_load
    def make_numeric_parameter(self, data, **kwargs) -> NumericParameter:
        return NumericParameter(**data)

    def dump(self, obj: NumericParameter, **kwargs):
        data: dict = super().dump(obj)
        if obj.timeUnit:
            data["timeUnit"] = obj.timeUnit.value
        return data


class FloatingParameterSC(ConstantParameterSC):
    value = fields.Float(required=True)
    timeUnit = fields.String(validate=TimeUnit.isTimeUnit)

    @post_load
    def make_floating_parameter(self, data, **kwargs) -> FloatingParameter:
        return FloatingParameter(**data)

    def dump(self, obj: FloatingParameter, **kwargs):
        data: dict = super().dump(obj)
        if obj.timeUnit:
            data["timeUnit"] = obj.timeUnit.value
        return data


class DurationParameterSC(ConstantParameterSC):
    value = Duration(required=True)

    @post_load
    def make_duration_parameter(self, data, **kwargs) -> DurationParameter:
        return DurationParameter(**data)


class DatetimeParameterSC(ConstantParameterSC):
    value = fields.DateTime(required=True)

    @post_load
    def make_datetime_parameter(self, data, **kwargs) -> DateTimeParameter:
        return DateTimeParameter(**data)

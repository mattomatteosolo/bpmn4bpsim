from marshmallow import fields, post_load
from bpmn4bpsim.bpsimclasses.ParameterValues.EnumParameter import EnumParameter
from bpmn4bpsim.schemas.ParameterValues import constant_schemas
from bpmn4bpsim.schemas.ParameterValues.ExtendedParameter import ExtendedParameterValueSC
from bpmn4bpsim.schemas.ParameterValues.ParameterValue import ParameterValueSC


class EnumParameterSC(ParameterValueSC):
    value = fields.List(
        fields.Nested(
            ExtendedParameterValueSC(constant_schemas)
        )
    )

    @post_load
    def make_enum_parameter(self, data: dict, **kwargs) -> EnumParameter:
        return EnumParameter(
            data['value'],
            data.get('validFor')
        )

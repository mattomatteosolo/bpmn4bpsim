from marshmallow import fields, post_load, post_dump, ValidationError
from typing import Mapping
from ..ParameterValue import ParameterValueSC
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.DistributionParameter import TruncatedNormalDistribution


class DistributionParameterSC(ParameterValueSC):
    timeUnit = fields.String(validate=TimeUnit.isTimeUnit)

    @post_dump
    def delete_none_field(self, data, **kwargs):
        if 'timeUnit' in data and data['timeUnit'] is None:
            data.pop('timeUnit')
        return data


class TruncatedNormalDistributionSC(DistributionParameterSC):
    mean = fields.Float()
    standardDeviation = fields.Float()
    min = fields.Float()
    max = fields.Float()

    @post_load
    def make_trunc_norm_distr(self, data: Mapping, **kwargs) -> TruncatedNormalDistribution:
        mean = data.get('mean')
        standardDeviation=data.get('standardDeviation')
        minV=data.get('min')
        maxV=data.get('max')
        if mean == None:
            raise ValidationError("Missing mean")
        if standardDeviation == None:
            raise ValidationError("Missing standardDeviation")
        if minV == None:
            raise ValidationError("Missing minV")
        if maxV == None:
            raise ValidationError("Missing maxV")
        return TruncatedNormalDistribution(
            mean= mean,
            standardDeviation=standardDeviation,
            minV=minV,
            maxV=maxV
        )

    @post_dump
    def make_dict(self, data: dict, **kwargs):
        if data.get('validFor') == None:
            data.pop('validFor', None)
        if data.get('timeUnit') == None:
            data.pop('timeUnit', None)
        return data

from marshmallow import (
    Schema,
    fields,
    ValidationError,
    validates,
    post_load,
    post_dump,
)
from typing import List
from bpmn4bpsim.bpsimclasses.ParameterValues.DistributionParameter.UserDistribution import (
    UserDistributionDataPoint,
    UserDistribution,
)
from bpmn4bpsim.schemas.ParameterValues import no_dist_schemas
from bpmn4bpsim.schemas.ParameterValues.DistributionParameter.DistributionParameter import (
    DistributionParameterSC,
)
from bpmn4bpsim.schemas.ParameterValues.ExtendedParameter import (
    ExtendedParameterValueSC,
)


class UserDistributionDataPointSC(Schema):
    value = fields.Nested(
        ExtendedParameterValueSC(type_schemas=no_dist_schemas), required=True
    )
    probability = fields.Float(required=True)

    @validates("probability")
    def validate_probability(self, probability: float, **kwargs):
        if probability <= 0 or probability > 1.0:
            raise ValidationError(
                "0 < probability >= 1.0 Ricevuo: " + str(probability)
            )

    @post_load
    def make_data_point(
        self, data: dict, **kwargs
    ) -> UserDistributionDataPoint:
        return UserDistributionDataPoint(**data)


class UserDistributionSC(DistributionParameterSC):
    points = fields.List(fields.Nested(UserDistributionDataPointSC))
    discrete = fields.Boolean()

    @validates("points")
    def validate_points(
        self, points: List[UserDistributionDataPoint], **kwargs
    ):
        sum_probability = 0.0
        for point in points:
            sum_probability += point.probability
        if sum_probability != 1.0:
            raise ValidationError(
                "Attesa una lista di UserDistributionDataPoint per points, in cui la somma delle diverse probability sia esattamente 1.0"
            )

    @post_load
    def make_user_distribution(self, data: dict, **kwargs) -> UserDistribution:
        return UserDistribution(**data)

    @post_dump
    def dump_user_dist(self, data, **kwargs):
        if data["validFor"] == None:
            del data["validFor"]
        return data

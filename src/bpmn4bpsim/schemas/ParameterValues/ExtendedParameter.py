from marshmallow import ValidationError, Schema
from marshmallow_oneofschema import OneOfSchema
from typing import Mapping


class ExtendedParameterValueSC(OneOfSchema):
    data_name = "parameterValue"

    def __init__(self, type_schemas: Mapping[str, Schema] = None, **kwargs):
        super().__init__(**kwargs)
        self.type_schemas = type_schemas

    def load(self, data: dict, **kwargs):
        data = data.copy()
        type_data = data.pop(self.type_field)
        data = data.get(self.data_name)
        data[self.type_field] = type_data

        if not (type_data and data):
            raise ValidationError('No type nor data were given')

        return super().load(data, **kwargs)

    def validate(self, data):
        try:
            self.load(data)
        except ValidationError as ve:
            return ve.messages
        return {}

    def dump(self, obj, **kwargs):
        data = {k: v for k, v in super().dump(
            obj, **kwargs).items() if v != None}
        data_type = data.pop(self.type_field)
        return {
            self.data_name: data,
            self.type_field: data_type
        }

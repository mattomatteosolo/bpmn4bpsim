from marshmallow import fields, ValidationError, validates, post_load
from bpmn4bpsim.schemas.ParameterValues.ParameterValue import ParameterValueSC
from bpmn4bpsim.bpsimclasses.ParameterValues.ExpressionParameter import (
    ExpressionParameter,
)


class ExpressionParameterSC(ParameterValueSC):
    value = fields.String(required=True)

    @validates("value")
    def validate_value(self, value: str, **kwargs):
        """
        Metodo per validare l'espressione contenuta nel value

        Parameters
        ----------
        value : str
            Stringa che deve contenere un'espressione xpath estesa da quelle BPSim

        Raises
        ------
        ValidationError
            Se la string non è un'espressione valida
        """
        if not isinstance(value, str):
            raise ValidationError("Invalid Expression")

    @post_load
    def make_expression_parameter(
        self, data: dict, **kwargs
    ) -> ExpressionParameter:
        return ExpressionParameter(**data)

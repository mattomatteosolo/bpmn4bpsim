from .ConstantParameter import (NumericParameterSC, FloatingParameterSC,
                                StringParameterSC, DatetimeParameterSC, DurationParameterSC, BooleanParameterSC)
from .ExpressionParameter import ExpressionParameterSC

constant_schemas = {
    'NumericParameter': NumericParameterSC,
    'FloatingParameter': FloatingParameterSC,
    'StringParameter': StringParameterSC,
    'BooleanParameter': BooleanParameterSC,
    'DurationParameter': DurationParameterSC,
    'DateTimeParameter': DatetimeParameterSC
}

from .EnumParameter import EnumParameterSC

no_dist_schemas = {
    **constant_schemas.copy(),
    'ExpressionParameter': ExpressionParameterSC,
    'EnumParameter': EnumParameterSC
}

from .DistributionParameter.UserDistribution import UserDistributionSC
from .DistributionParameter.DistributionParameter import TruncatedNormalDistributionSC

all_schemas = {
    **no_dist_schemas.copy(),
    'UserDistribution': UserDistributionSC,
    'TruncatedNormalDistribution': TruncatedNormalDistributionSC
    # eccetera eccetera
}
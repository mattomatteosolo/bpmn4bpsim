from marshmallow import Schema, fields, post_dump


class ParameterValueSC(Schema):
    validFor = fields.Str()

    @post_dump
    def delete_none_field(self, data, **kwargs):
        if 'validFor' in data and data['validFor'] is None:
            data.pop('validFor')
        return data
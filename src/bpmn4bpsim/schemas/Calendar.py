from icalendar import Calendar as iCalendar, Event
from isodate import parse_datetime, parse_duration, duration_isoformat
from marshmallow import (
    Schema, fields, ValidationError, post_load
)
from bpmn4bpsim.bpsimclasses.Calendar import Calendar


class iCalendarSC(fields.Field):
    # into object
    def _deserialize(
        self,
        calendar: iCalendar,
        attr,
        data,
        **kwargs
    ) -> dict:
        # check if calendar is None
        if calendar is None:
            raise ValidationError(
                'Expected a dict, got None'
            )
        ical = iCalendar()
        # setting prodid and version if None
        if calendar.get('prodid') is None:
            ical['prodid'] = 'BPSimaaS'
        if calendar.get('version') is None:
            ical['version'] = '2.0'
        # adding subcomponent (vevent)
        for item_name, item_value in calendar.items():
            if item_name == 'event':
                for event in item_value:
                    e = Event()
                    # creating any single vevent
                    for eventProperty, eventPropertyValue in event.items():
                        if eventProperty == 'dtstart' \
                                or eventProperty == 'dtend' \
                                or eventProperty == 'dtstamp':
                            e.add(eventProperty, parse_datetime(
                                eventPropertyValue))
                        elif eventProperty == 'duration':
                            e.add(eventProperty, parse_duration(
                                eventPropertyValue))
                        else:
                            e.add(eventProperty, eventPropertyValue)
                    ical.add_component(e)
            else:
                ical.add(item_name, item_value)
        return ical

    # into JSON
    def _serialize(
        self,
        calendar: dict,
        attr,
        obj,
        **kwargs
    ) -> iCalendar:
        # lowering key
        calendar_JSON = {item_name.lower(): item_value for item_name,
                         item_value in calendar.items()}
        # preaparing for event (and subcomponent that will be ignored)
        if len(calendar.subcomponents) > 0:
            calendar_JSON.update({'event': list()})
        for component in calendar.subcomponents:
            if isinstance(component, Event):
                # appending event
                event = dict()
                for item_name, item_value in component.items():
                    if item_name.lower() in {'dtstart', 'dtend', 'dtstamp'}:
                        event.update(
                            {
                                item_name.lower(): (parse_datetime(
                                    item_value.to_ical().decode('utf-8')
                                ).isoformat()[:19]
                                    .replace('-', '')
                                    .replace(':', '')
                                )
                            }
                        )
                    elif item_name.lower() == 'duration':
                        event.update({item_name.lower(): duration_isoformat(
                            parse_duration(item_value.to_ical().decode('utf-8')))})
                    elif item_name.lower() == 'rrule':
                        if isinstance(item_value, dict):
                            event['rrule'] = dict()
                            for rule_field_name, rule_field_value in dict(item_value).items():
                                event['rrule'].update(
                                    {rule_field_name.lower(): rule_field_value})
                        elif isinstance(item_value, str):
                            event['rrule'] = Calendar.parse_rrule(item_value)
                        else:
                            event['rrule'] = item_value
                    elif item_name.lower() == 'uid':
                        event[item_name.lower()] = str(item_value)
                    else:
                        event.update(
                            {
                                item_name.lower(): component.get(
                                    item_name
                                ).to_ical().decode('utf-8')
                            }
                        )
                calendar_JSON['event'].append(event)
        return calendar_JSON


class CalendarSC(Schema):
    id = fields.String()
    name = fields.String(required=True)
    calendar = iCalendarSC()

    @post_load
    def make_calendar(self, data: dict, **kwargs) -> Calendar:
        return Calendar(**data)

from lxml import etree
from marshmallow import validates, ValidationError, post_load, fields
from typing import List, Mapping
from bpmn4bpsim.bpsimclasses.ParameterGroups.ResourceParameters import ResourceParameters
from bpmn4bpsim.schemas.ParameterGroups.GroupParameters import ParametersGroupSC
from bpmn4bpsim.schemas.Parameter import ParameterSC, Parameter, all_schemas


class ResourceParametersSC(ParametersGroupSC):
    parameters = fields.List(fields.Nested(ParameterSC(schemas=all_schemas)))

    @validates('parameters')
    def validate_parameters(self, parameters: List[Parameter], **_):
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("Not a valid BPMN element was given")
        for parameter in parameters:
            if not ResourceParameters.isApplicable(self.bpmElement, parameter):
                raise ValidationError(
                    parameter.parameterName + ' is not a valid ResourceParameters')

    @post_load
    def make_resource_parameters(self, data: Mapping, **_) -> ResourceParameters:
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("Not a valid BPMN element was given")
        return ResourceParameters(self.bpmElement, **data)

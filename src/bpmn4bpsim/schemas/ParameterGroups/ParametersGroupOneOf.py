from lxml import etree
from marshmallow_oneofschema import OneOfSchema
from bpmn4bpsim.schemas.ParameterGroups import (
    CostParameters, ControlParameters, PriorityParameters,
    PropertyParameters, ResourceParameters, TimeParameters
)


class ParametersGroupOneOf(OneOfSchema):
    type_field = 'groupName'
    type_field_remove = True

    def __init__(self, bpmn_element: etree._Element, **kwargs):
        super().__init__(**kwargs)
        self.type_schemas = {
            'TimeParameters': TimeParameters.TimeParametersSC(bpmn_element),
            'CostParameters': CostParameters.CostParmetersSC(bpmn_element),
            'ControlParameters': ControlParameters.ControlParametersSC(bpmn_element),
            'PriorityParameters': PriorityParameters.PriorityParametersSC(bpmn_element),
            'PropertyParameters': PropertyParameters.PropertyParametersSC(bpmn_element),
            'ResourceParameters': ResourceParameters.ResourceParametersSC(bpmn_element)
        }

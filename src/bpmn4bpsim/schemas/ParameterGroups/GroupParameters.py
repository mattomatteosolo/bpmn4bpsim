from lxml import etree
from marshmallow import Schema, fields


class ParametersGroupSC(Schema):
    bpmElement = fields.Raw()

    def __init__(self, bpm_element: etree._Element | None, **kwargs):
        super().__init__(**kwargs)
        self.bpmElement = bpm_element

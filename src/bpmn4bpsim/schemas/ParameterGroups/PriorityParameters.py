from lxml import etree
from marshmallow import validates, fields, ValidationError, post_load
from typing import List, Mapping
from bpmn4bpsim.schemas.ParameterGroups.GroupParameters import ParametersGroupSC
from bpmn4bpsim.schemas.Parameter import ParameterSC, all_schemas
from bpmn4bpsim.bpsimclasses.ParameterGroups.PriorityParameters import PriorityParameters
from bpmn4bpsim.bpsimclasses.Parameter import Parameter




class PriorityParametersSC(ParametersGroupSC):
    parameters = fields.List(fields.Nested(ParameterSC(schemas=all_schemas)))

    @validates('parameters')
    def validate_parameters(self, parameters: List[Parameter], **_):
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("Not a valid BPMN element")
        for parameter in parameters:
            if not PriorityParameters.isApplicable(self.bpmElement, parameter):
                raise ValidationError(parameter.parameterName + ' is not a valid PriorityParameter')

    @post_load
    def make_priority_parameters(self, data: Mapping, **_) -> PriorityParameters:
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("Not a valid BPMN element")
        return PriorityParameters(self.bpmElement, **data)

from lxml import etree
from marshmallow import validates, ValidationError, fields, post_load
from typing import List, Mapping
from bpmn4bpsim.bpsimclasses.ParameterGroups.CostParameters import CostParameters
from bpmn4bpsim.schemas.ParameterGroups.GroupParameters import ParametersGroupSC
from bpmn4bpsim.schemas.Parameter import ParameterSC, Parameter
from bpmn4bpsim.schemas.Parameter import all_schemas



class CostParmetersSC(ParametersGroupSC):
    parameters = fields.List(fields.Nested(ParameterSC(schemas=all_schemas)))

    @validates('parameters')
    def validate_parameter(self, parameters: List[Parameter], **_):
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("not a valid BPMN element")
        for parameter in parameters:
            if not CostParameters.isApplicable(self.bpmElement, parameter):
                raise ValidationError('Parameter is not applicable to given element')

    @post_load
    def make_cost_parameter(self, data: Mapping, **_) -> CostParameters:
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("not a valid BPMN element")
        return CostParameters(self.bpmElement, **data)

from copy import deepcopy
from lxml import etree
from marshmallow import validates, post_load, ValidationError, fields, Schema
from marshmallow_oneofschema import OneOfSchema
from typing import List, Mapping, Union
from bpmn4bpsim.bpsimclasses.ParameterGroups.PropertyParameters import (
    PropertyParameters,
)
from bpmn4bpsim.bpsimclasses.Parameter import Parameter, Property
from bpmn4bpsim.schemas.ParameterGroups.GroupParameters import ParametersGroupSC
from bpmn4bpsim.schemas.Parameter import ParameterSC, PropertySC, all_schemas


class PropertyOrParameter(OneOfSchema):
    type_field = "parameterName"
    type_field_remove = True

    type_schemas: Mapping[str, Schema] = {
        "property": PropertySC(),
        "parameter": ParameterSC(schemas=all_schemas),
    }

    def get_obj_type(self, obj: Union[Property, Parameter]):
        if isinstance(obj, Property):
            return "property"
        elif isinstance(obj, Parameter):
            return "parameter"
        else:
            raise TypeError(
                "Unknown object type: {}".format(obj.__class__.__name__)
            )

    def load(self, data: dict, **_):
        """Method used to deserialize a Parameter or a Property

        Parameters
        ----------
        data : dict
            Data to be de-serialized

        Returns
        -------
        Union[Parameter, Property]
            De-serialized data
        """
        data = deepcopy(data)
        schema_name = data.get("parameterName")
        if schema_name == "Property":
            return self.type_schemas["property"].load(data)
        else:
            return self.type_schemas["parameter"].load(data)

    def dump(self, data: Union[Parameter, Property], **_):
        schema_name = self.get_obj_type(data)
        return self.type_schemas[schema_name].dump(data)


class PropertyParametersSC(ParametersGroupSC):

    parameters = fields.List(fields.Nested(PropertyOrParameter))

    def __init__(
        self,
        bpmn_element: etree._Element | None,
        scenario_parameters=False,
        **kwargs
    ):
        super().__init__(bpmn_element, **kwargs)
        self.scenario_parameters = scenario_parameters

    @validates("parameters")
    def validate_parameters(self, parameters: List[Parameter], **_):
        if not self.scenario_parameters:
            if not isinstance(self.bpmElement, etree._Element):
                raise ValidationError("Not a valid BPMN Element")
            for parameter in parameters:
                if not PropertyParameters.isApplicable(
                    self.bpmElement, parameter, self.scenario_parameters
                ):
                    raise ValidationError(
                        parameter.parameterName
                        + " is not a valid PropertyParameters"
                    )

    @post_load
    def make_priority_parameters(self, data: Mapping, **_):
        if isinstance(self.bpmElement, fields.Raw):
            raise ValidationError(
                "Something went wrong in validation, bpmElement still a Raw field"
            )
        return PropertyParameters(
            self.bpmElement,
            **data,
            scenario_parameters=self.scenario_parameters
        )

from copy import deepcopy
from lxml import etree
from marshmallow import fields, validates, ValidationError, post_load
from typing import List
from bpmn4bpsim.bpsimclasses.ParameterGroups.ControlParameters import (
    ControlParameters,
)
from bpmn4bpsim.schemas.ParameterGroups.GroupParameters import ParametersGroupSC
from bpmn4bpsim.schemas.ParameterValues import all_schemas
from bpmn4bpsim.schemas.Parameter import ParameterSC


class ControlParametersSC(ParametersGroupSC):
    parameters = fields.List(fields.Nested(ParameterSC(schemas=all_schemas)))

    @validates("parameters")
    def validate_parameters(self, data: List, **_):
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("Given element is not a valid BPMN element.")
        data = deepcopy(data)
        schema = ParameterSC(schemas=all_schemas)
        try:
                [schema.validate(i) for i in data]
                ControlParameters(self.bpmElement, data)
        except:
            raise ValidationError("Given parameters are not valid.")

    @post_load
    def make_control_parameters(self, data: dict, **_) -> ControlParameters:
        if not isinstance(self.bpmElement, etree._Element):
            raise ValidationError("")
        return ControlParameters(self.bpmElement, **data)

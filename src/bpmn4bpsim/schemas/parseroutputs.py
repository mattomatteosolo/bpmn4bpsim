from typing import Dict, Mapping
from marshmallow import Schema, fields, post_dump, pre_load, post_load
from marshmallow.decorators import validates_schema
from marshmallow.exceptions import ValidationError
from marshmallow_enum import EnumField
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import (
    ParameterGroupsName,
)
from bpmn4bpsim.parser.parseroutputs import (
    Possible,
    PossibleInputs,
    PossibleOutputs,
    OutputParameters,
)


class OutputParametersSC(Schema):
    parameterName = fields.Str()
    possibleOutputs = fields.List(EnumField(ResultType))

    @pre_load
    def uppering_po(self, data, **_):
        data["possibleOutputs"] = [i.upper() for i in data["possibleOutputs"]]
        return data

    @post_load
    def loading(self, data, **_):
        return OutputParameters(data["parameterName"], data["possibleOutputs"])

    @post_dump
    def dumping(self, data, **_) -> Dict:
        data["possibleOutputs"] = [i.lower() for i in data["possibleOutputs"]]
        return data


class PossibleOutputsSC(Schema):
    groupParameter = EnumField(ParameterGroupsName)
    parameters = fields.List(fields.Nested(OutputParametersSC()))

    @post_load
    def loading(self, data, **_):
        return PossibleOutputs(data["groupParameter"], data["parameters"])


class PossibleInputsSC(Schema):
    groupParameter = EnumField(ParameterGroupsName)
    parameters = fields.List(fields.Str())

    @post_load
    def loading(self, data, **_):
        return PossibleInputs(data["groupParameter"], data["parameters"])


class PossibleSC(Schema):
    xpathID = fields.Str()
    input = fields.List(fields.Nested(PossibleInputsSC()))
    output = fields.List(fields.Nested(PossibleOutputsSC()))

    @validates_schema
    def validate_possible(self, data: Mapping | Possible, **_):
        if isinstance(data, Possible):
            if not data.input and not data.output:
                raise ValidationError("Both input and output are empty")
        else:
            if not data.get("input") and not data.get("output"):
                raise ValidationError("Both input and output are empty")

    @post_load
    def loading(self, data, **_):
        return Possible(data["xpathID"], data["input"], data["output"])

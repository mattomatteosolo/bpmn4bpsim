from lxml import etree
from marshmallow import (
    Schema, fields, post_load, validates, ValidationError
)
from bpmn4bpsim.bpsimclasses.VendorExtension import VendorExtension



class VendorExtensionSC(Schema):
    name = fields.String(required=True)
    value = fields.String(required=True)

    @validates('value')
    def validate_value(self, data: str, **_):
        try:
            etree.XML(data)
        except:
            raise ValidationError('Not valid XML')

    @post_load
    def make_vendor_extension(self, data: dict, **_) -> VendorExtension:
        return VendorExtension(
            name=data['name'],
            value=etree.XML(data['value'])
        )

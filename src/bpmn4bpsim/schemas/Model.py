from lxml import etree
from marshmallow import (
    Schema, fields, validates, ValidationError, post_load, post_dump
)
from bpmn4bpsim.schemas.Scenario import ScenarioSC
from bpmn4bpsim.bpsimclasses.Modello import Modello


class ModelloSC(Schema):
    nomeModello = fields.String(required=True)
    _id = fields.String(required=True)
    scenari = fields.List(
        fields.Raw
    )

    def __init__(self, modello: etree._Element, **kwargs):
        self.modello = modello
        self.scenari = fields.List(
            fields.Nested(ScenarioSC(modello))
        )
        super().__init__(**kwargs)

    @post_load
    def make_modello(self, data: dict, **kwargs) -> Modello:
        schema = ScenarioSC(self.modello)
        data['scenari'] = [] if not data.get('scenari') else [
            schema.load(scenario) for scenario in data['scenari']]
        return Modello(
            self.modello,
            **data
        )

    @post_dump
    def make_modello_JSON(self, data: dict, **kwargs) -> dict:
        schema = ScenarioSC(self.modello)
        data['scenari'] = [] if not data.get('scenari') else [
            schema.dump(scenario) for scenario in data['scenari']]
        return data

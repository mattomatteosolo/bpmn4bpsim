from datetime import timedelta
from isodate import duration_isoformat, parse_duration
from marshmallow import fields, ValidationError
from typing import Union


class Duration(fields.Field):

    def _deserialize(self, value: Union[str, timedelta], attr, data, **kwargs):
        if isinstance(value, timedelta):
            return value
        try:
            return parse_duration(value)
        except:
            raise ValidationError(
                'The passed value is not a valid ISO duration string nor a string at all')

    def _serialize(self, value: timedelta, attr, obj, **kwargs):
        try:
            return duration_isoformat(value)
        except:
            return ""

    def _validate(self, value):
        if not isinstance(value, timedelta):
            if not isinstance(value, str):
                raise ValidationError('Input must be a string')

            try:
                duration_isoformat(value)
            except:
                raise ValidationError(
                    'The passed value is not a valid ISO duration')

from copy import deepcopy
from lxml import etree
from marshmallow import (
    Schema, validates, post_load, fields, pre_load, ValidationError,
    validates_schema, post_dump
)
from bpmn4bpsim.bpsimclasses.ElementParameters import ElementParameters
from bpmn4bpsim.bpsimclasses.ParameterGroups.GroupParameters import ParameterGroups
from bpmn4bpsim.schemas.ParameterGroups.ParametersGroupOneOf import ParametersGroupOneOf
from bpmn4bpsim.schemas.VendorExtension import VendorExtensionSC
from bpmn4bpsim.bpsimclasses import ParamNonValido


class ElementParametersSC(Schema):
    '''
    Classe per la definizione dello schema di un ElementParameter

    {
        "elementRef": `str`, // required
        "parametersId": `str`,
        "parametersGroup": `ParametersGroup`, // inteso come schema di disambiguazione per il polimorfismo
        "vendorExtensions": `List[VendorExtentions]`
    }
    '''
    elementRef = fields.String(required=True)
    parametersId = fields.String()
    parametersGroup = fields.List(fields.Raw())
    vendorExtensions = fields.List(fields.Nested(VendorExtensionSC))

    def __init__(self, document_root: etree._ElementTree, **kwargs):
        super().__init__(**kwargs)
        self.document_root = document_root

    @validates_schema
    def validate_lists(self, data: dict, **_):
        if not (data.get('parametersGroup')
                or data.get('vendorExtensions')):
            raise ValidationError(
                'Need at lest one between VendorExtensions or ParametersGroup, '
                'in order for an ElementParameter to be useful'
            )

    @validates('elementRef')
    def validate_element_ref(self, element_ref: str, **_):
        finder = etree.XPath(
            '//*[@id="{elementRef}"]'.format(elementRef=element_ref)
        )
        try:
            finder(self.document_root)[0]
        except IndexError:
            raise ValidationError(
                f"Didn't find any element with id {element_ref} within the "
                "root of the document"
            )

    @validates('parametersGroup')
    def validate_parameters_group(self, data, **_):
        for element in data:
            if not isinstance(element, ParameterGroups):
                raise ValidationError(
                    f'Not a valid ParmetersGroup: {element.__class__.__name__}')

    @pre_load
    def isolate_element_ref(self, data: dict, **_):
        data = deepcopy(data)
        finder = etree.XPath(
            '//*[@id="{elementRef}"]'.format(elementRef=data['elementRef'])
        )
        try:
            element: etree._Element = finder(self.document_root)[0]
        except IndexError:
            raise ParamNonValido(f'Elemento {data["elementRef"]} non trovato')
        schema = ParametersGroupOneOf(bpmn_element=element)
        for idx, parameter_group in enumerate(data['parametersGroup']):
            data['parametersGroup'][idx] = schema.load(parameter_group)
        return data

    @post_load
    def make_element_parameters(
        self,
        data: dict,
        **_
    ) -> ElementParameters:
        # data = deepcopy(data)
        finder = etree.XPath(
            '//*[@id="{elementRef}"]'.format(elementRef=data['elementRef'])
        )
        element = finder(self.document_root)[0]
        data['elementRef'] = element
        return ElementParameters(**data)

    @post_dump
    def make_dict(self, data: dict, **_):
        finder = etree.XPath(
            '//*[@id="{elementRef}"]'.format(elementRef=data['elementRef'])
        )
        element = finder(self.document_root)[0]
        schema = ParametersGroupOneOf(element)
        if data.get('parametersGroup'):
            for idx, parameter_group in enumerate(data['parametersGroup']):
                data['parametersGroup'][idx] = schema.dump(parameter_group)
        return data

from copy import deepcopy
from marshmallow import (
    Schema,
    fields,
    validates,
    ValidationError,
    post_load,
    post_dump,
    validates_schema,
    validate,
)
from typing import Mapping, List
from bpmn4bpsim.bpsimclasses.ResultType import ResultType
from bpmn4bpsim.bpsimclasses.Parameter import Parameter, Property
from bpmn4bpsim.schemas.ParameterValues.ExtendedParameter import (
    ExtendedParameterValueSC,
)
from bpmn4bpsim.schemas.ParameterValues import all_schemas


class ParameterSC(Schema):
    parameterName = fields.String(required=True)
    resultRequest = fields.List(fields.Raw())
    value: fields.Field = fields.List(fields.Raw())

    def __init__(self, schemas: Mapping[str, Schema], **kwargs):
        super().__init__(**kwargs)
        self.schemas = schemas
        self.fields["value"] = fields.List(
            fields.Nested(ExtendedParameterValueSC(schemas)), required=True
        )

    @validates("resultRequest")
    def validate_result_request(self, data: List[str], **kwargs):
        if data:
            l = deepcopy(data)
            l = [i.upper() for i in l]
            try:
                for i in l:
                    ResultType[i]
            except:
                raise ValidationError("")

    @validates_schema
    def validate_schema(self, data: dict, **kwargs):
        if not (data.get("value") != None or data.get("resultRequest") != None):
            raise ValidationError(
                "Almeno una lista tra value e resultRequest deve essere non vuota"
            )

    @post_load
    def make_parameter(self, data: dict, **kwargs) -> Parameter:
        ext_schema = ExtendedParameterValueSC(self.schemas)
        value = data.get('value')
        if value:
            for idx, param_value in enumerate(value):
                value[idx] = ext_schema.load(param_value)
        else:
            value = []
        rr = data.get("resultRequest")
        if rr:
            rr = [ResultType[i.upper()] for i in rr]
        else:
            rr = []
        return Parameter(
            data["parameterName"],
            value=value,
            resultRequest=rr,
        )

    @post_dump
    def make_parameter_json(self, data: dict, **kwargs):
        ext_schema = ExtendedParameterValueSC(self.schemas)
        if data.get("value"):
            for idx, param_value in enumerate(data["value"]):
                data["value"][idx] = ext_schema.dump(param_value)
        else:
            data.pop("value", None)
        if data.get("resultRequest"):
            for idx, result_type in enumerate(data["resultRequest"]):
                data["resultRequest"][idx] = result_type.value
        else:
            data.pop("resultRequest", None)
        return data


class PropertySC(ParameterSC):
    propertyName = fields.String()
    propertyType = fields.String(
        validate=validate.OneOf(["long", "double", "string", "bool"])
    )

    def __init__(self, **kwargs):
        super().__init__(schemas=all_schemas, **kwargs)

    @validates("parameterName")
    def validate_parameter_name(self, data, **kwargs):
        if data != "Property":
            raise ValidationError(
                "Field parameterName of property must be Property"
            )

    def load(self, prop: dict, **kwargs):
        prop = prop.copy()
        if "parameterName" in prop.keys():
            del prop["parameterName"]
        if "value" in prop.keys():
            schema = ExtendedParameterValueSC(type_schemas=all_schemas)
            for idx, parameter_value in enumerate(prop["value"]):
                prop["value"][idx] = schema.load(parameter_value)
        return Property(**prop)

    def dump(self, data: Property, **kwargs):
        ext_schema = ExtendedParameterValueSC(all_schemas)
        data_dict = dict()
        data_dict["propertyName"] = data.propertyName
        data_dict["parameterName"] = "Property"
        if data.value:
            data_dict["value"] = list()
            for param_value in data.value:
                data_dict["value"].append(ext_schema.dump(param_value))
        if data.resultRequest:
            data_dict["resultRequest"] = list()
            for result_type in data.resultRequest:
                data_dict["resultRequest"].append(result_type.value)
        if data.propertyType:
            data_dict["propertyType"] = data.propertyType.value
        return data_dict

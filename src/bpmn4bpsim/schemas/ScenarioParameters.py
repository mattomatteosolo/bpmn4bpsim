from isodate import parse_duration
from iso4217 import Currency
from marshmallow import (
    Schema, fields, validates, ValidationError, pre_load, post_load,
    post_dump
)
from bpmn4bpsim.bpsimclasses.TimeUnit import TimeUnit
from bpmn4bpsim.schemas.Parameter import ParameterSC, all_schemas
from bpmn4bpsim.schemas.ParameterGroups.PropertyParameters import PropertyParametersSC
from bpmn4bpsim.bpsimclasses.ScenarioParameters import ScenarioParameters


class ScenarioParametersSC(Schema):
    '''
    Classe che definisce lo schema di ScenarioParameters

    {
        start = `Parameter`,
        duration = `Parameter`,
        warmup = `Parameter`,
        propertyParameters = `PropertyParameters`,
        baseResultFrequency = `str`,
        seed = `str`,
        replication = `int`, // > 0
        baseTimeUnit = `TimeUnit`, // definito come uno tra ['ms','s','min','hour','day','year']
        baseCurrencyUnit = `str`,
        baseResultFrequencyCumul = `bool`,
        traceOutput = `bool`,
        traceFormat = `str`
    }
    '''
    start = fields.Nested(ParameterSC(schemas=all_schemas))
    duration = fields.Nested(ParameterSC(schemas=all_schemas))
    warmup = fields.Nested(ParameterSC(schemas=all_schemas))
    propertyParameters = fields.Nested(
        PropertyParametersSC(None, scenario_parameters=True),
        required=False)
    baseResultFrequency = fields.String(required=False)
    seed = fields.Integer(required=False)
    replication = fields.Integer(required=False)
    baseTimeUnit = fields.Raw(required=False)
    baseCurrencyUnit = fields.String(required=False)
    baseResultFrequencyCumul = fields.Boolean(required=False)
    traceOutput = fields.Boolean(required=False)
    traceFormat = fields.String(required=False)

    @validates('baseResultFrequency')
    def validate_base_result_frequency(self, base_result_frequency: str):
        try:
            parse_duration(base_result_frequency)
        except:
            raise ValidationError('Not a valid duration')

    @validates('baseTimeUnit')
    def validate_base_timeunit(self, tu: str, **kwargs):
        if not (isinstance(tu, TimeUnit) or TimeUnit.isTimeUnit(tu)):
            raise ValidationError('Incorrect time unit: {}'.format(tu))

    @pre_load
    def parse_time_unit(self, data: dict, **kwargs):
        data = data.copy()
        if data.get('baseResultFrequency'):
            data['baseResultFrequency'] = parse_duration(
                data['baseResultFrequency']
            )
        if data.get('baseTimeUnit'):
            data['baseTimeUnit'] = TimeUnit(data['baseTimeUnit'])
        if data.get('baseCurrencyUnit'):
            data['baseCurrencyUnit'] = Currency(data['baseCurrencyUnit'])
        return data

    @post_load
    def make_scenario_parameter(
        self,
        data: dict,
        **kwargs
    ) -> ScenarioParameters:
        return ScenarioParameters(**data)

    @post_dump
    def correct_types(self, data: dict, **kwargs):
        data = {k: v for k, v in data.items() if v != None}
        if data.get('baseTimeUnit') \
                and isinstance(data['baseTimeUnit'], TimeUnit):
            data['baseTimeUnit'] = data['baseTimeUnit'].value
        if data.get('baseCurrencyUnit') \
                and isinstance(data['baseCurrencyUnit'], Currency):
            data['baseCurrencyUnit'] = data['baseCurrencyUnit'].value
        return data

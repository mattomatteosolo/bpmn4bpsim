from lxml import etree
from marshmallow import (
    Schema,
    validates,
    ValidationError,
    fields,
    post_load,
    post_dump,
)
from bpmn4bpsim.schemas.ElementParameters import ElementParametersSC
from bpmn4bpsim.schemas.ScenarioParameters import ScenarioParametersSC
from bpmn4bpsim.schemas.VendorExtension import VendorExtensionSC
from bpmn4bpsim.schemas.Calendar import CalendarSC
from bpmn4bpsim.bpsimclasses.Scenario import Scenario


class ScenarioSC(Schema):
    """
    Classe che definisce lo schema di uno scenario

    {
        _id = `str`,
        name = `str`, // required
        description = `str`,
        author = `str`,
        vendor = `str`,
        version = `str`,
        inherits = `str`,
        created = `datetime`,
        modified = `datetime`,
        scenarioParameters = `ScenarioParameters`,
        elementParameters = `List[ElementParameters]`,
        vendorExtensions = `List[VendorExtensions]`,
        calendars = `List[Calendars]`
    }
    """

    # attributes
    _id = fields.String()
    name = fields.String(required=True)
    description = fields.String()
    author = fields.String()
    vendor = fields.String()
    version = fields.String()
    inherits = fields.String()
    created = fields.DateTime(format="iso")  # Datetime
    modified = fields.DateTime(format="iso")  # Datetime
    # "sequence"
    scenarioParameters = fields.Nested(
        ScenarioParametersSC()
    )  # ScenarioParameterSC
    elementParameters = fields.List(fields.Raw())  # ElementParameterSC
    vendorExtensions = fields.List(fields.Nested(VendorExtensionSC))
    calendars = fields.List(fields.Nested(CalendarSC()))  # CalendarSC

    def __init__(self, document_root: etree._ElementTree, **kwargs):
        self.document_root = document_root
        super().__init__(**kwargs)

    @validates("elementParameters")
    def validate_element_paramiters(self, data: list, **_):
        if data:
            schema = ElementParametersSC(self.document_root)
            for idx, item in enumerate(data):
                if len(schema.validate(item)) > 0:
                    raise ValidationError(
                        f"ElementParameters not valid, index: {idx}"
                    )

    @post_load
    def make_scenario(self, data: dict, **_) -> Scenario:
        # data = deepcopy(data)
        if data.get("elementParameters"):
            schema = ElementParametersSC(self.document_root)
            data["elementParameters"] = [
                schema.load(item) for item in data["elementParameters"]
            ]
        return Scenario(**data)

    @post_dump
    def delete_none_fields(self, data: dict, **_):
        if data.get("elementParameters"):
            schema = ElementParametersSC(self.document_root)
            data["elementParameters"] = [
                schema.dump(item) for item in data["elementParameters"]
            ]
        return {
            k: v
            for k, v in data.items()
            if v is not None and v != "" and v != []
        }

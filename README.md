# bpmn4bpsim

`bpsimaas` submodule.

## Description

This package implements logic for BPSim in python. This also provide utilities
to handle BPMN models written in XML format, utilities to parse those models and
retrieve the element that accept BPSim parameters.

In addition, another python module is provided in order to grant the possibility
to serialize and deserialize this data using
[marshmallow](https://marshmallow.readthedocs.io/en/stable/).

## Utilizzo
This project can be used as
[git submodule](https://git-scm.com/book/it/v2/Git-Tools-Submodules), or as a
remote pip dependency:

```bash
$ pip install <vcs>+<method>://<url-to-package>
```

Now vcs is git, and recnmended method is ssh.

If you wanted to clone the repo and work on it, remember to install
[requirements](#requirements)

## Requirements {#requirements}
To work on the package we recommend to first create a virtual environment and
then install requirements ad dev requirements:

```bash
python -m venv <nome_venv>
source <nome_venve/bin/activate>
pip install -r requirements.txt requirements_dev.txt
```

## Test

To test the package locally, first install it

```bash
pip install -e .
```

and then run python unittest (discover mode will run every test it find)
```bash
python -m unittest discover -v -s tests
```

# TODO
# Note d'uso
Continuando a lavorare sulle classi definite da BPSim, leggendo gli esempi e la
definizione dello schema, mi rendo conto in continuazione di dover fare delle
assunzioni, in quanto i moduli python per xml hanno una portata limitata quando
si parla di XPath. In particolare, devo fare attenzione alla classe
`ExpressionParameter`.

## ExpressionParameter
Si tratta di uno strumento molto potente e, sebbene non sia in grado di riconoscere da subito, o all'interno di queste classi, per quale scopo questi oggetti vengano usati, non posso prescindere dal renderne disponibile l'uso.

 Pertanto, la dove possibile, cercherò di wrappare il conenuto di questi oggetti, all'interno di funzioni XPath note che restituiscono un oggetto del tipo richiesto dal parametro.

 Esempi:
 - `TimeParameters`, in tutti i parametri (validi per l'input) si aspetta istanze degli stessi tipi: Floating, Numeric o Duration. Ovviamente, per duration non posso fare nulla, ma posso cambiare il valore dell'esspressione con number( """ExpressionParameter.value""" )
 - `ControlParameters`, il campo `Condition` si aspetta un `BooleanParameter`, ma è comunque possibile passarvi un expression parameter; possiamo farlo "wrappando" l'espressione nella funzione built-in XPath `boolean(expression)`

Un'altra possibile opzione sarebbe quella di rimandare tutto agli utilizzatori della classe, oppure creare una mappa degli oggetti che un'espressione deve contenere per restituire un certo tipo.

Ad esempio, se l'utilizzatore fosse il servizio di api: farei prima una "esecuzione" della funzione, se il valore di ritorno è di un tipo ammissibile per il parametro, allora nessun problema, altrimenti, restituirei un eccezione (codice di errore http).

## Struttura ogetti JSON
Struttura/Schema degli oggetti JSON per gli oggettti di questo modulo ('TipoParametro = { definizione python dict }'):

```
ParameterValue = {
  "validFor": <string:id calendar>,
  "value": <Dipende dal tipo: float, string(anche datetime e duration), bool>,
  "timeUnit":<string:TimeUnit.values>
  "min":<>,
  "max":<>,
  "shape":<>,
  "scale":<>,
  ... e così via per i parametri specifici di ciascuna distribuzione
  "points": <UserDistributionDataPoint JSON>,
  "discrete": <bool>
}
```

```
UserDistributionDataPoint = {
  "probability":<float>,
  "value": <ParameterValue JSON>
}
```

```
Parameter = {
  'parameterName':<string: Nome del parametro BPSIM che risulterà in fase di serializzazione>,
  "value": [
    {
      "type":<string:Nome del param value contenuto in paramValue, usato nelle reflections>
      "parameterValue":<ParameterValue JSON>,
    }
    ...
  ],
  "resultRequest":[
    <string:ResultType.values>
  ]
}
```

```
elementRef = <etree._Element>
ParametersGroup = {
  "parameters": [
    ...
    <Parameter JSON>
    ...
  ]
}
```

```
elementRef = <etree._Element>
ElementParameters = {
  "id": <string: Eventualmente None>,
  "parameterGroupName": <ParameterGroup JSON>,
  ...
}
```

## NOTA

Dando accesso alle ExpressionParameter rendiamo lo strumento BPSim molto più
potente, ma rendiamo possibile un maggior numero di errori da parte degli
utenti. Pertanto, lasciamo agli utilizzatori della classe il compito di
effettuare una sorta di pre-processing della funzione nell'expression parameter,
per verificare che il suo risultato sia coerente con le tipologie di input
accettate.

# TODOs

-   [ ] enum for parameter name
